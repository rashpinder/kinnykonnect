package com.kinnyconnect.app.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */

/*
 * TextView with Custom font family
 * */
@SuppressLint("AppCompatCustomView")
public class TextViewLight extends TextView {

    /*
     * Getting Current Class Name
     * */
    String mTag = TextViewLight.this.getClass().getSimpleName();

    /*
     * Constructor with
     * #Context
     * */
    public TextViewLight(Context context) {
        super(context);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */
    public TextViewLight(Context context,  AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */
    public TextViewLight(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * #int defStyleAttr
     * */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TextViewLight(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new RalewayLight(context).getFont());
        } catch (Exception e) {
            Log.e(mTag, e.toString());
        }
    }
}
