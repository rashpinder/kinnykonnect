package com.kinnyconnect.app.fonts;

import android.content.Context;
import android.graphics.Typeface;

class RalewayBold {
    /*
     * Initialize Context
     * */
    Context mContext;
    /*
     * Initialize the Typeface
     * */
    private Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    private String path = "RalewayBold.ttf";

    /*
     * Default Constructor
     * */
    public RalewayBold() {
    }

    /*
     * Constructor with Context
     * */
    public RalewayBold(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null) {
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        }
        return fontTypeface;
    }

}
