package com.kinnyconnect.app.fonts;

import android.content.Context;
import android.graphics.Typeface;


public class RalewayMedium {
    /*
     * Initialize the Typeface
     * */
    public static Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    String path = "RalewayMedium.ttf";
    /*
     * Initialize Context
     * */
    public Context mContext;
    /*
     * Default Constructor
     * */
    public RalewayMedium() {
    }
    /*
     * Constructor with Context
     * */
    public RalewayMedium(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
            return fontTypeface;
    }
}