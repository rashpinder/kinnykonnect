package com.kinnyconnect.app.fonts;

import android.content.Context;
import android.graphics.Typeface;


public class ItalicText {
    /*
     * Initialize the Typeface
     * */
    public static Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    public String path = "COCOGOOSEPRO-ITALIC.ttf";
    /*
     * Initialize Context
     * */
    public Context mContext;
    /*
     * Default Constructor
     * */
    public ItalicText() {
    }

    /*
     * Constructor with Context
     * */
    public ItalicText(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        return fontTypeface;
    }

}