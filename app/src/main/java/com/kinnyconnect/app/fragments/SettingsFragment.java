package com.kinnyconnect.app.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.DashboardActivity;
import com.kinnyconnect.app.activities.PrivacySettingsActivity;
import com.kinnyconnect.app.activities.ResetPasswordActivity;
import com.kinnyconnect.app.activities.SignUpActivity;
import com.kinnyconnect.app.activities.WebViewActivity;
import com.kinnyconnect.app.fragments.BaseFragment;
import com.kinnyconnect.app.utils.Constants;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.kinnyconnect.app.activities.DrawerActivity.editAddressET;
import static com.kinnyconnect.app.activities.DrawerActivity.imgLocIV;
import static com.kinnyconnect.app.activities.DrawerActivity.txtLocationTV;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = SettingsFragment.this.getClass().getSimpleName();

    /**
     * Current fragment Instance
     */
    Fragment mFragment = SettingsFragment.this;

    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.resetPasswordRL)
    RelativeLayout resetPasswordRL;
    @BindView(R.id.dashboardRL)
    RelativeLayout dashboardRL;
    @BindView(R.id.privacyPolicyRL)
    RelativeLayout privacyPolicyRL;
    @BindView(R.id.helpRL)
    RelativeLayout helpRL;


    public SettingsFragment() {
        // Required empty public constructor
    }

    /*
     * Fragment Override method
     * #onCreateView
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, v);
        imgLocIV.setVisibility(View.GONE);
        txtLocationTV.setVisibility(View.GONE);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick({R.id.resetPasswordRL, R.id.dashboardRL, R.id.privacyPolicyRL, R.id.helpRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.resetPasswordRL:
                performResetPasswordClick();
                break;
            case R.id.dashboardRL:
//                performDashboardClick();
                break;
            case R.id.privacyPolicyRL:
                performPrivacySettingsClick();
                break;
            case R.id.helpRL:
                performHelpClick();
                break;

        }
    }

    private void performHelpClick() {
        Intent mIntent = new Intent(getActivity(), WebViewActivity.class);
        mIntent.putExtra(Constants.WV_TYPE, Constants.HELP_SUPPORT);
        startActivity(mIntent);
    }

    private void performPrivacySettingsClick() {
        Intent mIntent = new Intent(getActivity(), PrivacySettingsActivity.class);
//        mIntent.putExtra(Constants.WV_TYPE, Constants.PRIVACY_POLICY);
        startActivity(mIntent);
    }

    private void performDashboardClick() {
        Intent intent = new Intent(getActivity(), DashboardActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void performResetPasswordClick() {
        Intent intent = new Intent(getActivity(), ResetPasswordActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();}
}


