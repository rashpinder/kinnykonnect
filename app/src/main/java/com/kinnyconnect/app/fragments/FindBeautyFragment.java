package com.kinnyconnect.app.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.FindBeautyAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.CategoryListingModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindBeautyFragment extends BaseFragment implements FindBeautyAdapter.PaginationInterface{
    /**
     * Getting the Current Class Name
     */
    String TAG = FindBeautyFragment.this.getClass().getSimpleName();

    /**
     * Current fragment Instance
     */
    Fragment mFragment = FindBeautyFragment.this;
    /*
     * Widgets
     * */
    Unbinder unbinder;
    FindBeautyAdapter mFindBeautyAdapter;
    @BindView(R.id.beautyRV)
    RecyclerView beautyRV;
    List<CategoryListingModel.CategoryDetail> mCategoryList;

    private int page_no = 1;
    private int item_count = 10;
    private String strLastPage = "FALSE";
    private List<CategoryListingModel.CategoryDetail> tempArrayList = new ArrayList<>();


    FindBeautyAdapter.PaginationInterface paginationInterface;
    private RecyclerView.LayoutManager layoutManager;

    public FindBeautyFragment() {
        // Required empty public constructor
    }

    /*
     * Fragment Override method
     * #onCreateView
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_find_beauty, container, false);
        unbinder = ButterKnife.bind(this, v);
        paginationInterface = this;
        if (tempArrayList != null) {
            tempArrayList.clear();
        }
        if (mCategoryList != null) {
            mCategoryList.clear();
        }
        showCategoryList();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

    private void showCategoryList() {
        if (!isNetworkAvailable(Objects.requireNonNull(getActivity()))) {
            showAlertDialog(Objects.requireNonNull(getActivity()), getString(R.string.internet_connection_error));
        } else {

            if (page_no == 1) {
                showProgressDialog(Objects.requireNonNull(getActivity()));
            } else if (page_no > 1) {
                dismissProgressDialog();
//                mProgressBar.setVisibility(View.GONE);
            }
            executeCategoryListApi();
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("category", "all");
        mMap.put("role", "2");
        mMap.put("pageNo", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeCategoryListApi() {
//        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.categoryListRequest(mParam()).enqueue(new Callback<CategoryListingModel>() {
            @Override
            public void onResponse(Call<CategoryListingModel> call, Response<CategoryListingModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                CategoryListingModel mModel = response.body();
                mCategoryList = new ArrayList<>();
                if (page_no == 1) {
                    dismissProgressDialog();
                }

                if (page_no > 1) {
                    assert mModel != null;
                    if (mModel.getCategoryDetails() == null) {
                        strLastPage = "TRUE";
                    } else if (mModel.getCategoryDetails().size() < 10) {
//                        Toast.makeText(getActivity(), "No more items!!!", Toast.LENGTH_SHORT).show();
                        strLastPage = "TRUE";
                    } else {
                        strLastPage = "FALSE";
                    }
                }


                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mCategoryList = mModel.getCategoryDetails();
                    } else if (page_no > 1) {
                        tempArrayList = mModel.getCategoryDetails();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mCategoryList.addAll(tempArrayList);
                        }
                    }

                    if (page_no == 1) {
                        setCategoryAdapter();
                    } else {
                        mFindBeautyAdapter.notifyDataSetChanged();
                    }

                } else {
                    showAlertDialog(Objects.requireNonNull(getActivity()), mModel.getMessage());
                }
            }

            private void setCategoryAdapter() {
                if (getActivity()!=null){
//                    mFindBeautyAdapter = new FindBeautyAdapter(Objects.requireNonNull(getActivity()), (ArrayList<CategoryListingModel.CategoryDetail>) mCategoryList, paginationInterface);
//                    beautyRV.setLayoutManager(new LinearLayoutManager(Objects.requireNonNull(getActivity())));
//                    beautyRV.setAdapter(mFindBeautyAdapter);
//                    mFindBeautyAdapter.notifyDataSetChanged();
                }}


            @Override
            public void onFailure(Call<CategoryListingModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            showProgressDialog(Objects.requireNonNull(getActivity()));
//            mProgressBar.setVisibility(View.VISIBLE);

            page_no++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeCategoryListApi();
                    } else {
                        if(progressDialog.isShowing()){
                            dismissProgressDialog();}
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }
}