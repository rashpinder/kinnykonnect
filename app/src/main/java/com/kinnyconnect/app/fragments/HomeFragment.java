package com.kinnyconnect.app.fragments;

import android.Manifest;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.model.LatLng;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.BeautyHorizontalAdapter;
import com.kinnyconnect.app.adapters.FindBeautyDesAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.FindBeautyInterface;
import com.kinnyconnect.app.model.FindBeautyModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kinnyconnect.app.activities.DrawerActivity.imgLocIV;
import static com.kinnyconnect.app.activities.DrawerActivity.txtLocationTV;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements FindBeautyDesAdapter.PaginationInterface, FindBeautyInterface{
//        , GoogleApiClient.ConnectionCallbacks,
//        GoogleApiClient.OnConnectionFailedListener,
//        LocationListener,
//        ResultCallback<LocationSettingsResult> {

/************************
 *Fused Google Location
 **************/
public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
protected static final int REQUEST_CHECK_SETTINGS = 0x1;
protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
protected final static String KEY_LOCATION = "location";
protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
public double mLatitude;
public double mLongitude;
protected GoogleApiClient mGoogleApiClient;
protected LocationRequest mLocationRequest;
protected LocationSettingsRequest mLocationSettingsRequest;
protected Location mCurrentLocation;
protected Boolean mRequestingLocationUpdates;
protected String mLastUpdateTime;
private String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
private String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeFragment.this.getClass().getSimpleName();

    /**
     * Current fragment Instance
     */
    Fragment mFragment = HomeFragment.this;

    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.beautyRV)
    RecyclerView beautyRV;
    @BindView(R.id.BeautyHorizontalRV)
    RecyclerView BeautyHorizontalRV;
    FindBeautyDesAdapter mFindBeautyDescAdapter;
    BeautyHorizontalAdapter mhorizontalTextAdapter;
    String cat_id;
    String title;
    String latitude;
    String longitude;
    List<FindBeautyModel.Datum> mDataList;
    List<FindBeautyModel.CategoryListing> mCategoryList;
    private FindBeautyInterface listener;

    private int page_no = 1;
    private int item_count = 10;
    private String strLastPage = "FALSE";
    private List<FindBeautyModel.Datum> tempArrayList = new ArrayList<>();
    public static TextView txtTitleTV;
    String title_var = "1";
    int pos=0;
    String FINAL_LAT;
    String img;
    String FINAL_LONG;
        String address;
        int AUTOCOMPLETE_REQUEST_CODE = 5;
        String lat = "";
        String lng = "";
        String timeStamp = "";
        LatLng latLng = null;
        String addres;
        String curr_lat ;
        String curr_long ;
    FindBeautyModel mModel;

    FindBeautyDesAdapter.PaginationInterface paginationInterface;

    public HomeFragment() {
        // Required empty public constructor
    }

    /*
     * Fragment Override method
     * #onCreateView
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_hme, container, false);
        txtTitleTV = v.findViewById(R.id.txtTitleTV);
        unbinder = ButterKnife.bind(this, v);
        /*
         * For places Location google api
         * */
        paginationInterface = this;
        listener = this;

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imgLocIV.setVisibility(View.VISIBLE);
        txtLocationTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (tempArrayList != null) {
            tempArrayList.clear();
        }
        if (mDataList != null) {
            mDataList.clear();
        }
        if (mCategoryList != null) {
            mCategoryList.clear();
        }
        if (((KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.LAT_NEW,null))!=null)&&((KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.LNG_NEW,null))!=null))
        {
            FINAL_LAT=KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.LAT_NEW,null);
            FINAL_LONG=KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.LNG_NEW,null);

        }
        else {
            FINAL_LAT=KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.CURR_LAT,null);
            FINAL_LONG=KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.CURR_LONG,null);

        }
        page_no=1;
        performGetFindBeauty();
    }

    /*
            Execute api
             */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(getActivity(), KinnyConnectPreferences.ID, null));
        mMap.put("catID", cat_id);
        mMap.put("latitude", FINAL_LAT);
        mMap.put("longitude", FINAL_LONG);
        mMap.put("pageNo", String.valueOf(page_no));
        mMap.put("distance", "100");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performGetFindBeauty() {
        if (!isNetworkAvailable(getActivity())) {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        } else {
            executeFindBeautyApi();
        }
    }

    private void executeFindBeautyApi() {
        if (page_no == 1) {
            showProgressDialog(Objects.requireNonNull(getActivity()));
        } else if (page_no > 1) {
            dismissProgressDialog();
//                mProgressBar.setVisibility(View.GONE);
        }
//        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.findBeautyRequest(mParam()).enqueue(new Callback<FindBeautyModel>() {
            @Override
            public void onResponse(Call<FindBeautyModel> call, Response<FindBeautyModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                mModel = response.body();
                page_no = 1;
                mDataList = new ArrayList<>();
                mCategoryList = new ArrayList<>();
                mCategoryList = mModel.getCategoryListing();
                mDataList = mModel.getData();
                strLastPage=mModel.getLast_page();

                if (page_no == 1) {
                    dismissProgressDialog();
                }

//                if (page_no > 1) {
//                    assert mModel != null;
//                    if (mModel.getData() == null) {
//                        strLastPage = "TRUE";
//                    } else if (mModel.getData().size() < 10) {
////                        Toast.makeText(getActivity()(), "No more items!!!", Toast.LENGTH_SHORT).show();
//                        strLastPage = "TRUE";
//                    } else {
//                        strLastPage = "FALSE";
//                    }
//                }


                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mDataList = mModel.getData();
                    } else if (page_no > 1) {
                        tempArrayList = mModel.getData();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mDataList.addAll(tempArrayList);
                        }
                    }

                    if (page_no == 1) {
                        setBeautyAdapter();
                        setHorizontalBeautyAdapter();
                    } else {
//                        mFindBeautyDescAdapter.notifyDataSetChanged();
                    }

                    if (title_var.equals("1")) {
                        txtTitleTV.setText(mModel.getCatID());
                    }
                } else {
                    if(mDataList!=null){
                        mDataList.clear();
                    }
                    setBeautyAdapter();
//                    setHorizontalBeautyAdapter();
//                    showAlertDialog(Objects.requireNonNull(getActivity()), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<FindBeautyModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void setBeautyAdapter() {
        mFindBeautyDescAdapter = new FindBeautyDesAdapter(getActivity(), (ArrayList<FindBeautyModel.Datum>) mDataList, mModel, paginationInterface);
        beautyRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        beautyRV.setAdapter(mFindBeautyDescAdapter);
        mFindBeautyDescAdapter.notifyDataSetChanged();
    }

    private void setHorizontalBeautyAdapter() {
        mhorizontalTextAdapter = new BeautyHorizontalAdapter(getActivity(), (ArrayList<FindBeautyModel.CategoryListing>) mCategoryList, listener, txtTitleTV,pos,img);
        BeautyHorizontalRV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        BeautyHorizontalRV.setAdapter(mhorizontalTextAdapter);
        mhorizontalTextAdapter.notifyDataSetChanged();
    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            if (strLastPage.equals("FALSE")){
                showProgressDialog(Objects.requireNonNull(getActivity()
            ));
//            mProgressBar.setVisibility(View.VISIBLE);

            page_no++;}
            else {
                dismissProgressDialog();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeFindBeautyApi();
                    } else {
                        if (progressDialog.isShowing()) {
                            dismissProgressDialog();
                        }
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }


    @Override
    public void getData(String cat_id, String title,int pos,String img) {
        this.cat_id = cat_id;
        this.title = title;
        this.pos = pos;
        this.img = img;
        txtTitleTV.setText(title);
        title_var = "2";
        executeFindBeautyApi();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }


//    /******************
//     *Fursed Google Location
//     ************/
//    private void updateValuesFromBundle(Bundle savedInstanceState) {
//        if (savedInstanceState != null) {
//            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
//                mRequestingLocationUpdates = savedInstanceState.getBoolean(
//                        KEY_REQUESTING_LOCATION_UPDATES);
//            }
//            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
//                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
//            }
//            // Update the value of mLastUpdateTime from the Bundle and update the UI.
//            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
//                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
//            }
//        }
//    }
//
//    /**
//     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
//     * LocationServices API.
//     */
//    protected synchronized void buildGoogleApiClient() {
//        Log.i("", "Building GoogleApiClient");
//        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .build();
//    }
//
//    protected void createLocationRequest() {
//        mLocationRequest = new LocationRequest();
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//    }
//
//    /**
//     * Uses a {@link LocationSettingsRequest.Builder} to build
//     * a {@link LocationSettingsRequest} that is used for checking
//     * if a device has the needed location settings.
//     */
//    protected void buildLocationSettingsRequest() {
//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
//        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true);
//        mLocationSettingsRequest = builder.build();
//    }
//
//    /**
//     * Check if the device's location settings are adequate for the app's needs using the
//     * {@link SettingsApi#checkLocationSettings(GoogleApiClient,
//     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
//     */
//    public void checkLocationSettings() {
//        PendingResult<LocationSettingsResult> result =
//                LocationServices.SettingsApi.checkLocationSettings(
//                        mGoogleApiClient,
//                        mLocationSettingsRequest
//                );
//        result.setResultCallback(this);
//    }
//
//    /**
//     * The callback invoked when
//     * {@link SettingsApi#checkLocationSettings(GoogleApiClient,
//     * LocationSettingsRequest)} is called. Examines the
//     * {@link LocationSettingsResult} object and determines if
//     * location settings are adequate. If they are not, begins the process of presenting a location
//     * settings dialog to the user.
//     */
//    @Override
//    public void onResult(LocationSettingsResult locationSettingsResult) {
//        final Status status = locationSettingsResult.getStatus();
//        switch (status.getStatusCode()) {
//            case LocationSettingsStatusCodes.SUCCESS:
//                Log.i("", "All location settings are satisfied.");
//                startLocationUpdates();
//                break;
//            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
//                        "upgrade location settings ");
//                try {
//                    // Show the dialog by calling startResolutionForResult(), and check the result
//                    // in onActivityResult().
//                    status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
//                } catch (IntentSender.SendIntentException e) {
//                    Log.i("", "PendingIntent unable to execute request.");
//                }
//                break;
//            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
//                        "not created.");
//                break;
//            default:
//                // finish();
//        }
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            // Check for the integer request code originally supplied to startResolutionForResult().
//            case REQUEST_CHECK_SETTINGS:
//                switch (resultCode) {
//                    case Activity.RESULT_OK:
//                        Log.i("", "User agreed to make required location settings changes.");
//                        startLocationUpdates();
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Log.i("", "User chose not to make required location settings changes.");
//                        requestPermission();
//                        break;
//                    default:
//                        // finish();
//                }
//
//        }
//    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
//    protected void startLocationUpdates() {
//        try {
//            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                return;
//            }
//            LocationServices.FusedLocationApi.requestLocationUpdates(
//                    mGoogleApiClient,
//                    mLocationRequest,
//                    (LocationListener) getActivity()
//            ).setResultCallback(new ResultCallback<Status>() {
//                @Override
//                public void onResult(Status status) {
//                    mRequestingLocationUpdates = true;
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * Removes location updates from the FusedLocationApi.
//     */
//    protected void stopLocationUpdates() {
//        LocationServices.FusedLocationApi.removeLocationUpdates(
//                mGoogleApiClient, (LocationListener) getActivity()).setResultCallback(new ResultCallback<Status>() {
//            @Override
//            public void onResult(Status status) {
//                mRequestingLocationUpdates = false;
//            }
//        });
//    }
//
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        Log.e(TAG, "onStart");
//        if (mGoogleApiClient != null)
//            mGoogleApiClient.connect();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        Log.e(TAG, "onResume");
//        if (mGoogleApiClient!=null){
//        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
//            startLocationUpdates();
//        }}
//    }
//
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        Log.e(TAG, "onPause");
//        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
//        if (mGoogleApiClient.isConnected()) {
//            stopLocationUpdates();
//        }
//        //  dismissProgressDialog();
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        Log.e(TAG, "onStop");
//        mGoogleApiClient.disconnect();
//        dismissProgressDialog();
//    }
//
//
//
//    @Override
//    public void onConnected(Bundle connectionHint) {
//        Log.i("", "Connected to GoogleApiClient");
//        if (mCurrentLocation == null) {
//            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                return;
//            }
//            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
//        }
//    }
//
//    /**
//     * Callback that fires when the location changes.
//     */
//    @Override
//    public void onLocationChanged(Location location) {
//        mCurrentLocation = location;
//        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
//        mLatitude = mCurrentLocation.getLatitude();
//        mLongitude = mCurrentLocation.getLongitude();
//        Log.e(TAG, "*********LATITUDE********" + mLatitude);
//        Log.e(TAG, "*********LONGITUDE********" + mLongitude);
//        KinnyConnectPreferences.writeString(getActivity(), KinnyConnectPreferences.CURR_LAT,"" + mLatitude);
//        KinnyConnectPreferences.writeString(getActivity(), KinnyConnectPreferences.CURR_LONG, ""+mLongitude);
////        KinnyConnectPreferences.writeString(getActivity(), KinnyConnectPreferences.CURRENT_LOCATION_LATITUDE, "" + mLatitude);
////        KinnyConnectPreferences.writeString(getActivity(), KinnyConnectPreferences.CURRENT_LOCATION_LONGITUDE, "" + mLongitude);
//
//        try {
//            Geocoder geocoder;
//            List<Address> addresses;
//            geocoder = new Geocoder(getActivity(), Locale.getDefault());
//            addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//
//            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//            String city = addresses.get(0).getLocality();
//            String state = addresses.get(0).getAdminArea();
//            String country = addresses.get(0).getCountryName();
//            String postalCode = addresses.get(0).getPostalCode();
//            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onConnectionSuspended(int cause) {
//        Log.i("", "Connection suspended");
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult result) {
//        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
//        // onConnectionFailed.
//        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
//    }
//
//    /**
//     * Stores activity data in the Bundle.
//     */
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
//        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
//        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
//        super.onSaveInstanceState(savedInstanceState);
//    }
//    /*****************************************/
//
//    /*********
//     * Support for Marshmallows Version
//     * GRANT PERMISSION FOR TAKEING IMAGE
//     * 1) ACCESS_FINE_LOCATION
//     * 2) ACCESS_COARSE_LOCATION
//     **********/
//    public boolean checkPermission() {
//        int mlocationFineP = ContextCompat.checkSelfPermission(getActivity(), mAccessFineLocation);
//        int mlocationCourseP = ContextCompat.checkSelfPermission(getActivity(), mAccessCourseLocation);
//        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
//    }
//
//    public void requestPermission() {
//        ActivityCompat.requestPermissions(getActivity(), new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
//    }
//
//
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case REQUEST_PERMISSION_CODE: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//                    checkLocationSettings();
//                } else {
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    // requestPermission();
//                }
//                return;
//            }
//
//        }
//    }
    
}


