package com.kinnyconnect.app.fragments;

import android.Manifest;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.model.LatLng;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.activities.DrawerActivity;
import com.kinnyconnect.app.adapters.BeautyHorizontalAdapter;
import com.kinnyconnect.app.adapters.FindBeautyDesAdapter;

import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.FindBeautyInterface;
import com.kinnyconnect.app.model.FindBeautyModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kinnyconnect.app.activities.DrawerActivity.imgLocIV;
import static com.kinnyconnect.app.activities.DrawerActivity.txtLocationTV;
import static com.kinnyconnect.app.utils.Constants.REQUEST_PERMISSION_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeeFragment extends BaseFragment implements FindBeautyDesAdapter.PaginationInterface, FindBeautyInterface{
  /**
     * Getting the Current Class Name
     */
    String TAG = HomeeFragment.this.getClass().getSimpleName();

    /**
     * Current fragment Instance
     */
    Fragment mFragment = HomeeFragment.this;

    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.beautyRV)
    RecyclerView beautyRV;
    @BindView(R.id.imgBusinessIV)
    ImageView imgBusinessIV;
    @BindView(R.id.BeautyHorizontalRV)
    RecyclerView BeautyHorizontalRV;
    FindBeautyDesAdapter mFindBeautyDescAdapter;
    BeautyHorizontalAdapter mhorizontalTextAdapter;
    String cat_id;
    String title;
    String latitude;
    String longitude;
    List<FindBeautyModel.Datum> mDataList;
    List<FindBeautyModel.CategoryListing> mCategoryList;
    private FindBeautyInterface listener;
    RequestOptions options;
    private int page_no = 1;
    private int item_count = 10;
    private String strLastPage = "FALSE";
    private List<FindBeautyModel.Datum> tempArrayList = new ArrayList<>();
    public static TextView txtTitleTV;
    String title_var = "1";
    int pos=0;
    String FINAL_LAT;
    String FINAL_LONG;
    String img;
    FindBeautyModel mModel;

    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    FindBeautyDesAdapter.PaginationInterface paginationInterface;

    public HomeeFragment() {
        // Required empty public constructor
    }

    /*
     * Fragment Override method
     * #onCreateView
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_homee, container, false);
        txtTitleTV = v.findViewById(R.id.txtTitleTV);
        unbinder = ButterKnife.bind(this, v);
        /*
         * For places Location google api
         * */
        paginationInterface = this;
        listener = this;
        options = new RequestOptions()
                .placeholder(R.drawable.ic_home_ph)
                .error(R.drawable.ic_home_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imgLocIV.setVisibility(View.VISIBLE);
        txtLocationTV.setVisibility(View.VISIBLE);

//        imgLocIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if (tempArrayList != null) {
            tempArrayList.clear();
        }
        if (mDataList != null) {
            mDataList.clear();
        }
        if (mCategoryList != null) {
            mCategoryList.clear();
        }
        if (((KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.LAT_NEW,null))!=null)&&((KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.LNG_NEW,null))!=null))
        {
            FINAL_LAT=KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.LAT_NEW,null);
            FINAL_LONG=KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.LNG_NEW,null);

        }
        else {
            FINAL_LAT=KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.CURR_LAT,null);
            FINAL_LONG=KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.CURR_LONG,null);

        }
        KinnyConnectPreferences.writeString(getActivity(),KinnyConnectPreferences.FIN_LAT,FINAL_LAT);
        KinnyConnectPreferences.writeString(getActivity(),KinnyConnectPreferences.FIN_LONG,FINAL_LONG);
        page_no=1;
        performGetFindBeauty();
    }

    /*
            Execute api
             */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(getActivity(), KinnyConnectPreferences.ID, null));
        mMap.put("catID", cat_id);
        mMap.put("latitude", FINAL_LAT);
        mMap.put("longitude", FINAL_LONG);
        mMap.put("pageNo", String.valueOf(page_no));
        mMap.put("distance", "100");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performGetFindBeauty() {
        if (!isNetworkAvailable(getActivity())) {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        } else {
            executeFindBeautyApi();
        }
    }

    private void executeFindBeautyApi() {
        if (page_no == 1) {
            showProgressDialog(requireActivity());
        } else if (page_no > 1) {
            dismissProgressDialog();
//                mProgressBar.setVisibility(View.GONE);
        }
//        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.findBeautyRequest(mParam()).enqueue(new Callback<FindBeautyModel>() {
            @Override
            public void onResponse(Call<FindBeautyModel> call, Response<FindBeautyModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                 mModel = response.body();
                page_no = 1;
                mDataList = new ArrayList<>();
                mCategoryList = new ArrayList<>();
                mCategoryList = mModel.getCategoryListing();
                mDataList = mModel.getData();
                strLastPage=mModel.getLast_page();

                if (page_no == 1) {
                    dismissProgressDialog();
                }
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mDataList = mModel.getData();
                    } else if (page_no > 1) {
                        tempArrayList = mModel.getData();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mDataList.addAll(tempArrayList);
                        }
                    }

                    if (page_no == 1) {
                        setBeautyAdapter();
                        setHorizontalBeautyAdapter();
                    } else {
//                        mFindBeautyDescAdapter.notifyDataSetChanged();
                    }

                    if (title_var.equals("1")) {
                        txtTitleTV.setText(mModel.getCatID());
                        Glide.with(requireActivity()).load(mCategoryList.get(0).getImage()).apply(options).into(imgBusinessIV);
                    }
                    txtNoDataFountTV.setVisibility(View.GONE);
                } else {
                    if(mDataList!=null){
                        mDataList.clear();
                    }
                    setBeautyAdapter();
                    setHorizontalBeautyAdapter();

                    if (title_var.equals("1")) {
                    if (mCategoryList.get(0).getTitle()!=null){
                        txtTitleTV.setText(mCategoryList.get(0).getTitle());
                        Glide.with(requireActivity()).load(mCategoryList.get(0).getImage()).apply(options).into(imgBusinessIV);
                    }}

                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<FindBeautyModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void setBeautyAdapter() {
        mFindBeautyDescAdapter = new FindBeautyDesAdapter(getActivity(), (ArrayList<FindBeautyModel.Datum>) mDataList,mModel, paginationInterface);
        beautyRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        beautyRV.setAdapter(mFindBeautyDescAdapter);
        mFindBeautyDescAdapter.notifyDataSetChanged();
    }

    private void setHorizontalBeautyAdapter() {
        mhorizontalTextAdapter = new BeautyHorizontalAdapter(getActivity(), (ArrayList<FindBeautyModel.CategoryListing>) mCategoryList, listener, txtTitleTV,pos,img);
        BeautyHorizontalRV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        BeautyHorizontalRV.setAdapter(mhorizontalTextAdapter);
        BeautyHorizontalRV.getLayoutManager().scrollToPosition(pos);
        mhorizontalTextAdapter.notifyDataSetChanged();
    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            if (strLastPage.equals("FALSE")){
                showProgressDialog(requireActivity());
//            mProgressBar.setVisibility(View.VISIBLE);

                page_no++;}
            else {
                dismissProgressDialog();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeFindBeautyApi();
                    } else {
                        if (progressDialog.isShowing()) {
                            dismissProgressDialog();
                        }
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }


    @Override
    public void getData(String cat_id, String title,int pos,String img) {
        this.cat_id = cat_id;
        this.title = title;
        this.pos = pos;
        this.img = img;
        txtTitleTV.setText(title);
        title_var = "2";
        this.img = img;
        Glide.with(getActivity()).load(img).apply(options).into(imgBusinessIV);
        executeFindBeautyApi();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

}


