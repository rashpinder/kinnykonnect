package com.kinnyconnect.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.BusinessSignInActivity;
import com.kinnyconnect.app.activities.HomeActivity;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class JoinCommunityFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = JoinCommunityFragment.this.getClass().getSimpleName();

    /**
     * Current fragment Instance
     */
    Fragment mFragment = JoinCommunityFragment.this;

    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.txtStandardPackageTV)
    TextView txtStandardPackageTV;



    public JoinCommunityFragment() {
        // Required empty public constructor
    }

    /*
     * Fragment Override method
     * #onCreateView
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_join_community, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick({R.id.txtStandardPackageTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtStandardPackageTV:
                performStandardPackageClick();
                break;
        }
    }

    private void performStandardPackageClick() {
//        if (KinnyConnectPreferences.readBoolean(getActivity(),KinnyConnectPreferences.IS_BUSINNESS_LOGIN,false)){
//            Intent intent = new Intent(getActivity(), HomeActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//    }
//    else{
//        Intent intent = new Intent(getActivity(), BusinessSignInActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//    }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }
}


