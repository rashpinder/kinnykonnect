package com.kinnyconnect.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.activities.HomeActivity;
import com.kinnyconnect.app.adapters.CurrentAppointmentAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.CurrentAppointmentModel;
import com.kinnyconnect.app.model.ProfileModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentAppointmentFragment extends BaseFragment {


    /**
     * Getting the Current Class Name
     */
    String TAG = CurrentAppointmentFragment.this.getClass().getSimpleName();


    /**
     * Widgets
     */
    @BindView(R.id.currentAppSH)
    StickyListHeadersListView currentAppSH;
    CurrentAppointmentAdapter currentAppointmentAdapter;
    ArrayList<CurrentAppointmentModel> currentAppointmentModelArrayList = new ArrayList<>();
    Context mActivity;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;

    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     *
     * Initialize Objects...
     * */


    /*
     * Default Constructor
     * */
    public CurrentAppointmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_current_appointment, container, false);
        mUnbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getCurrentAppointments();
    }

    private void getCurrentAppointments() {
        if (!isNetworkAvailable(Objects.requireNonNull(getActivity()))) {
            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
        } else {
            if (currentAppointmentModelArrayList!=null){
                currentAppointmentModelArrayList.clear();
            }
            executeGetCurrentAppointmentsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(getActivity(), KinnyConnectPreferences.ID, null));
        mMap.put("type", "2");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetCurrentAppointmentsApi() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getCurrentAppRequest(mParam()).enqueue(new retrofit2.Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                try {
                    JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                    JSONObject jsonObject = jsonObjectMM.getJSONObject("Appointment_details");
                    Log.e(TAG, "obj: " + jsonObjectMM.getJSONObject("Appointment_details"));
                    Iterator<String> keys = jsonObject.keys();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        Log.e(TAG, "setJsonModel: " + key);
                        if (jsonObject.get(key) instanceof JSONArray) {
                            for (int i = 0; i < ((JSONArray) jsonObject.get(key)).length(); i++) {
                                JSONObject mObject = ((JSONArray) jsonObject.get(key)).getJSONObject(i);
                                CurrentAppointmentModel model = new CurrentAppointmentModel();
                                Log.e(TAG, ": businessName::::::" + mObject.getString("businessName"));
                                Log.e(TAG, ": businessImage::::::" + mObject.getString("businessImage"));
                                Log.e(TAG, ": bookingTime::::::" + mObject.getString("bookingTime"));
                                Log.e(TAG, ": serviceName::::::" + mObject.getString("serviceName"));
                                Log.e(TAG, ": created::::::" + mObject.getString("created"));

                                model.setBusinessName(mObject.getString("businessName"));
                                model.setBusinessImage(mObject.getString("businessImage"));
                                model.setBookingTime(mObject.getString("bookingTime"));
                                model.setServiceName(mObject.getString("serviceName"));
                                model.setCreated(mObject.getString("created"));

                                currentAppointmentModelArrayList.add(model);
//                                if (currentAppointmentModelArrayList.size()==0) {
//                                    txtNoDataFountTV.setVisibility(View.VISIBLE);
//                                } else {
//                                    txtNoDataFountTV.setVisibility(View.GONE);
//                                }
                            }
                        }
                    }

                } catch (Exception e) {
                    Log.e(TAG, "setJsonModel: " + e.toString());
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                }

                setCurrentAppointmentAdapter();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setCurrentAppointmentAdapter() {
        if (currentAppSH != null) {
            currentAppointmentAdapter = new CurrentAppointmentAdapter(getActivity(), currentAppointmentModelArrayList);
            currentAppSH.setAdapter(currentAppointmentAdapter);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            mActivity = context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mActivity = null;
    }

}
