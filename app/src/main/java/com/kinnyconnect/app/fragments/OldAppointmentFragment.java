package com.kinnyconnect.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.JsonObject;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.activities.HomeActivity;
import com.kinnyconnect.app.adapters.OldAppointmentAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.OldAppointmentModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * A simple {@link Fragment} subclass.
 */
public class OldAppointmentFragment extends BaseFragment {


    /**
     * Getting the Current Class Name
     */
    String TAG = OldAppointmentFragment.this.getClass().getSimpleName();


    /**
     * Widgets
     */
    @BindView(R.id.oldAppSH)
    StickyListHeadersListView oldAppSH;
    OldAppointmentAdapter oldAppointmentAdapter;
    ArrayList<OldAppointmentModel> oldAppointmentModelArrayList = new ArrayList<>();
    Context mActivity;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;

    /*
     * Initialize  Unbinder for butterknife;
     * */
    private Unbinder mUnbinder;

    /*
     *
     * Initialize Objects...
     * */


    /*
     * Default Constructor
     * */
    public OldAppointmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_old_appointment, container, false);
        mUnbinder = ButterKnife.bind(this, mView);
        getOldAppointments();
        return mView;
    }

    private void getOldAppointments() {
        if (!isNetworkAvailable(Objects.requireNonNull(getActivity()))) {
            showAlertDialog(getActivity(), getString(R.string.internet_connection_error));
        } else {
            if (oldAppointmentModelArrayList!=null){
                oldAppointmentModelArrayList.clear();
            }
            executeGetOldAppointmentsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(getActivity(), KinnyConnectPreferences.ID, null));
        mMap.put("type", "1");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetOldAppointmentsApi() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getOldAppRequest(mParam()).enqueue(new retrofit2.Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());

                try {
                    JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                    JSONObject jsonObject = jsonObjectMM.getJSONObject("Appointment_details");
                    Log.e(TAG, "obj: " + jsonObjectMM.getJSONObject("Appointment_details"));
                    Iterator<String> keys = jsonObject.keys();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        Log.e(TAG, "setJsonModel: " + key);
                        if (jsonObject.get(key) instanceof JSONArray) {
                            for (int i = 0; i < ((JSONArray) jsonObject.get(key)).length(); i++) {
                                JSONObject mObject = ((JSONArray) jsonObject.get(key)).getJSONObject(i);
                                OldAppointmentModel model = new OldAppointmentModel();
                                Log.e(TAG, ": businessName::::::" + mObject.getString("businessName"));
                                Log.e(TAG, ": businessImage::::::" + mObject.getString("businessImage"));
                                Log.e(TAG, ": bookingTime::::::" + mObject.getString("bookingTime"));
                                Log.e(TAG, ": serviceName::::::" + mObject.getString("serviceName"));
                                Log.e(TAG, ": created::::::" + mObject.getString("created"));

                                model.setBusinessName(mObject.getString("businessName"));
                                model.setBusinessImage(mObject.getString("businessImage"));
                                model.setBookingTime(mObject.getString("bookingTime"));
                                model.setServiceName(mObject.getString("serviceName"));
                                model.setCreated(mObject.getString("created"));
                                oldAppointmentModelArrayList.add(model);
Log.e(TAG,"old"+oldAppointmentModelArrayList.size());
//                                if (oldAppointmentModelArrayList.size()==0) {
//                                    txtNoDataFountTV.setVisibility(View.VISIBLE);
//                                } else {
//                                    txtNoDataFountTV.setVisibility(View.GONE);
//                                }
                            }
                        }
                    }

                } catch (Exception e) {
                    Log.e(TAG, "setJsonModel: " + e.toString());
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                }
                setOldAppointmentAdapter();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setOldAppointmentAdapter() {
        if (oldAppSH != null) {
            oldAppointmentAdapter = new OldAppointmentAdapter(getActivity(), oldAppointmentModelArrayList);
            oldAppSH.setAdapter(oldAppointmentAdapter);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            mActivity =context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mActivity = null;
    }

}
