package com.kinnyconnect.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.activities.ManageBusinessAccountActivity;
import com.kinnyconnect.app.activities.TellUsAboutYourselfActivity;
import com.kinnyconnect.app.activities.TellUsAboutYourselfThreeActivity;
import com.kinnyconnect.app.activities.TellUsAboutYourselfTwoActivity;
import com.kinnyconnect.app.adapters.DealsPromotionsAdapter;
import com.kinnyconnect.app.adapters.FindBeautyAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.CategoryListingModel;
import com.kinnyconnect.app.model.DealsModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

import butterknife.BindView;
import butterknife.ButterKnife;

import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kinnyconnect.app.activities.DrawerActivity.editAddressET;
import static com.kinnyconnect.app.activities.DrawerActivity.imgLocIV;
import static com.kinnyconnect.app.activities.DrawerActivity.txtLocationTV;

/**
 * A simple {@link Fragment} subclass.
 */
public class DealsFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = DealsFragment.this.getClass().getSimpleName();

    /**
     * Current fragment Instance
     */
    Fragment mFragment = DealsFragment.this;
    /*
     * Widgets
     * */
    Unbinder unbinder;
    DealsPromotionsAdapter dealsPromotionsAdapter;
    @BindView(R.id.dealsRV)
    RecyclerView dealsRV;
    List<DealsModel.Datum> mDealsList;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;


//    private RecyclerView.LayoutManager layoutManager;

    public DealsFragment() {
        // Required empty public constructor
    }

    /*
     * Fragment Override method
     * #onCreateView
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_deals, container, false);
        unbinder = ButterKnife.bind(this, v);
        imgLocIV.setVisibility(View.GONE);
        txtLocationTV.setVisibility(View.GONE);
        getDealsAndPromotions();
        return v;
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("DealPromotion", "all");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void getDealsAndPromotions() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getDealsRequest(mParam()).enqueue(new Callback<DealsModel>() {
            @Override
            public void onResponse(Call<DealsModel> call, Response<DealsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                DealsModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mDealsList = new ArrayList<>();
                    mDealsList = mModel.getData();

                    setDealsAdapter();
                    txtNoDataFountTV.setVisibility(View.GONE);
                } else {
//                    showAlertDialog(getActivity(), mModel.getMessage());
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                }
            }

            private void setDealsAdapter() {
                if (getActivity() != null) {
            dealsPromotionsAdapter = new DealsPromotionsAdapter(Objects.requireNonNull(getActivity()), (ArrayList<DealsModel.Datum>) mDealsList);
            dealsRV.setLayoutManager(new LinearLayoutManager(Objects.requireNonNull(getActivity())));
            dealsRV.setAdapter(dealsPromotionsAdapter);
            dealsPromotionsAdapter.notifyDataSetChanged();
//        }
            }}

            @Override
            public void onFailure(Call<DealsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
  


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }


}