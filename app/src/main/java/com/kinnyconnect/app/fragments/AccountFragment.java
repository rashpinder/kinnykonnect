package com.kinnyconnect.app.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.BookingAppointmentsActivity;
import com.kinnyconnect.app.activities.BookingPurchaseActivity;
import com.kinnyconnect.app.activities.BusinessSignInActivity;
import com.kinnyconnect.app.activities.BusinessSignUpActivity;
import com.kinnyconnect.app.activities.CartActivity;
import com.kinnyconnect.app.activities.FavoritesActivity;
import com.kinnyconnect.app.activities.InviteActivity;
import com.kinnyconnect.app.activities.LoginActivity;
import com.kinnyconnect.app.activities.ManageBusinessAccountActivity;
import com.kinnyconnect.app.activities.NotificationsActivity;
import com.kinnyconnect.app.activities.ProfileActivity;
import com.kinnyconnect.app.activities.RedeemPointsActivity;
import com.kinnyconnect.app.activities.SignUpActivity;
import com.kinnyconnect.app.activities.TellUsAboutYourselfActivity;
import com.kinnyconnect.app.fragments.BaseFragment;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.kinnyconnect.app.activities.DrawerActivity.editAddressET;
import static com.kinnyconnect.app.activities.DrawerActivity.imgLocIV;
import static com.kinnyconnect.app.activities.DrawerActivity.txtLocationTV;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = AccountFragment.this.getClass().getSimpleName();

    /**
     * Current fragment Instance
     */
    Fragment mFragment = AccountFragment.this;

    Unbinder unbinder;

    /*
     * Widgets
     * */
    @BindView(R.id.accountInfoRL)
    RelativeLayout accountInfoRL;
    @BindView(R.id.notificationRL)
    RelativeLayout notificationRL;
    @BindView(R.id.myBookingsRL)
    RelativeLayout myBookingsRL;
    @BindView(R.id.pointsRL)
    RelativeLayout pointsRL;
    @BindView(R.id.inviteRL)
    RelativeLayout inviteRL;
    @BindView(R.id.manageRL)
    RelativeLayout manageRL;
 @BindView(R.id.cartRL)
 RelativeLayout cartRL;
 @BindView(R.id.favRL)
    RelativeLayout favRL;
 @BindView(R.id.appointmentsRL)
    RelativeLayout appointmentsRL;
 @BindView(R.id.txtUsernameTV)
    TextView txtUsernameTV;
 @BindView(R.id.businessView)
    View businessView;


    public AccountFragment() {
        // Required empty public constructor
    }

    /*
     * Fragment Override method
     * #onCreateView
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_account, container, false);
        unbinder = ButterKnife.bind(this, v);
        txtUsernameTV.setText(KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.NAME,null));

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.IS_BUSINNESS_LOGIN, false)){
            businessView.setVisibility(View.VISIBLE);
            manageRL.setVisibility(View.VISIBLE);
        }
        else {
            businessView.setVisibility(View.GONE);
            manageRL.setVisibility(View.GONE);}
        imgLocIV.setVisibility(View.GONE);
        txtLocationTV.setVisibility(View.GONE);}


    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick({R.id.accountInfoRL, R.id.pointsRL, R.id.myBookingsRL, R.id.inviteRL, R.id.manageRL,R.id.favRL,R.id.notificationRL,R.id.cartRL,R.id.appointmentsRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.accountInfoRL:
                if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.ISLOGIN, false)){
                    performAccountInfoClick(); }
                else {
                    showLoginAlertDialog(getActivity(),"Please Login First");
                }
                break;
            case R.id.pointsRL:
                if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.ISLOGIN, false)){
                    performPointsClick(); }
                else {
                    showLoginAlertDialog(getActivity(),"Please Login First");
                }

                break;
            case R.id.myBookingsRL:
                if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.ISLOGIN, false)){
                    performBookingClick(); }
                else {
                    showLoginAlertDialog(getActivity(),"Please Login First"); }


                break;
            case R.id.inviteRL:
                if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.ISLOGIN, false)){
                    performInviteClick();}
                else {
                    showLoginAlertDialog(getActivity(),"Please Login First"); }

                break;
                case R.id.notificationRL:
                if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.ISLOGIN, false)){
                    performNotificationClick();}
                else {
                    showLoginAlertDialog(getActivity(),"Please Login First"); }

                break;
            case R.id.manageRL:
                if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.IS_BUSINNESS_LOGIN, false)){
                    performManageClick(); }
                else {
                    showBusinessAlertDialog(getActivity(),"Please Login First"); }
                break;
                case R.id.favRL:
                    if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.ISLOGIN, false)){
                        performFavClick(); }
                    else {
                        showLoginAlertDialog(getActivity(),"Please Login First"); }
                break;
                    case R.id.cartRL:
                    if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.ISLOGIN, false)){
                        performCartClick(); }
                    else {
                        showLoginAlertDialog(getActivity(),"Please Login First"); }
                break;
                    case R.id.appointmentsRL:
                    if(KinnyConnectPreferences.readBoolean(getActivity(), KinnyConnectPreferences.ISLOGIN, false)){
                        performAppointmentClick(); }
                    else {
                        showLoginAlertDialog(getActivity(),"Please Login First"); }
                break;

        }
    }

    private void performAppointmentClick() {
        startActivity(new Intent(getActivity(),BookingAppointmentsActivity.class));
    }

    private void performCartClick() {
        Intent intent = new Intent(getActivity(), CartActivity.class);
        intent.putExtra("value","account");
        startActivity(intent);
    }

    private void performNotificationClick() {
        startActivity(new Intent(getActivity(),NotificationsActivity.class));
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showBusinessLoginAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, BusinessSignInActivity.class));
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void performFavClick() {
        startActivity(new Intent(getActivity(),FavoritesActivity.class));

    }

    private void performManageClick() {
        if (KinnyConnectPreferences.readBoolean(getActivity(),KinnyConnectPreferences.IS_BUSINNESS_LOGIN,false)){
//            if (KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.FILL,null).equals("0")||
//                    KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.FILL,null).equals("1")||
//                    KinnyConnectPreferences.readString(getActivity(),KinnyConnectPreferences.FILL,null).equals("2")){
//                showBusinessAlertDialog(getActivity(), "Please complete process of Business login First"); }
//            else{
                Intent intent = new Intent(getActivity(), ManageBusinessAccountActivity.class);
                intent.putExtra("value","account");
                startActivity(intent);
//            }
        }
        else {
            showBusinessAlertDialog(getActivity(), "Please login with Business Account");
        }


    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showBusinessAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
//        TextView btnYes = alertDialog.findViewById(R.id.btnYes);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, BusinessSignInActivity.class);
                startActivity(intent);
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void performInviteClick() {
        startActivity(new Intent(getActivity(),InviteActivity.class));
    }

    private void performBookingClick() {
        startActivity(new Intent(getActivity(),BookingPurchaseActivity.class));
    }

    private void performPointsClick() {
        startActivity(new Intent(getActivity(),RedeemPointsActivity.class));
    }

    private void performAccountInfoClick() {
        startActivity(new Intent(getActivity(), ProfileActivity.class));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }


}


