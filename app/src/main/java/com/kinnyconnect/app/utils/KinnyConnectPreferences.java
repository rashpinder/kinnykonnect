package com.kinnyconnect.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class KinnyConnectPreferences {

    /***************
     * Define SharedPrefrances Keys & MODE
     ****************/
    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;
    /*
     * Keys
     * */
    public static final String ID = "id";
    public static final String IS_GOOGLE_LOGIN = "is_google_login";
    public static final String IS_CONNECT = "is_connect";
    public static final String TYPE_TWO = "type_two";
     public static final String CALENDAR_DETAIL = "calendar_detail";
    public static final String ISLOGIN = "is_login";
    public static final String IS_BUSINNESS_LOGIN = "is_business_login";
    public static final String EMAIL = "email";
    public static final String ROLE = "role";
    public static final String PASSWORD = "password";
    public static final String VERIFICATION_CODE = "verification_code";
    public static final String VERIFIED = "verified";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String PURCHASED_PLAN = "purchased_plan";
    public static final String CREATED = "created";
    public static final String EXPIRE_DATE = "expire_date";
    public static final String IMAGE = "image";
    public static final String LAT = "lat";
    public static final String LONG = "long";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String NAME = "name";
    public static final String GOOGLE_TOKEN = "google_token";
    public static final String FB_TOKEN = "fb_token";
    public static final String DEVICE_TYPE = "device_type";

    public static final String BUSINESS_ID = "business_id";
    public static final String BUSINNESS_EMAIL = "business_email";
    public static final String BUSINNESS_ROLE = "business_role";
    public static final String BUSINNESS_PASSWORD = "business_password";
    public static final String BUSINNESS_VERIFICATION_CODE = "business_verification_code";
    public static final String BUSINNESS_VERIFIED = "business_verified";
    public static final String BUSINNESS_DEVICE_TOKEN = "business_device_token";
    public static final String BUSINNESS_PURCHASED_PLAN = "business_purchased_plan";
    public static final String BUSINNESS_CREATED = "business_created";
    public static final String BUSINNESS_EXPIRE_DATE = "business_expire_date";
    public static final String BUSINNESS_IMAGE = "business_image";
    public static final String BUSINNESS_LAT = "business_lat";
    public static final String BUSINNESS_LONG = "business_long";
    public static final String BUSINNESS_NAME = "business_name";
    public static final String BUSINNESS_GOOGLE_TOKEN = "business_google_token";
    public static final String BUSINNESS_FB_TOKEN = "business_fb_token";
    public static final String BUSINNESS_DEVICE_TYPE = "business_device_type";
    public static final String FILL = "fill";
    public static final String CURRENT_LOCATION_NEW = "current_loc_new";
    public static final String LAT_NEW = "new_lat";
    public static final String LNG_NEW = "new_long";
    public static final String CURR_LAT = "current_lat";
    public static final String CURR_LONG = "current_long";
    public static final String FIN_LAT = "fin_lat";
    public static final String FIN_LONG = "fin_long";
    public static final String PRODUCT_IMAGE = "product_image";
    public static final String ADD = "add";
    public static final String SELECTED_LOCATION = "selected_location";

    /*
     * Write the Boolean Value
     * */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /*
     * Read the Boolean Valueget
     * */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /*
     * Write the Integer Value
     * */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();

    }

    /*
     * Read the Interger Value
     * */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /*
     * Write the String Value
     * */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();

    }

    /*
     * Read the String Value
     * */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /*
     * Write the Float Value
     * */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /*
     * Read the Float Value
     * */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /*
     * Write the Long Value
     * */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /*
     * Read the Long Value
     * */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}

