package com.kinnyconnect.app.utils;

public class Constants {
    /*
     * Constants @params
     * */

    public  static int REQUEST_CODE = 123;
    public  static final int REQUEST_PERMISSION_CODE = 919;
    public static String ACCOUNT_FRAGMENT = "account_fragment";
    public static String BUSINESS_FRAGMENT = "business_fragment";
    public static String SETTINGS_FRAGMENT = "settings_fragment";
    public static String FIND_BEAUTY_FRAGMENT = "find_beauty_fragment";
    public static String SHOP_FRAGMENT = "shop_fragment";
    public static String HOME_FRAGMENT = "home_fragment";
    public static String DEALS_FRAGMENT = "deals_fragment";
    public static String JOIN_COMM_FRAGMENT = "join_comm_fragment";

    public static final String TRICKS_ID = "tricks_id";
    public static final String CHAPTER_ID = "chapter_id";
    public static final String URL = "url";
    public  static String WV_TYPE = "wv_type";
    public  static String PRIVACY_POLICY = "privacy_policy";
    public  static String HELP_SUPPORT = "help_support";
    public  static String TOTAL_PRICE = "total_price";
    public  static String PUBLISHABLE_KEY = "";

    public static final String CLIENT_ID_SERVER = "606968004218-51n0sjfrgbpdo2dfqjldlj1dnrovs6d2.apps.googleusercontent.com";
    public static final String CLIENT_SECRET_SERVER = "BSPctUu3f_1k0GzBy3RYnk-5";




    /*
     * urls
     * */
    //web view urls
    public static String PRIVACY_POLICY_WEB = "https://www.dharmani.com/kinnyKonnect/PrivacyPolicy.html";
    public static String HELP_SUPPORT_WEB = "https://www.dharmani.com/kinnyKonnect/Help.html";

    /*
     * @Api Urls
     * */
    private final static String BASE_URL = "";

    //Working Socket URL
    public static final String SocketURL = "http://jaohar-uk.herokuapp.com:80";

    public  static final int VIEW_TYPE_SENDER_MESSAGE = 1;
    public  static final int VIEW_TYPE_RECIEVER_MESSAGE = 0;



}
