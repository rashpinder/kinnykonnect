package com.kinnyconnect.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class LoginPrefrences {

    /***************
     * Define SharedPrefrances Keys & MODE
     ****************/
    public static final String PREF_NAME = "Login_PREF";
    public static final int MODE = Context.MODE_PRIVATE;
    /*
     * Keys
     * */
    public static final String REMEMBER = "remember";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String REMEMBER_BUSINESS = "remember";
    public static final String BUSINESS_EMAIL = "business_email";
    public static final String BUSINESS_PASSWORD = "business_password";

    /*
     * Write the Boolean Value
     * */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /*
     * Read the Boolean Valueget
     * */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /*
     * Write the Integer Value
     * */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();

    }

    /*
     * Read the Interger Value
     * */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /*
     * Write the String Value
     * */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();

    }

    /*
     * Read the String Value
     * */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /*
     * Write the Float Value
     * */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /*
     * Read the Float Value
     * */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /*
     * Write the Long Value
     * */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /*
     * Read the Long Value
     * */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}

