package com.kinnyconnect.app.interfaces;


import com.kinnyconnect.app.model.TimeSlotModel;

public interface SelectTimeSlotInterface {
    public void getTimeSlot(TimeSlotModel.FreeSlot mFreeTimeSlotsModel);
}
