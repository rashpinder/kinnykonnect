package com.kinnyconnect.app.interfaces;

import com.google.gson.JsonObject;
import com.kinnyconnect.app.model.AddToCartModel;
import com.kinnyconnect.app.model.ApplyCouponModel;
import com.kinnyconnect.app.model.BookModel;
import com.kinnyconnect.app.model.BookingPurchaseModel;
import com.kinnyconnect.app.model.BusinessDetailModel;
import com.kinnyconnect.app.model.CardModel;
import com.kinnyconnect.app.model.CartModel;
import com.kinnyconnect.app.model.CategoryListingModel;
import com.kinnyconnect.app.model.DealsModel;
import com.kinnyconnect.app.model.EditHoursModel;
import com.kinnyconnect.app.model.EndTimeModel;
import com.kinnyconnect.app.model.FavoriteModel;
import com.kinnyconnect.app.model.FindBeautyModel;
import com.kinnyconnect.app.model.ForgotPasswordModel;
import com.kinnyconnect.app.model.GoogleLoginLinkModel;
import com.kinnyconnect.app.model.IncDecQuantityModel;
import com.kinnyconnect.app.model.LoginModel;
import com.kinnyconnect.app.model.LogoutModel;
import com.kinnyconnect.app.model.ManageBusinessModel;
import com.kinnyconnect.app.model.NotificationsModel;
import com.kinnyconnect.app.model.ProfileModel;
import com.kinnyconnect.app.model.RatingModel;
import com.kinnyconnect.app.model.ReedemPointsModel;
import com.kinnyconnect.app.model.RefreshTokenModel;
import com.kinnyconnect.app.model.ResetPasswordModel;
import com.kinnyconnect.app.model.ShopListModel;
import com.kinnyconnect.app.model.SignUpModel;
import com.kinnyconnect.app.model.SingleProductModel;
import com.kinnyconnect.app.model.TellYourselfModel;
import com.kinnyconnect.app.model.TimeSlotModel;
import com.kinnyconnect.app.model.ViewAllReviewsModel;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    String BASE_URL = "https://www.dharmani.com/kinnyKonnect/webservices/";


    @POST("signUp.php")
    Call<SignUpModel> signUpRequest(@Body Map<String, String> mParams);

    @POST("logIn.php")
    Call<LoginModel> loginRequest(@Body Map<String, String> mParams);

    @POST("forgetPassword.php")
    Call<ForgotPasswordModel> forgotPasswordRequest(@Body Map<String, String> mParams);

    @POST("googleLogin.php")
    Call<LoginModel> loginWithGoogleRequest(@Body Map<String, String> mParams);

    @POST("facebookLogin.php")
    Call<LoginModel> loginWithFbRequest(@Body Map<String, String> mParams);

    @POST("logOut.php")
    Call<LogoutModel> logoutRequest(@Body Map<String, String> mParams);

    @POST("changePassword.php")
    Call<ResetPasswordModel> changePassRequest(@Body Map<String, String> mParams);

  @POST("GetCategoryListing.php")
    Call<CategoryListingModel> categoryListRequest(@Body Map<String, String> mParams);

  @POST("manageBusiness.php")
    Call<ManageBusinessModel> businessListRequest(@Body Map<String, String> mParams);

  @POST("createAboutBusiness.php")
    Call<TellYourselfModel> createAccountRequest(@Body Map<String, String> mParams);


  @POST("getAllNearByBusiness.php")
    Call<FindBeautyModel> findBeautyRequest(@Body Map<String, String> mParams);

  @POST("editWorkingHours.php")
    Call<EditHoursModel> editHoursRequest(@Body Map<String, String> mParams);

  @POST("editMessageBoard.php")
    Call<EditHoursModel> editMBRequest(@Body Map<String, String> mParams);

  @POST("openCloseShop.php")
    Call<EditHoursModel> editOpenCloseRequest(@Body Map<String, String> mParams);

 @POST("GetProductListing.php")
    Call<ShopListModel> getShopItemsListRequest(@Body Map<String, String> mParams);

 @POST("getBusinessProduct.php")
    Call<ShopListModel> getBusinessShopItemsListRequest(@Body Map<String, String> mParams);

 @POST("getSingleProductDetail.php")
    Call<SingleProductModel> getSingleProductItemsRequest(@Body Map<String, String> mParams);

 @POST("addToCart.php")
    Call<AddToCartModel> addToCartRequest(@Body Map<String, String> mParams);

 @POST("addToWishlist.php")
    Call<AddToCartModel> addToWishlistRequest(@Body Map<String, String> mParams);

 @POST("AddServicesForBusiness.php")
    Call<AddToCartModel> addServiceRequest(@Body Map<String, String> mParams);

 @POST("cartDetails.php")
    Call<CartModel> getCartDetailsRequest(@Body Map<String, String> mParams);

 @POST("quantityIncreaseDecrease.php")
    Call<IncDecQuantityModel> getItemPriceRequest(@Body Map<String, String> mParams);

 @POST("removeCart.php")
    Call<AddToCartModel> deleteProductFromCartRequest(@Body Map<String, String> mParams);

 @POST("removeWishList.php")
    Call<AddToCartModel> removeFavRequest(@Body Map<String, String> mParams);

 @POST("contactUs.php")
    Call<ForgotPasswordModel> contactUsRequest(@Body Map<String, String> mParams);

 @POST("getProfileDetails.php")
    Call<ProfileModel> getProfileDetailsRequest(@Body Map<String, String> mParams);

 @POST("whishlistDetails.php")
    Call<FavoriteModel> getFavListRequest(@Body Map<String, String> mParams);

 @POST("dealPromotion.php")
    Call<DealsModel> getDealsRequest(@Body Map<String, String> mParams);

 @POST("RedeemPointsByUserID.php")
    Call<ReedemPointsModel> getPointsRequest(@Body Map<String, String> mParams);

 @POST("getBusinessProfileDetails.php")
    Call<BusinessDetailModel> getBusinessDetailRequest(@Body Map<String, String> mParams);

 @POST("submitRatingReview.php")
    Call<RatingModel> submitReviewsRequest(@Body Map<String, String> mParams);

    @Multipart
    @POST("GoogleLoginLink.php")
    Call<GoogleLoginLinkModel> loginGoogleLinkRequest(@Part("userID") RequestBody userID);

    @POST("GetRefreshTokenForAndroid.php")
    Call<RefreshTokenModel> refreshTokenRequest(@Body Map<String, String> mParams);

    @POST("businessBookedTimeSlotByDate.php")
    Call<TimeSlotModel> getTimeSlotsRequest(@Body Map<String, String> mParams);


    @POST("getEndTimeByStartTime.php")
    Call<EndTimeModel> getEndSlotsRequest(@Body Map<String, String> mParams);

    @Multipart
    @POST("editProfile.php")
    Call<ForgotPasswordModel> editProfileRequest(@Part("userID") RequestBody userID,
                                                 @Part("address") RequestBody address,
                                                 @Part("bio") RequestBody bio,
                                                 @Part MultipartBody.Part profileImage);



    @POST("bookingAppointment.php")
    Call<BookModel> bookAppointmentRequest(@Body Map<String, String> mParams);

    @POST("bookingAndPurchase.php")
    Call<BookingPurchaseModel> getBookingPurchaseRequest(@Body Map<String, String> mParams);

    @POST("notificationActivity.php")
    Call<NotificationsModel> getNotificationRequest(@Body Map<String, String> mParams);


    @POST("listingReviewRating.php")
    Call<ViewAllReviewsModel> getReviewsRequest(@Body Map<String, String> mParams);

    @Multipart
    @POST("BuyTicketByStripe.php")
    Call<CardModel>addCardStripe(@Part("user_id") RequestBody user_id,
                                   @Part("event_id") RequestBody event_idd,
                                   @Part("ticket_id") RequestBody ticket_id,
                                   @Part("no_of_ticket") RequestBody no_of_tickets,
                                   @Part("stripeToken") RequestBody stripe_token);


    @POST("getCurrentAndOldBooking.php")
    Call<JsonObject> getOldAppRequest(@Body Map<String, String> mParams);

    @POST("getCurrentAndOldBooking.php")
    Call<JsonObject> getCurrentAppRequest(@Body Map<String, String> mParams);

    @POST("applyCoupanCode.php")
    Call<ApplyCouponModel> applyCouponRequest(@Body Map<String, String> mParams);

    @POST("editServiceForBusiness.php")
    Call<ForgotPasswordModel> editServiceRequest(@Body Map<String, String> mParams);


    @POST("deleteServiceForBusiness.php")
    Call<ForgotPasswordModel> deleteServiceRequest(@Body Map<String, String> mParams);

//    @Multipart
//    @POST("bookingAppointment.php")
//    Call<BookModel> bookAppointmentRequest(@Part("userID") RequestBody userID,
//                                           @Part("businessID") RequestBody businessID,
//                                           @Part("startTime") RequestBody startTime,
//                                           @Part("endTime") RequestBody endTime,
//                                           @Part("day") RequestBody day);
}