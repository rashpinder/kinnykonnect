package com.kinnyconnect.app.interfaces;


import com.kinnyconnect.app.model.BusinessDetailModel;

public interface SelectServiceInterface  {
    public void getSelectedService(BusinessDetailModel.BusinessDetail.Service mServicesModel);
}
