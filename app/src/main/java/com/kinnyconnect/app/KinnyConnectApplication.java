package com.kinnyconnect.app;

import android.app.Application;

public class KinnyConnectApplication extends Application
{
        /**
         * Getting the Current Class Name
         */
        public static final String TAG = KinnyConnectApplication.class.getSimpleName();
    /**
     * Initialize the Applications Instance
     */
    private static KinnyConnectApplication mInstance;

    public static synchronized KinnyConnectApplication getInstance()
    {
        return mInstance;
    }
//    /*
//     * Socket
//     * */
//    private Socket mSocket;
//    {
//        try {
//            mSocket = IO.socket(Constants.SocketURL);
//        } catch (URISyntaxException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    public Socket getSocket() {
//        return mSocket;
//    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        mInstance = this;
    }
}
