package com.kinnyconnect.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.ShopItemActivity;
import com.kinnyconnect.app.model.CategoryListingModel;

import java.util.ArrayList;


public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.MyViewHolder> {
    private Context context;
    ArrayList<CategoryListingModel.CategoryDetail> mArrayList = new ArrayList<>();
    ShopAdapter.PaginationInterface paginationInterface;
    int count = 0;

    public interface PaginationInterface {
        public void mPaginationInterface(boolean isLastScrolled);
    }

    public ShopAdapter(Context context,ArrayList<CategoryListingModel.CategoryDetail> mArrayList, ShopAdapter.PaginationInterface paginationInterface) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.paginationInterface = paginationInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_shop_collection_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        CategoryListingModel.CategoryDetail mModel = mArrayList.get(listPosition);

        if (listPosition >= mArrayList.size() - 1) {
            paginationInterface.mPaginationInterface(true);
        }
        if (listPosition % 2 == 0) {
            holder.textTitleTwoTV.setVisibility(View.GONE);
            holder.textTitleOneTV.setVisibility(View.VISIBLE);
            holder.textTitleOneTV.setText(mModel.getTitle());
        } else {
            holder.textTitleTwoTV.setVisibility(View.VISIBLE);
            holder.textTitleOneTV.setVisibility(View.GONE);
            holder.textTitleTwoTV.setText(mModel.getTitle());
        }
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(context)
                .load(mModel.getImage())
                .apply(options)
                .into(holder.imgBeautyIV);
        holder.imgBeautyIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShopItemActivity.class);
                intent.putExtra("category_id", mArrayList.get(listPosition).getCatID());
                intent.putExtra("title", mArrayList.get(listPosition).getTitle());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mArrayList == null) ? 0 : mArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textTitleOneTV;
        TextView textTitleTwoTV;
        ImageView imgBeautyIV;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.textTitleOneTV = itemView.findViewById(R.id.textTitleOneTV);
            this.textTitleTwoTV = itemView.findViewById(R.id.textTitleTwoTV);
            this.imgBeautyIV = itemView.findViewById(R.id.imgBeautyIV);
        }
    }
}
