package com.kinnyconnect.app.adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.model.NotificationsModel;

import java.nio.channels.FileChannel;
import java.util.ArrayList;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private Context context;
    ArrayList<NotificationsModel.Datum> mArrayList ;

    public NotificationAdapter(Context context, ArrayList<NotificationsModel.Datum> mArrayList) {
        this.context = context;
        this.mArrayList = mArrayList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notifications_list, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        NotificationsModel.Datum mModel = mArrayList.get(listPosition);
//        RequestOptions options = new RequestOptions()
//                .placeholder(R.drawable.ic_ph)
//                .error(R.drawable.ic_ph)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .priority(Priority.HIGH)
//                .dontAnimate()
//                .dontTransform();
//        Glide.with(context)
//                .load(mModel.getImage())
//                .apply(options)
//                .into(holder.profileImg);
        holder.txtNotificationTypeTV.setText(mModel.getTitle());
        holder.txtMessageTV.setText(mModel.getMessage());
        long timestampString =  (Long.parseLong(mModel.getCreated()));
        String value = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").
                format(new java.util.Date(timestampString * 1000));
//        String time = DateUtils.formatDateTime(context, Long.parseLong(mModel.getCreated()), DateUtils.FORMAT_SHOW_TIME);
//        String date =  DateUtils.formatDateTime(context, Long.parseLong(mModel.getCreated()), DateUtils.FORMAT_SHOW_DATE);

        holder.txtDateTimeTV.setText(value);
    }

    @Override
    public int getItemCount() {
        return (mArrayList == null) ? 0 : mArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtNotificationTypeTV;
        TextView txtMessageTV;
        TextView txtDateTimeTV;
//        ImageView profileImg;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtNotificationTypeTV = itemView.findViewById(R.id.txtNotificationTypeTV);
            this.txtMessageTV = itemView.findViewById(R.id.txtMessageTV);
//            this.profileImg = itemView.findViewById(R.id.profileImg);
            this.txtDateTimeTV = itemView.findViewById(R.id.txtDateTimeTV);
        }
    }
}
