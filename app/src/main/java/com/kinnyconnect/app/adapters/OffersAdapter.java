package com.kinnyconnect.app.adapters;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.model.SingleProductModel;

import java.util.ArrayList;


public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.MyViewHolder> {
    private Context context;
    ArrayList<SingleProductModel.Data.Offer> mOffersList;

    public OffersAdapter(Context context,ArrayList<SingleProductModel.Data.Offer> mOffersList) {
        this.context = context;
        this.mOffersList = mOffersList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_offer_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        SingleProductModel.Data.Offer mModel = mOffersList.get(listPosition);
        holder.availableOfferTV.setText(mModel.getOffer());

        holder.availableOfferTV.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context,"Link Copied",Toast.LENGTH_SHORT).show();
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", holder.availableOfferTV.getText());
                clipboard.setPrimaryClip(clip);
                return false;
            }
        });

    }


    @Override
    public int getItemCount() {
        return (mOffersList == null) ? 0 : mOffersList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView availableOfferTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.availableOfferTV = itemView.findViewById(R.id.availableOfferTV);
        }
    }

}
