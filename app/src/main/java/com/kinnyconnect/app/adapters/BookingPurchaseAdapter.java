package com.kinnyconnect.app.adapters;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.CartActivity;
import com.kinnyconnect.app.activities.ShopItemDescriptionActivity;
import com.kinnyconnect.app.interfaces.DeleteItemInterface;
import com.kinnyconnect.app.model.BookingPurchaseModel;
import com.kinnyconnect.app.model.FavoriteModel;
import com.kinnyconnect.app.model.SingleProductModel;

import java.util.ArrayList;

import butterknife.BindView;


public class BookingPurchaseAdapter extends RecyclerView.Adapter<BookingPurchaseAdapter.MyViewHolder> {
    private Context context;
    ArrayList<BookingPurchaseModel.Datum> mList;
    RequestOptions options;
    private DeleteItemInterface deleteItemInterface;

    public BookingPurchaseAdapter(Context context,ArrayList<BookingPurchaseModel.Datum> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_booking_puchase_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .skipMemoryCache(true)
                .dontAnimate()
                .dontTransform();
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        BookingPurchaseModel.Datum mModel = mList.get(listPosition);
        Glide.with(context).load(mModel.getImage()).apply(options).into(holder.imgProductIV);
        holder.txtOrderNoTV.setText(mModel.getOrderID());
        holder.txtProductNameTV.setText(mModel.getCatName());
        holder.txtProductTV.setText(mModel.getTitle());
        holder.txtPriceTV.setText(mModel.getPrice());
//        holder.txtDeliverdTV.setText(mModel.getPrice());

    }

    @Override
    public int getItemCount() {
        return (mList == null) ? 0 : mList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtHistoryTV;
        TextView txtOrderNoTV;
        ImageView imgProductIV;
        TextView txtProductNameTV;
        TextView txtProductTV;
        TextView txtPriceTV;
        TextView txtDeliverdTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtHistoryTV = itemView.findViewById(R.id.txtHistoryTV);
            this.txtOrderNoTV = itemView.findViewById(R.id.txtOrderNoTV);
            this.imgProductIV = itemView.findViewById(R.id.imgProductIV);
            this.txtProductNameTV = itemView.findViewById(R.id.txtProductNameTV);
            this.txtProductTV = itemView.findViewById(R.id.txtProductTV);
            this.txtPriceTV = itemView.findViewById(R.id.txtPriceTV);
            this.txtDeliverdTV = itemView.findViewById(R.id.txtDeliverdTV);
        }
    }

}
