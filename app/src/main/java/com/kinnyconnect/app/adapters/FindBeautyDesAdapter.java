package com.kinnyconnect.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.BusinessItemListActivity;
import com.kinnyconnect.app.activities.CategoriesActivity;
import com.kinnyconnect.app.activities.DashboardActivity;
import com.kinnyconnect.app.model.FindBeautyModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;


public class FindBeautyDesAdapter extends RecyclerView.Adapter<FindBeautyDesAdapter.MyViewHolder> {
      private Context context;
    ArrayList<FindBeautyModel.Datum> mArrayList = new ArrayList<>();
    PaginationInterface paginationInterface;
    FindBeautyModel mModel;
    int count = 0;

    public interface PaginationInterface {
        public void mPaginationInterface(boolean isLastScrolled);
    }

    public FindBeautyDesAdapter(Context context, ArrayList<FindBeautyModel.Datum> mArrayList, FindBeautyModel mModel, PaginationInterface paginationInterface) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.mModel = mModel;
        this.paginationInterface = paginationInterface;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_find_beauty_list_two, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
//        FindBeautyModel.Datum mModel = mArrayList.get(listPosition);
//        FindBeautyModel.CategoryListing mCatModel=  mModel.getCategoryListing();
        if (listPosition >= mArrayList.size() - 1) {
            paginationInterface.mPaginationInterface(true);
        }
            holder.txtTitleTV.setText(mModel.getData().get(listPosition).getBusinessName());
            holder.txtLocTV.setText(mModel.getData().get(listPosition).getAddress());
            holder.txtRatingTV.setText(mModel.getData().get(listPosition).getRating());

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(context)
                .load(mModel.getData().get(listPosition).getBusinessImage())
                .apply(options)
                .into(holder.imgBeautyIV);
        if (!mModel.getData().get(listPosition).getRating().equals("")){
        holder.ratingbarRB.setRating(Float.parseFloat(mModel.getData().get(listPosition).getRating()));
        holder.ratingbarRB.setEnabled(false);}
                holder.txtDetailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DashboardActivity.class);
//                intent.putExtra("business_id", mModel.getBusinessID());
//                intent.putExtra("title", mModel.getTitle());
                intent.putExtra("business_id", mModel.getData().get(listPosition).getBusinessID());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }
        });

        holder.mainRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BusinessItemListActivity.class);
                intent.putExtra("category_id", mModel.getCategoryListing().get(listPosition).getCatID());
                intent.putExtra("title", mModel.getCategoryListing().get(listPosition).getTitle());
                intent.putExtra("cat_title", mModel.getData().get(listPosition).getCatTitle());
                intent.putExtra("business_id", mModel.getData().get(listPosition).getBusinessID());
                intent.putExtra("latitude", KinnyConnectPreferences.readString(context,KinnyConnectPreferences.FIN_LAT,null));
                intent.putExtra("longitude", KinnyConnectPreferences.readString(context,KinnyConnectPreferences.FIN_LONG,null));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);


//                Intent intent = new Intent(context, BusinessItemListActivity.class);
//                intent.putExtra("category_id", mModel.getCategoryListing().get(listPosition).getCatID());
//                intent.putExtra("title", mModel.getCategoryListing().get(listPosition).getTitle());
//                intent.putExtra("business_id", mModel.getData().get(listPosition).getBusinessID());
//                intent.putExtra("latitude", KinnyConnectPreferences.readString(context,KinnyConnectPreferences.FIN_LAT,null));
//                intent.putExtra("longitude", KinnyConnectPreferences.readString(context,KinnyConnectPreferences.FIN_LONG,null));
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mArrayList == null) ? 0 : mArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTitleTV;
        TextView txtRatingTV;
        TextView txtLocTV;
        ImageView imgBeautyIV;
        RelativeLayout mainRL;
        LinearLayout locLL;
        TextView txtDetailTV;
        RatingBar ratingbarRB;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtTitleTV = itemView.findViewById(R.id.txtTitleTV);
            this.txtRatingTV = itemView.findViewById(R.id.txtRatingTV);
            this.imgBeautyIV = itemView.findViewById(R.id.imgBeautyIV);
            this.txtLocTV = itemView.findViewById(R.id.txtLocTV);
            this.mainRL = itemView.findViewById(R.id.mainRL);
            this.locLL = itemView.findViewById(R.id.locLL);
            this.txtDetailTV = itemView.findViewById(R.id.txtDetailTV);
            this.ratingbarRB = itemView.findViewById(R.id.ratingbarRB);
        }
    }
}
