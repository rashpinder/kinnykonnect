package com.kinnyconnect.app.adapters;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.CartActivity;
import com.kinnyconnect.app.activities.ShopItemDescriptionActivity;
import com.kinnyconnect.app.interfaces.DeleteItemInterface;
import com.kinnyconnect.app.model.FavoriteModel;
import com.kinnyconnect.app.model.SingleProductModel;

import java.util.ArrayList;


public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.MyViewHolder> {
    private Context context;
    ArrayList<FavoriteModel.Datum> mList;
    RequestOptions options;
    private DeleteItemInterface deleteItemInterface;

    public FavoriteAdapter(Context context,ArrayList<FavoriteModel.Datum> mList,DeleteItemInterface deleteItemInterface) {
        this.context = context;
        this.mList = mList;
        this.deleteItemInterface = deleteItemInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fav_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .skipMemoryCache(true)
                .dontAnimate()
                .dontTransform();
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        FavoriteModel.Datum mModel = mList.get(listPosition);
        Glide.with(context).load(mModel.getImage()).apply(options).into(holder.imgProductIV);
        holder.txtTitleTV.setText(mModel.getTitle());
        holder.txtPriceTV.setText(mModel.getPrice());

//        holder.txtRatingTV.setText(mModel.get());
//        holder.txtTitleTV.setText(mModel.get());
//        holder.txtTitleTV.setText(mModel.get());
        holder.txtDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItemInterface.getData(mModel.getId(),listPosition);
            }
        });

        holder.txtBuyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, ShopItemDescriptionActivity.class);
                intent.putExtra("product_id", mModel.getProductID());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mList == null) ? 0 : mList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTitleTV;
        TextView txtPriceTV;
        TextView txtDeleteTV;
        TextView txtRatingTV;
        TextView txtBuyTV;
        ImageView imgProductIV;
        RatingBar ratingbarRB;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtTitleTV = itemView.findViewById(R.id.txtTitleTV);
            this.imgProductIV = itemView.findViewById(R.id.imgProductIV);
            this.txtPriceTV = itemView.findViewById(R.id.txtPriceTV);
            this.ratingbarRB = itemView.findViewById(R.id.ratingbarRB);
            this.txtDeleteTV = itemView.findViewById(R.id.txtDeleteTV);
            this.txtBuyTV = itemView.findViewById(R.id.txtBuyTV);
            this.txtRatingTV = itemView.findViewById(R.id.txtRatingTV);
        }
    }

}
