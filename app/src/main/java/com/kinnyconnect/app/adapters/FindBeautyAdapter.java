package com.kinnyconnect.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.BusinessItemListActivity;
import com.kinnyconnect.app.activities.FindBeautyActivity;
import com.kinnyconnect.app.activities.HomeActivity;
import com.kinnyconnect.app.activities.LoginActivity;
import com.kinnyconnect.app.activities.ShopItemActivity;
import com.kinnyconnect.app.activities.TitleFindBeautyActivity;
import com.kinnyconnect.app.interfaces.CatInterface;
import com.kinnyconnect.app.interfaces.CatInterface;
import com.kinnyconnect.app.model.CategoryListingModel;

import java.util.ArrayList;


public class FindBeautyAdapter extends RecyclerView.Adapter<FindBeautyAdapter.MyViewHolder> {
    private Context context;
    ArrayList<CategoryListingModel.CategoryDetail> mArrayList = new ArrayList<>();
    String latitude="";
    String longitude="";
//    String business_id="";
    PaginationInterface paginationInterface;
    CatInterface catInterface;
    int count = 0;

    public interface PaginationInterface {
        public void mPaginationInterface(boolean isLastScrolled);
    }

    public FindBeautyAdapter(Context context, ArrayList<CategoryListingModel.CategoryDetail> mArrayList, PaginationInterface paginationInterface, String latitude, String longitude, CatInterface catInterface) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.paginationInterface = paginationInterface;
        this.catInterface = catInterface;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_find_beauty_list, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        CategoryListingModel.CategoryDetail mModel = mArrayList.get(listPosition);

        if (listPosition >= mArrayList.size() - 1) {
            paginationInterface.mPaginationInterface(true);
        }
        if (listPosition % 2 == 0) {
            holder.textTitleTwoTV.setVisibility(View.GONE);
            holder.textTitleOneTV.setVisibility(View.VISIBLE);
            holder.textTitleOneTV.setText(mModel.getTitle());
        } else {
            holder.textTitleTwoTV.setVisibility(View.VISIBLE);
            holder.textTitleOneTV.setVisibility(View.GONE);
            holder.textTitleTwoTV.setText(mModel.getTitle());
        }
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(context)
                .load(mModel.getImage())
                .apply(options)
                .into(holder.imgBeautyIV);

        holder.imgBeautyIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catInterface.getData(mModel.getCatID(),mModel.getTitle(),listPosition,mModel.getImage());
//
//                Intent intent = new Intent(context, HomeActivity.class);
//                intent.putExtra("category_id", mArrayList.get(listPosition).getCatID());
//                intent.putExtra("title", mArrayList.get(listPosition).getTitle());
//                intent.putExtra("pos", listPosition);
//                intent.putExtra("latitude", latitude);
//                intent.putExtra("longitude", longitude);
//                intent.putExtra("img", mArrayList.get(listPosition).getImage());
//                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mArrayList == null) ? 0 : mArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textTitleOneTV;
        TextView textTitleTwoTV;
        ImageView imgBeautyIV;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.textTitleOneTV = itemView.findViewById(R.id.textTitleOneTV);
            this.textTitleTwoTV = itemView.findViewById(R.id.textTitleTwoTV);
            this.imgBeautyIV = itemView.findViewById(R.id.imgBeautyIV);
        }
    }
}
