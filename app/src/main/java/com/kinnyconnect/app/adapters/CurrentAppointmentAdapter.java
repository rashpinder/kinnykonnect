package com.kinnyconnect.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.model.CurrentAppointmentModel;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class CurrentAppointmentAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private static final String TAG = "CurrentAppAdapter";
    private Context context;
    ArrayList<CurrentAppointmentModel> currentAppointmentModelArrayList;
    private LayoutInflater inflater;
    RequestOptions options;

    public CurrentAppointmentAdapter(Context context, ArrayList<CurrentAppointmentModel> currentAppointmentModelArrayList){
        this.context = context;
        this.currentAppointmentModelArrayList = currentAppointmentModelArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return currentAppointmentModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return currentAppointmentModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        CurrentAppointmentModel mModel = currentAppointmentModelArrayList.get(position);
        CurrentAppointmentAdapter.ViewHolder holder;

        if (convertView == null) {
            holder = new CurrentAppointmentAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.item_calender_list, parent, false);
            options = new RequestOptions()
                    .placeholder(R.drawable.ic_ph)
                    .error(R.drawable.ic_ph)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .skipMemoryCache(true)
                    .dontAnimate()
                    .dontTransform();
            holder.txtNameTV = convertView.findViewById(R.id.txtNameTV);
            holder.txtServiceNameTV = convertView.findViewById(R.id.txtServiceNameTV);
            holder.txtTimeTV = convertView.findViewById(R.id.txtTimeTV);
            holder.imgProfilePicCIV = convertView.findViewById(R.id.imgProfilePicCIV);

            convertView.setTag(holder);

        } else {
            holder = (CurrentAppointmentAdapter.ViewHolder) convertView.getTag();
        }
        try {
            Glide.with(context).load(currentAppointmentModelArrayList.get(position).getBusinessImage()).apply(options).into(holder.imgProfilePicCIV);
            holder.txtNameTV.setText(currentAppointmentModelArrayList.get(position).getBusinessName());
            holder.txtServiceNameTV.setText(currentAppointmentModelArrayList.get(position).getServiceName());
            holder.txtTimeTV.setText(currentAppointmentModelArrayList.get(position).getBookingTime());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }


    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        CurrentAppointmentAdapter.HeaderViewHolder holder;
        if (convertView == null) {
            holder = new CurrentAppointmentAdapter.HeaderViewHolder();
            convertView = inflater.inflate(R.layout.header_layout, parent, false);
            holder.headerTV = convertView.findViewById(R.id.headerTV);

            convertView.setTag(holder);
        } else {
            holder = (CurrentAppointmentAdapter.HeaderViewHolder) convertView.getTag();
        }
        if (currentAppointmentModelArrayList.get(position).getCreated() != null && currentAppointmentModelArrayList.get(position).getCreated().length() > 0) {
            String headerText = "" + currentAppointmentModelArrayList.get(position).getCreated();
            holder.headerTV.setText(headerText);
        }
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        if (currentAppointmentModelArrayList.size() > 0)
            return currentAppointmentModelArrayList.get(position).getCreated().subSequence(0, 1).charAt(0);
        else
            return 0;
    }

    class HeaderViewHolder {
        TextView headerTV;
    }

    class ViewHolder {
        TextView txtNameTV, txtServiceNameTV,txtTimeTV;
        ImageView imgProfilePicCIV;
    }
}
