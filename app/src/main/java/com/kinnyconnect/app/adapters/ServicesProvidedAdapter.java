package com.kinnyconnect.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.model.BusinessDetailModel;
import com.kinnyconnect.app.model.FindBeautyModel;
import com.kinnyconnect.app.model.ManageBusinessModel;

import java.util.ArrayList;
import java.util.List;


public class ServicesProvidedAdapter extends RecyclerView.Adapter<ServicesProvidedAdapter.MyViewHolder> {
    private Context context;
    List<BusinessDetailModel.BusinessDetail.Service> mServicesList;

    public ServicesProvidedAdapter(Context context, List<BusinessDetailModel.BusinessDetail.Service> mServicesList) {
        this.context = context;
        this.mServicesList = mServicesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_services_provided_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        BusinessDetailModel.BusinessDetail.Service mModel = mServicesList.get(listPosition);
        holder.txtServicesTV.setText(mModel.getTitle());
    }

    @Override
    public int getItemCount() {
        return (mServicesList == null) ? 0 : mServicesList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLL;
        TextView txtServicesTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.mainLL = itemView.findViewById(R.id.mainLL);
            this.txtServicesTV = itemView.findViewById(R.id.txtServicesTV);
        }
    }
}
