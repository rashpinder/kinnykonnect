package com.kinnyconnect.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.interfaces.EndTimeSlotsInterface;
import com.kinnyconnect.app.interfaces.SelectTimeSlotInterface;
import com.kinnyconnect.app.model.EndTimeModel;
import com.kinnyconnect.app.model.TimeSlotModel;

import java.util.ArrayList;

/**
 * Created by android-da on 6/7/18.
 */

public class EndTimeSlotsAdapter extends RecyclerView.Adapter<EndTimeSlotsAdapter.MyViewHolder> {
    Activity mActivity;
//    ArrayList<TimeSlotModel.FreeSlot> mArrayList;
    String mEndTimeSlot;
    EndTimeSlotsInterface endTimeSlotsInterface;
    private int selectedPosition = -1;// no selection by default

    public EndTimeSlotsAdapter(Activity mActivity, EndTimeSlotsInterface endTimeSlotsInterface,String mEndTimeSlot) {
        this.mActivity = mActivity;
        this.mEndTimeSlot = mEndTimeSlot;
        this.endTimeSlotsInterface = endTimeSlotsInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_time_slot, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        TimeSlotModel.FreeSlot mModel = mArrayList.get(position);
        holder.txtTimeTV.setText(mEndTimeSlot);
        holder.txtTimeTV.setBackgroundResource(R.drawable.bg_timeslot_select);
//        if (selectedPosition == position){
//            holder.txtTimeTV.setBackgroundResource(R.drawable.bg_timeslot_select);
//        }else{
//            holder.txtTimeTV.setBackgroundResource(R.drawable.bg_timeslot_unselect);
//        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endTimeSlotsInterface.getEndTimeSlot(mEndTimeSlot);
                selectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTimeTV;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTimeTV = itemView.findViewById(R.id.txtTimeTV);
        }
    }


}

