package com.kinnyconnect.app.adapters;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.interfaces.DeleteItemInterface;
import com.kinnyconnect.app.interfaces.FavInterface;
import com.kinnyconnect.app.interfaces.IncDecClickInterface;
import com.kinnyconnect.app.model.CartModel;
import java.util.ArrayList;


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    private Context context;
    ArrayList<CartModel.Datum> mCartArrayList;
    RequestOptions options;
    private IncDecClickInterface listener;
    private DeleteItemInterface deleteItemInterface;
    private FavInterface favInterface;

    public CartAdapter(Context context,ArrayList<CartModel.Datum> mCartArrayList,IncDecClickInterface listener,DeleteItemInterface deleteItemInterface,FavInterface favInterface) {
        this.context = context;
        this.mCartArrayList = mCartArrayList;
        this.listener = listener;
        this.deleteItemInterface = deleteItemInterface;
        this.favInterface = favInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cart_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .skipMemoryCache(true)
                .dontAnimate()
                .dontTransform();
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        CartModel.Datum mModel = mCartArrayList.get(listPosition);
        holder.txtTitleTV.setText(mModel.getTitle());
        holder.txtItemCountTV.setText(mModel.getQuantity());
        holder.txtPriceTV.setText(mModel.getPrice());
        Glide.with(context).load(mModel.getImage()).apply(options).into(holder.imgProductIV);
        holder.imgDecIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String count = holder.txtItemCountTV.getText().toString();
                int no = Integer.parseInt(count) - 1;
                if (no >= 1) {
                    holder.txtItemCountTV.setText(no + "");
                    listener.getData(mModel.getProductID(),"2");
                } else {
                    Toast.makeText(context,"Quantity cannot be less than 1",Toast.LENGTH_SHORT).show();
                }


            }
        });

        holder.imgIncIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String count = holder.txtItemCountTV.getText().toString();
                int no = Integer.parseInt(count) + 1;

                holder.txtItemCountTV.setText(no + "");
                listener.getData(mModel.getProductID(),"1");

            }
        });

        holder.txtDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItemInterface.getData(mModel.getCartID(),listPosition);
            }
        });
        holder.txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favInterface.getFavData(mModel.getProductID());
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mCartArrayList == null) ? 0 : mCartArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProductIV;
        ImageView imgDecIV;
        ImageView imgIncIV;
        TextView txtTitleTV;
        TextView txtItemCountTV;
        TextView txtPriceTV;
        TextView txtDeleteTV;
        TextView txtSaveTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.imgProductIV = itemView.findViewById(R.id.imgProductIV);
            this.txtTitleTV = itemView.findViewById(R.id.txtTitleTV);
            this.txtItemCountTV = itemView.findViewById(R.id.txtItemCountTV);
            this.txtPriceTV = itemView.findViewById(R.id.txtPriceTV);
            this.imgDecIV = itemView.findViewById(R.id.imgDecIV);
            this.imgIncIV = itemView.findViewById(R.id.imgIncIV);
            this.txtDeleteTV = itemView.findViewById(R.id.txtDeleteTV);
            this.txtSaveTV = itemView.findViewById(R.id.txtSaveTV);
        }
    }

}
