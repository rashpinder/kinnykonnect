package com.kinnyconnect.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.interfaces.SelectServiceInterface;
import com.kinnyconnect.app.interfaces.ServiceInterface;
import com.kinnyconnect.app.model.BusinessDetailModel;

import java.util.List;

/**
 * Created by android-da on 6/7/18.
 */

public class SelectServiceAdapter extends RecyclerView.Adapter<SelectServiceAdapter.MyViewHolder> {
    Activity mActivity;
    List<BusinessDetailModel.BusinessDetail.Service> mServicesList;
    SelectServiceInterface mSelectServiceInterface;
  ServiceInterface mServiceInterface;
    private int selectedPosition = 0;// no selection by default

    public SelectServiceAdapter(Activity mActivity,    List<BusinessDetailModel.BusinessDetail.Service> mServicesList,SelectServiceInterface mSelectServiceInterface,ServiceInterface mServiceInterface) {
        this.mActivity = mActivity;
        this.mServicesList = mServicesList;
        this.mSelectServiceInterface = mSelectServiceInterface;
        this.mServiceInterface = mServiceInterface;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_select_service, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BusinessDetailModel.BusinessDetail.Service mModel = mServicesList.get(position);
        holder.txtServiceTV.setText(mModel.getTitle());
        holder.txtPriceTV.setText(mModel.getPrice());
if (selectedPosition==0){
//    selectedPosition = holder.getAdapterPosition();
    mSelectServiceInterface.getSelectedService(mServicesList.get(selectedPosition));
//    try {
//        notifyDataSetChanged();
//    } catch (Exception e) {
//    }
}
        if (mServicesList.size() - 1 == position)
            holder.mViewV.setVisibility(View.GONE);
        else
            holder.mViewV.setVisibility(View.VISIBLE);
holder.checkBookCB.setClickable(false);
        holder.checkBookCB.setChecked(selectedPosition == position);

        holder.serviceLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = holder.getAdapterPosition();
                mSelectServiceInterface.getSelectedService(mServicesList.get(selectedPosition));
mServiceInterface.getService(mServicesList.get(selectedPosition));
                try {
                    notifyDataSetChanged();
                } catch (Exception e) {
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mServicesList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox checkBookCB;
        public TextView txtServiceTV;
        public TextView txtPriceTV;
        public View mViewV;
        public View serviceLL;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            checkBookCB = itemView.findViewById(R.id.checkBookCB);
            txtServiceTV = itemView.findViewById(R.id.txtServiceTV);
            mViewV = itemView.findViewById(R.id.mViewV);
            serviceLL = itemView.findViewById(R.id.serviceLL);
            txtPriceTV = itemView.findViewById(R.id.txtPriceTV);
        }
    }


}

