package com.kinnyconnect.app.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.model.ViewAllReviewsModel;

import java.util.ArrayList;


public class ViewAllReviewsAdapter extends RecyclerView.Adapter<ViewAllReviewsAdapter.MyViewHolder> {
    private Context context;
    ViewAllReviewsAdapter.PaginationInterface paginationInterface;
    ArrayList<ViewAllReviewsModel.Datum> mArrayList ;

    public interface PaginationInterface {
        public void mPaginationInterface(boolean isLastScrolled);
    }


    public ViewAllReviewsAdapter(Context context, ArrayList<ViewAllReviewsModel.Datum> mArrayList, ViewAllReviewsAdapter.PaginationInterface paginationInterface) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.paginationInterface = paginationInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_services_provided_list, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        ViewAllReviewsModel.Datum mModel = mArrayList.get(listPosition);
        if (listPosition >= mArrayList.size() - 1) {
            paginationInterface.mPaginationInterface(true);
        }
        holder.txtServicesTV.setText(mModel.getReview()+" - "+mModel.getName());

    }

    @Override
    public int getItemCount() {
        return (mArrayList == null) ? 0 : mArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLL;
        TextView txtServicesTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.mainLL = itemView.findViewById(R.id.mainLL);
            this.txtServicesTV = itemView.findViewById(R.id.txtServicesTV);
        }
    }
}
