package com.kinnyconnect.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.ShopItemActivity;
import com.kinnyconnect.app.activities.ShopItemDescriptionActivity;
import com.kinnyconnect.app.model.CategoryListingModel;
import com.kinnyconnect.app.model.ShopListModel;

import java.util.ArrayList;


public class ShopItemAdapter extends RecyclerView.Adapter<ShopItemAdapter.MyViewHolder> {
    private Context context;
    ShopItemAdapter.PaginationInterface paginationInterface;
    ArrayList<ShopListModel.ProductDetail> mArrayList ;

    public interface PaginationInterface {
        public void mPaginationInterface(boolean isLastScrolled);
    }


    public ShopItemAdapter(Context context, ArrayList<ShopListModel.ProductDetail> mArrayList, ShopItemAdapter.PaginationInterface paginationInterface) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.paginationInterface = paginationInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_shop_list, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        ShopListModel.ProductDetail mModel = mArrayList.get(listPosition);
        if (listPosition >= mArrayList.size() - 1) {
            paginationInterface.mPaginationInterface(true);
        }
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.ic_ph)
                .error(R.drawable.ic_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        Glide.with(context)
                .load(mModel.getImage())
                .apply(options)
                .into(holder.imgShopItemIV);
        holder.txtItemPriceTV.setText(mModel.getPrice());
        holder.txtItemTV.setText(mModel.getTitle());
        holder.itemRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShopItemDescriptionActivity.class);
                intent.putExtra("product_id", mModel.getProductID());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mArrayList == null) ? 0 : mArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtItemTV;
        TextView txtItemPriceTV;
        ImageView imgShopItemIV;
        RelativeLayout itemRL;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtItemTV = itemView.findViewById(R.id.txtItemTV);
            this.txtItemPriceTV = itemView.findViewById(R.id.txtItemPriceTV);
            this.imgShopItemIV = itemView.findViewById(R.id.imgShopItemIV);
            this.itemRL = itemView.findViewById(R.id.itemRL);
        }
    }
}
