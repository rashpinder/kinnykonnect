package com.kinnyconnect.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.model.BusinessDetailModel;

import java.util.List;


public class ShareExperienceAdapter extends RecyclerView.Adapter<ShareExperienceAdapter.MyViewHolder> {
    private Context context;
    List<BusinessDetailModel.BusinessDetail.ShareExperience> mShareExperienceList;

    public ShareExperienceAdapter(Context context, List<BusinessDetailModel.BusinessDetail.ShareExperience> mShareExperienceList) {
        this.context = context;
        this.mShareExperienceList = mShareExperienceList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_services_provided_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        BusinessDetailModel.BusinessDetail.ShareExperience mModel = mShareExperienceList.get(listPosition);
        holder.txtServicesTV.setText(mModel.getReview()+" - "+mModel.getName());

    }

    @Override
    public int getItemCount() {
        return (mShareExperienceList == null) ? 0 : mShareExperienceList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLL;
        TextView txtServicesTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.mainLL = itemView.findViewById(R.id.mainLL);
            this.txtServicesTV = itemView.findViewById(R.id.txtServicesTV);
        }
    }
}
