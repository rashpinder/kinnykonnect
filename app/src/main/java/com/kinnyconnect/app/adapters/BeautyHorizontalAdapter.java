package com.kinnyconnect.app.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.fragments.HomeFragment;
import com.kinnyconnect.app.interfaces.FindBeautyInterface;
import com.kinnyconnect.app.model.FindBeautyModel;

import java.util.ArrayList;


public class BeautyHorizontalAdapter extends RecyclerView.Adapter<BeautyHorizontalAdapter.SingleViewHolder> {
    private Context context;
    ArrayList<FindBeautyModel.CategoryListing> mArrayList = new ArrayList<>();
    private FindBeautyInterface listener;
    TextView textView;
    private int checkedPosition;
    private String img;

    public BeautyHorizontalAdapter(Context context, ArrayList<FindBeautyModel.CategoryListing> mArrayList, FindBeautyInterface listener, TextView textView, int checkedPosition,String img) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.listener = listener;
        this.textView = textView;
        this.checkedPosition = checkedPosition;
        this.img = img;
    }

    @Override
    public SingleViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_find_beauty_actvity_list, parent, false);
        textView = HomeFragment.txtTitleTV;
        SingleViewHolder myViewHolder = new SingleViewHolder(view);
        return myViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final SingleViewHolder holder, final int listPosition) {
        holder.bind(mArrayList.get(listPosition));
    }

    @Override
    public int getItemCount() {
        return (mArrayList == null) ? 0 : mArrayList.size();
    }


    class SingleViewHolder extends RecyclerView.ViewHolder {

        TextView itemBeautyTV;
        RelativeLayout beautyRL;
        View view;

        SingleViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemBeautyTV = itemView.findViewById(R.id.itemBeautyTV);
            this.beautyRL = itemView.findViewById(R.id.beautyRL);
            this.view = itemView.findViewById(R.id.view);
        }

        void bind(final FindBeautyModel.CategoryListing mArrayList) {
            if (checkedPosition == -1) {
                view.setVisibility(View.INVISIBLE);
            } else {
                if (checkedPosition == getAdapterPosition()) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.INVISIBLE);
                }
            }
            itemBeautyTV.setText(mArrayList.getTitle());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    view.setVisibility(View.VISIBLE);
                    if (checkedPosition != getAdapterPosition()) {
                        notifyItemChanged(checkedPosition);
                        checkedPosition = getAdapterPosition();
                    }
                    listener.getData(mArrayList.getCatID(), mArrayList.getTitle(), checkedPosition,mArrayList.getImage());
                }
            });
        }
    }

    public FindBeautyModel.CategoryListing getSelected() {
        if (checkedPosition != -1) {
            return mArrayList.get(checkedPosition);
        }
        return null;
    }
}
