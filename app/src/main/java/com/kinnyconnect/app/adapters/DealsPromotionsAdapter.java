package com.kinnyconnect.app.adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.model.DealsModel;

import java.util.ArrayList;


public class DealsPromotionsAdapter extends RecyclerView.Adapter<DealsPromotionsAdapter.MyViewHolder> {
    private final Context context;
    ArrayList<DealsModel.Datum> mList;


    public DealsPromotionsAdapter(Context context, ArrayList<DealsModel.Datum> mList) {
        this.context = context;
        this.mList = mList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_deals_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        DealsModel.Datum mModel = mList.get(listPosition);
        holder.txtPercentTV.setText(mModel.getTitle());
        holder.txtOfferTV.setText(mModel.getDescription());
        holder.txtOfferCodeTV.setText(mModel.getCoupanCode());
        holder.txtDateTV.setText(mModel.getExpireDate());

        holder.txtOfferCodeTV.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context, "Link Copied", Toast.LENGTH_SHORT).show();
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", holder.txtOfferCodeTV.getText());
                clipboard.setPrimaryClip(clip);
                return false;
            }
        });
//        holder.txtOfferCodeTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context,"Link Copied",Toast.LENGTH_SHORT).show();
//                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
//                ClipData clip = ClipData.newPlainText("label", holder.txtOfferCodeTV.getText());
//                clipboard.setPrimaryClip(clip);
//            }
//        });


        holder.txtCopyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Link Copied", Toast.LENGTH_SHORT).show();
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", holder.txtOfferCodeTV.getText());
                clipboard.setPrimaryClip(clip);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mList == null) ? 0 : mList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textDetailsTV;
        TextView txtPercentTV;
        TextView txtOfferTV;
        TextView txtOfferCodeTV;
        TextView txtDateTV;
        TextView txtTimeTV;
        TextView txtCopyTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textDetailsTV = itemView.findViewById(R.id.textDetailsTV);
            this.txtPercentTV = itemView.findViewById(R.id.txtPercentTV);
            this.txtOfferTV = itemView.findViewById(R.id.txtOfferTV);
            this.txtOfferCodeTV = itemView.findViewById(R.id.txtOfferCodeTV);
            this.txtDateTV = itemView.findViewById(R.id.txtDateTV);
            this.txtTimeTV = itemView.findViewById(R.id.txtTimeTV);
            this.txtCopyTV = itemView.findViewById(R.id.txtCopyTV);
        }
    }

}
