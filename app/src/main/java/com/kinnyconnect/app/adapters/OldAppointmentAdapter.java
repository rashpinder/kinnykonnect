package com.kinnyconnect.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.model.OldAppointmentModel;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class OldAppointmentAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private static final String TAG = "CurrentAppAdapter";
    private Context context;
    ArrayList<OldAppointmentModel> oldAppointmentModelArrayList;
    private LayoutInflater inflater;
    RequestOptions options;

    public OldAppointmentAdapter(Context context, ArrayList<OldAppointmentModel> oldAppointmentModelArrayList){
        this.context = context;
        this.oldAppointmentModelArrayList = oldAppointmentModelArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return oldAppointmentModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return oldAppointmentModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        OldAppointmentModel mModel = oldAppointmentModelArrayList.get(position);
        OldAppointmentAdapter.ViewHolder holder;

        if (convertView == null) {
            holder = new OldAppointmentAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.item_calender_list, parent, false);
            options = new RequestOptions()
                    .placeholder(R.drawable.ic_ph)
                    .error(R.drawable.ic_ph)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .skipMemoryCache(true)
                    .dontAnimate()
                    .dontTransform();
            holder.txtNameTV = convertView.findViewById(R.id.txtNameTV);
            holder.txtServiceNameTV = convertView.findViewById(R.id.txtServiceNameTV);
            holder.txtTimeTV = convertView.findViewById(R.id.txtTimeTV);
            holder.imgProfilePicCIV = convertView.findViewById(R.id.imgProfilePicCIV);

            convertView.setTag(holder);

        } else {
            holder = (OldAppointmentAdapter.ViewHolder) convertView.getTag();
        }
        try {
            Glide.with(context).load(oldAppointmentModelArrayList.get(position).getBusinessImage()).apply(options).into(holder.imgProfilePicCIV);
            holder.txtNameTV.setText(oldAppointmentModelArrayList.get(position).getBusinessName());
            holder.txtServiceNameTV.setText(oldAppointmentModelArrayList.get(position).getServiceName());
            holder.txtTimeTV.setText(oldAppointmentModelArrayList.get(position).getBookingTime());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }


    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        OldAppointmentAdapter.HeaderViewHolder holder;
        if (convertView == null) {
            holder = new OldAppointmentAdapter.HeaderViewHolder();
            convertView = inflater.inflate(R.layout.header_layout, parent, false);
            holder.headerTV = convertView.findViewById(R.id.headerTV);

            convertView.setTag(holder);
        } else {
            holder = (OldAppointmentAdapter.HeaderViewHolder) convertView.getTag();
        }
        if (oldAppointmentModelArrayList.get(position).getCreated() != null && oldAppointmentModelArrayList.get(position).getCreated().length() > 0) {
            String headerText = "" + oldAppointmentModelArrayList.get(position).getCreated();
            holder.headerTV.setText(headerText);
        }
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        //return the first character of the country as ID because this is what headers are based upon
        if (oldAppointmentModelArrayList.size() > 0)
            return oldAppointmentModelArrayList.get(position).getCreated().subSequence(0, 1).charAt(0);
        else
            return 0;
    }

    class HeaderViewHolder {
        TextView headerTV;
    }

    class ViewHolder {
        TextView txtNameTV, txtServiceNameTV,txtTimeTV;
        ImageView imgProfilePicCIV;
    }
}
