package com.kinnyconnect.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.activities.EditServiceActivity;
import com.kinnyconnect.app.interfaces.DeleteItemInterface;
import com.kinnyconnect.app.model.CategoryListingModel;
import com.kinnyconnect.app.model.ManageBusinessModel;

import java.util.ArrayList;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.MyViewHolder> {
    private Context context;
    ArrayList<ManageBusinessModel.Datum.Service> mServiceArrayList;
    private DeleteItemInterface deleteItemInterface;

    public ServicesAdapter(Context context,ArrayList<ManageBusinessModel.Datum.Service> mServiceArrayList,DeleteItemInterface deleteItemInterface) {
        this.context = context;
        this.mServiceArrayList = mServiceArrayList;
        this.deleteItemInterface = deleteItemInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_services_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        ManageBusinessModel.Datum.Service mModel = mServiceArrayList.get(listPosition);
        holder.txtServiceNameTV.setText(mModel.getTitle());
        holder.txtDescription.setText(mModel.getDescription());
        holder.txtTimeTV.setText(mModel.getTime());
        holder.txtPriceTV.setText(mModel.getPrice());
        holder.imgEditIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, EditServiceActivity.class);
                intent.putExtra("service_name",mModel.getTitle());
                intent.putExtra("service_desc",mModel.getDescription());
                intent.putExtra("service_time",mModel.getTime());
                intent.putExtra("service_price",mModel.getPrice());
                intent.putExtra("service_id",mModel.getSerID());
                intent.putExtra("pos",listPosition);
                context.startActivity(intent);

            }
        });
        holder.imgDeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItemInterface.getData(mModel.getSerID(),listPosition);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mServiceArrayList == null) ? 0 : mServiceArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtServiceNameTV;
        TextView txtTimeTV;
        TextView txtDescription;
        TextView txtPriceTV;
        ImageView imgDeleteIV;
        ImageView imgEditIV;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtServiceNameTV = itemView.findViewById(R.id.txtServiceNameTV);
            this.txtTimeTV = itemView.findViewById(R.id.txtTimeTV);
            this.txtDescription = itemView.findViewById(R.id.txtDescription);
            this.txtPriceTV = itemView.findViewById(R.id.txtPriceTV);
            this.imgDeleteIV = itemView.findViewById(R.id.imgDeleteIV);
            this.imgEditIV = itemView.findViewById(R.id.imgEditIV);
        }
    }
}
