package com.kinnyconnect.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.CartAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.DeleteItemInterface;
import com.kinnyconnect.app.interfaces.FavInterface;
import com.kinnyconnect.app.interfaces.IncDecClickInterface;
import com.kinnyconnect.app.model.AddToCartModel;
import com.kinnyconnect.app.model.ApplyCouponModel;
import com.kinnyconnect.app.model.CartModel;
import com.kinnyconnect.app.model.IncDecQuantityModel;
import com.kinnyconnect.app.utils.Constants;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends BaseActivity implements IncDecClickInterface, DeleteItemInterface, FavInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = CartActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = CartActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.cartRV)
    RecyclerView cartRV;
    @BindView(R.id.totalRL)
    RelativeLayout totalRL;
    @BindView(R.id.txtPriceTV)
    TextView txtPriceTV;
    @BindView(R.id.txtDollarTV)
    TextView txtDollarTV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.txtProceedTV)
    TextView txtProceedTV;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    @BindView(R.id.txtApplyCouponTV)
    TextView txtApplyCouponTV;
    @BindView(R.id.txtTotalDiscountTV)
    TextView txtTotalDiscountTV;
    @BindView(R.id.txtTotalPriceTV)
    TextView txtTotalPriceTV;
    @BindView(R.id.txtCouponTV)
    TextView txtCouponTV;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    List<CartModel.Datum> mCartArrayList;
    CartAdapter mCartAdapter;
    private IncDecClickInterface listener;
    private DeleteItemInterface deleteItemInterface;
    private FavInterface favInterface;
    String productID;
    String type;
    String cartID;
    String value;
    String total_price;
    int pos;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        listener = this;
        deleteItemInterface = this;
        favInterface = this;
        getIntentData();
        getCartDetails();
        scrollView.setOnTouchListener((v, event) -> {
            hideKeyBoard(mActivity,getCurrentFocus());
            return true;
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("value") != null) {
                value = (String) getIntent().getExtras().get("value");
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setCartAdapter() {
        mCartAdapter = new CartAdapter(mActivity, (ArrayList<CartModel.Datum>) mCartArrayList, listener, deleteItemInterface, favInterface);
        cartRV.setLayoutManager(new LinearLayoutManager(mActivity));
        cartRV.setAdapter(mCartAdapter);
        mCartAdapter.notifyDataSetChanged();
    }


    @OnClick({R.id.txtProceedTV, R.id.imgBackIV,R.id.txtApplyCouponTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtProceedTV:
                performProceedClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;
                case R.id.txtApplyCouponTV:
                performApplyClick();
                break;

        }
    }

    private void performApplyClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            if (isValidate()) {
            executeApplyCouponApi();
        }}
    }

    public boolean isValidate() {
        boolean flag = true;
        if (txtCouponTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_coupon_code));
            flag = false;
        }
        return flag;
    }


    private void performProceedClick() {
//        Intent intent=new Intent(mActivity,PaymentActivity.class);
//       intent.putExtra(Constants.TOTAL_PRICE,total_price);
//        startActivity(intent);
    }

    private void performBackClick() {

        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (value.equals("account")){
//            Intent intent = new Intent(mActivity, DrawerActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//            finish();
//        }
//        else {
            super.onBackPressed();
//        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getCartDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetCartDetailsApi();
        }
    }

    private void executeGetCartDetailsApi() {
        if (mCartArrayList != null) {
            mCartArrayList.clear();
        }
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getCartDetailsRequest(mParams()).enqueue(new Callback<CartModel>() {
            @Override
            public void onResponse(Call<CartModel> call, Response<CartModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                CartModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mCartArrayList = new ArrayList<>();
                    mCartArrayList = mModel.getData();
                    txtPriceTV.setText(mModel.getTotalprice());
                    txtNoDataFountTV.setVisibility(View.GONE);
                    setCartAdapter();
                } else {
//                    showAlertDialog(mActivity, mModel.getMessage());
//                    showToast(mActivity,mModel.getMessage());
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                    if (mCartArrayList != null) {
                        mCartAdapter.notifyDataSetChanged();
                    }
                    totalRL.setVisibility(View.GONE);
                    txtProceedTV.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CartModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Execute api
     * */
    private Map<String, String> mCouponParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        mMap.put("coupanCode", txtCouponTV.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeApplyCouponApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.applyCouponRequest(mCouponParams()).enqueue(new Callback<ApplyCouponModel>() {
            @Override
            public void onResponse(Call<ApplyCouponModel> call, Response<ApplyCouponModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ApplyCouponModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity,mModel.getMessage());
                    txtTotalPriceTV.setText(mModel.getTotal());
                    total_price=mModel.getTotal();
                    txtTotalDiscountTV.setText(mModel.getDiscount());
                } else {
                    showAlertDialog(mActivity,mModel.getMessage());
                    txtTotalDiscountTV.setText("");
                    txtTotalPriceTV.setText("");
                }
            }

            @Override
            public void onFailure(Call<ApplyCouponModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void getData(String productID, String type) {
        this.productID = productID;
        this.type = type;
        getPriceDetails();
    }

    private void getPriceDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetPriceDetailsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mIncDecParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        mMap.put("productID", productID);
        mMap.put("type", type);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetPriceDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getItemPriceRequest(mIncDecParams()).enqueue(new Callback<IncDecQuantityModel>() {
            @Override
            public void onResponse(Call<IncDecQuantityModel> call, Response<IncDecQuantityModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                IncDecQuantityModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    getCartDetails();
//                    showAlertDialog(mActivity, mModel.getMessage());
                } else {
//                    showAlertDialog(mActivity, mModel.getMessage());
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<IncDecQuantityModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void getData(String cartID,int pos) {
        this.cartID = cartID;
        this.pos = pos;
        deleteCartItem();
    }

    private void deleteCartItem() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeDeleteCartItemApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mDeleteItemParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("cartID", cartID);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeDeleteCartItemApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteProductFromCartRequest(mDeleteItemParams()).enqueue(new Callback<AddToCartModel>() {
            @Override
            public void onResponse(Call<AddToCartModel> call, Response<AddToCartModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                AddToCartModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mCartArrayList.remove(pos);
                    mCartAdapter.notifyDataSetChanged();
//                    showAlertDialog(mActivity, mModel.getMessage());
//                    getCartDetails();

                } else {
                    showToast(mActivity, mModel.getMessage());
//                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddToCartModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void getFavData(String productID) {
        this.productID = productID;
        performFavorite();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mFavParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        mMap.put("productID", productID);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performFavorite() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeAddToFavApi();
        }
    }


    private void executeAddToFavApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addToWishlistRequest(mFavParams()).enqueue(new Callback<AddToCartModel>() {
            @Override
            public void onResponse(Call<AddToCartModel> call, Response<AddToCartModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                AddToCartModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
//                    imgFavIV.setImageResource(R.drawable.ic_fav_active);
                } else {
//                    showAlertDialog(mActivity, mModel.getMessage());
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddToCartModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}
