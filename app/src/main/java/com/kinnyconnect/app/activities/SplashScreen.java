package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.utils.WelcomePrefrences;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashScreen extends BaseActivity {
    /*
     * Initlaize Activity...
     * */
    Activity mActivity = SplashScreen.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = SplashScreen.this.getClass().getSimpleName();
    @BindView(R.id.txtEnterTV)
    TextView txtEnterTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
    }
    @OnClick({R.id.txtEnterTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtEnterTV:
                performEnterClick();
                break;}}

    private void performEnterClick() {
        WelcomePrefrences.writeBoolean(mActivity, WelcomePrefrences.REMEMBER, true);
        Intent i = new Intent(SplashScreen.this, DrawerActivity.class);
        startActivity(i);
        finish();
    }

}
