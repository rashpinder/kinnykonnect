package com.kinnyconnect.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.activities.BaseActivity;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.ForgotPasswordModel;
import com.kinnyconnect.app.model.ProfileModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = EditProfileActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = EditProfileActivity.this;

    /*
     * Widgets
     * */

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;

    @BindView(R.id.txtNameTV)
    TextView txtNameTV;

    @BindView(R.id.imgProfileIV)
    ImageView imgProfileIV;

    @BindView(R.id.profileFL)
    FrameLayout profileFL;

    @BindView(R.id.editBioET)
    EditText editBioET;

    @BindView(R.id.editAddressET)
    EditText editAddressET;

    @BindView(R.id.txtUpdateTV)
    TextView txtUpdateTV;
    @BindView(R.id.scroll)
    ScrollView scroll;

    Bitmap selectedImage;

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setStatusBar(mActivity);
        ButterKnife.bind(this);
        getUserProfileDetails();
        scroll.setOnTouchListener((v, event) -> {
            hideKeyBoard(mActivity,getCurrentFocus());
            return true;
        });
    }


    private void getUserProfileDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetUserDetailsApi();
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetUserDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProfileDetailsRequest(mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ProfileModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    String imageurl = mModel.getUserDetails().getProfileImage();
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();
                    if (imageurl != null) {
                        Glide.with(mActivity)
                                .load(imageurl)
                                .apply(options)
                                .into(imgProfileIV);
                    } else {
                        String img = KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.IMAGE, null);
                        Glide.with(mActivity)
                                .load(img)
                                .apply(options)
                                .into(imgProfileIV);
                    }
                    txtNameTV.setText(mModel.getUserDetails().getName());
                    editBioET.setText(mModel.getUserDetails().getBio());
                    editAddressET.setText(mModel.getUserDetails().getAddress());
                } else {
                    showAlertDialog(mActivity, "No Data Available");
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @OnClick({R.id.imgBackIV, R.id.txtUpdateTV, R.id.profileFL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
            case R.id.txtUpdateTV:
                performUpdateClick();
                break;
            case R.id.profileFL:
                performProfileClick();
                break;
        }
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /* Camera Gallery functionality
     * */
    private void performProfileClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {

                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), selectedImage,"IMG_" + Calendar.getInstance().getTime(), null);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .skipMemoryCache(true)
                            .dontAnimate()
                            .dontTransform();

                    if (path != null) {
                        Glide.with(mActivity).load(path)
                                .apply(options)
                                .into(imgProfileIV);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity,"Cropping failed: " + result.getError());
            }
        }
    }

    private void performUpdateClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeUpdateProfileApi();
            }
        }
    }

    private void executeUpdateProfileApi() {
        showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;

        if (selectedImage != null) {
            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(selectedImage));
            mMultipartBody1 = MultipartBody.Part.createFormData("profileImage", getAlphaNumericString() + ".jpg", requestFile1);
            Log.e("img","img"+mMultipartBody1);
        }
        RequestBody userID = RequestBody.create(MediaType.parse("multipart/form-data"), KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        RequestBody address = RequestBody.create(MediaType.parse("multipart/form-data"), editAddressET.getText().toString().trim());
        RequestBody bio = RequestBody.create(MediaType.parse("multipart/form-data"), editBioET.getText().toString().trim());
//        RequestBody name = RequestBody.create(MediaType.parse("multipart/form-data"), txtNameTV.getText().toString().trim());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface.editProfileRequest(userID,address,bio,mMultipartBody1).enqueue(new Callback<ForgotPasswordModel>() {

            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ForgotPasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.e(TAG, "value--" + byteArray);
        return byteArray;
    }

    // generate dynamically string
    public String getAlphaNumericString() {
        int n = 20;

// chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

// create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

// generate a random number between
// 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

// add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    /*
     * Set up validations
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editAddressET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_address));
            flag = false;
        } else if (editBioET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_bio));
            flag = false;
        } else if (editBioET.getText().toString().trim().length()>100) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_bio));
            flag = false;
        }

        return flag;
    }
}
