package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.kinnyconnect.app.R;
import com.kinnyconnect.app.fragments.CurrentAppointmentFragment;
import com.kinnyconnect.app.fragments.OldAppointmentFragment;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingAppointmentsActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = BookingAppointmentsActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = BookingAppointmentsActivity.this;


    /**
     * Widgets
     */
    @BindView(R.id.layoutCurrentAppLL)
    LinearLayout layoutCurrentAppLL;
    @BindView(R.id.currentAppView)
    View currentAppView;
    @BindView(R.id.layoutOldAppLL)
    LinearLayout layoutOldAppLL;
    @BindView(R.id.oldAppView)
    View oldAppView;
    @BindView(R.id.containerAppointFL)
    FrameLayout containerAppointFL;
    @BindView(R.id.txtOldAppTV)
    TextView txtOldAppTV;
    @BindView(R.id.txtCurrentAppTV)
    TextView txtCurrentAppTV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;



    /*
     * Activity Override method
     * #onActivityCreated
     **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_appointments);
        //butter knife
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        performCurrentAppClick();
    }


    @OnClick({R.id.layoutCurrentAppLL, R.id.layoutOldAppLL,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layoutCurrentAppLL:
                performCurrentAppClick();
                break;
            case R.id.layoutOldAppLL:
                performOldAppClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }

    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent mIntent = new Intent(mActivity, DrawerActivity.class);
//        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(mIntent);
    }

    private void performCurrentAppClick() {
        txtCurrentAppTV.setTextColor(getResources().getColor(R.color.colorBtn));
        currentAppView.setBackgroundColor(getResources().getColor(R.color.colorBtn));
        txtOldAppTV.setTextColor(getResources().getColor(R.color.colorBlack));
        oldAppView.setBackgroundColor(getResources().getColor(R.color.colorBlack));
        switchFragment(mActivity,new CurrentAppointmentFragment(),false,null);
    }

    private void performOldAppClick() {
        txtCurrentAppTV.setTextColor(getResources().getColor(R.color.colorBlack));
        currentAppView.setBackgroundColor(getResources().getColor(R.color.colorBlack));
        txtOldAppTV.setTextColor(getResources().getColor(R.color.colorBtn));
        oldAppView.setBackgroundColor(getResources().getColor(R.color.colorBtn));
        switchFragment(mActivity,new OldAppointmentFragment(),false,null);
    }


    /********
     *Replace Fragment In Activity
     **********/
    private void switchFragment(Activity activity, Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerAppointFL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }
}
