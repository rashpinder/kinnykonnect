package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    /*
     * Get Class Name
     * */
    String TAG = BaseActivity.this.getClass().getSimpleName();
    /*
     * Initialize Activity
     * */
    Activity mActivity = BaseActivity.this;

    /*
     * Initialize Other Classes Objects...
     * */
    public Dialog progressDialog;
    private long mLastClickTab1 = 0;
    /*
     * Get Is User Login
     * */
    public Boolean IsUserLogin() {
        return KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, false);
    }

    /*
     * Get User ID
     * */
    public String getID() {
        return KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, "");
    }

    /*
     * Get User Email ID
     * */
    public String getUserEmailID() {
        return KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.EMAIL, "");
    }

    /*
     * Get User Name
     * */
    public String getUserName() {
        return KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.NAME, "");
    }

    /*
     * Get User Profile Picture
     * */
    public String getUserProfilePicture() {
        return KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.IMAGE, "");
    }


    /*
    transparent status bar
     */
    public void setStatusBar(Activity mActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            // edited here
            mActivity.getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }
    }

    public void preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return;
        }
        mLastClickTab1 = SystemClock.elapsedRealtime();
    }

 public void preventFbMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return;
        }
        mLastClickTab1 = SystemClock.elapsedRealtime();
    }

    /*
     * Finish the activity
     * */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionSlideDownExit();
    }
    /*
     * Compare two dates for Appointment Done
     * */
    public boolean IsCalenderAvailable(String appointmentDate) {
        boolean isDateConfirm = false;
        java.text.DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date dateobj = new Date();
        String currentDate = df.format(dateobj);

        Log.e(TAG, "***Current Date***" + currentDate);
        Log.e(TAG, "***Appointment Date***" + appointmentDate);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date mAppointmentDate = sdf.parse(appointmentDate);
            Date mCurrentDate = sdf.parse(currentDate);

            if (mAppointmentDate.after(mCurrentDate)) {
                isDateConfirm = true;
                Log.e(TAG, "Date1 is after Date2");
            }

            if (mAppointmentDate.before(mCurrentDate)) {
                isDateConfirm = false;
                Log.e(TAG, "Date1 is before Date2");
            }

            if (mAppointmentDate.equals(mCurrentDate)) {
                isDateConfirm = false;
                Log.e(TAG, "Date1 is equal Date2");
            }
        } catch (Exception e) {
            Log.e(TAG, "****ERROR****" + e.toString());
        }


        return isDateConfirm;
    }

    /*
     * Get Day Full Name
     * */
    public String convertTimeToDayName(String input) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy/MM/dd");

        Date date = null;
        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
        }

        SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
        String goal = outFormat.format(date);

        return goal;
    }
    /*
     * To Start the New Activity
     * */
    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionSlideUPEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    public void overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    public void overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down);
    }


    /*
     * Show Progress Dialog
     * */

    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        if (progressDialog != null)
            progressDialog.show();
    }


    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showLoginAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, LoginActivity.class));
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }
    /*
     * Hide Keyboard
     * */
    public boolean hideKeyBoard(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }


    /*to switch between fragments*/
    public void switchFragment(final Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameL, fragment, Tag);
            if (addToStack)
                fragmentTransaction.addToBackStack(Tag);
            if (bundle != null)
                fragment.setArguments(bundle);
            fragmentTransaction.commit();
            fragmentManager.executePendingTransactions();
        }
    }

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    /*
     * Share Gmail Intent
     * */
    public void shareIntentGmail(Activity mActivity, String profileUrl) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Deal Breaker:");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out friend profile and share your view : " + profileUrl);
        mActivity.startActivity(Intent.createChooser(sharingIntent, "Choose One"));
    }

    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /*
     * Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialogFinish(final Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }


    public String formatException(Exception exception) {
        String formattedString = "Internal Error";
        Log.e("BaseActivity", " -- Error: " + exception.toString());
        Log.getStackTraceString(exception);

        String temp = exception.getMessage();

        if (temp != null && temp.length() > 0) {
            formattedString = temp.split("\\(")[0];
            if (temp != null && temp.length() > 0) {
                return formattedString;
            }
        }

        return formattedString;
    }


    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialog(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);
                    }
                }, mYear, mMonth, mDay);

//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 568025136000L);

        datePickerDialog.show();
    }


    public boolean isValidDateOfBirth(TextView mTextView) {
        boolean isDateValid = false;
        String dob = mTextView.getText().toString().trim();

        long mTimeInLong = getTimeInLong(dob);
        long difference = System.currentTimeMillis() - mTimeInLong;
        long dy = TimeUnit.MILLISECONDS.toDays(difference);

        final long years = dy / 365;

        isDateValid = years >= 18;
        Log.e(TAG, "isValidDateOfBirth: " + isDateValid);
        Log.e(TAG, "isValidDateOfBirth: " + years);

        return isDateValid;
    }


    private long getTimeInLong(String mDate) {
        long startDate = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(mDate);

            startDate = date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return startDate;
    }


    public String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }


    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public String getCurrentDate() {
        Date d = new Date();
        CharSequence mCurrentDate = DateFormat.format("dd/mm/yyyy", d.getTime());
        return mCurrentDate.toString();
    }


    public void showTimePicker(TextView mTextview)
    {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        int am = mcurrentTime.get(Calendar.AM_PM);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(mActivity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//    String am_pm;
//    if (selectedHour >= 12) {
//    am_pm = "PM";
//    } else {
//    am_pm = "AM";
//    }
                // mTextview.setText(selectedHour + ":" + selectedMinute);
                mTextview.setText(selectedHour + ":" + convertDate(selectedMinute));
                mTextview.setTextColor(Color.BLACK);

            }
        }, hour, minute,  android.text.format.DateFormat.is24HourFormat(mActivity));//Yes 24 hour time
        mTimePicker.setTitle("Select Time");

        mTimePicker.show();
    }

    public static String convertDate(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }
    /*
     * Initailze places location
     *
     * */
    public void initializePlaceSdKLocation() {
        // Initialize the SDK
        Places.initialize(mActivity, getString(R.string.places_api_key));

        // Create a new Places client instance
        PlacesClient placesClient = Places.createClient(mActivity);
    }

}