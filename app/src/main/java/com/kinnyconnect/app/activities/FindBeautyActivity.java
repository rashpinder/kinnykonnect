package com.kinnyconnect.app.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.BeautyHorizontalAdapter;
import com.kinnyconnect.app.adapters.FindBeautyDesAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.FindBeautyInterface;
import com.kinnyconnect.app.model.FindBeautyModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindBeautyActivity extends BaseActivity implements FindBeautyDesAdapter.PaginationInterface {
//        , FindBeautyInterface{
    /**
     * Getting the Current Class Name
     */
    String TAG = FindBeautyActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = FindBeautyActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.beautyRV)
    RecyclerView beautyRV;
    @BindView(R.id.BeautyHorizontalRV)
    RecyclerView BeautyHorizontalRV;
    FindBeautyDesAdapter mFindBeautyDescAdapter;
    BeautyHorizontalAdapter mhorizontalTextAdapter;
    String cat_id;
    String latitude;
    String longitude;
    List<FindBeautyModel.Datum> mDataList;
    List<FindBeautyModel.CategoryListing> mCategoryList;
    private FindBeautyInterface listener;
    FindBeautyModel mModel;
    private int page_no = 1;
    private int item_count = 10;
    private String strLastPage = "FALSE";
    private List<FindBeautyModel.Datum> tempArrayList = new ArrayList<>();


    FindBeautyDesAdapter.PaginationInterface paginationInterface;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_beauty);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getIntentData();
        paginationInterface =this;
//        listener =this;
        if (tempArrayList != null) {
            tempArrayList.clear();
        }
        if (mDataList != null) {
            mDataList.clear();
        }  if (mCategoryList != null) {
            mCategoryList.clear();
        }
        performGetFindBeauty();

    }
    /*
    Execute api
     */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.ID,null));
        mMap.put("catID", cat_id);
        mMap.put("latitude",  latitude);
        mMap.put("longitude",longitude );
        mMap.put("pageNo",String.valueOf(page_no) );
        mMap.put("distance","100");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performGetFindBeauty() {
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.internet_connection_error));
            } else {
                if (page_no == 1) {
                    showProgressDialog(Objects.requireNonNull(mActivity));
                } else if (page_no > 1) {
                    dismissProgressDialog();
//                mProgressBar.setVisibility(View.GONE);
                }
                executeFindBeautyApi();
            }
        }

    private void executeFindBeautyApi() {

//        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.findBeautyRequest(mParam()).enqueue(new Callback<FindBeautyModel>() {
            @Override
            public void onResponse(Call<FindBeautyModel> call, Response<FindBeautyModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                mModel = response.body();
                page_no=1;
                mDataList = new ArrayList<>();
                mCategoryList=new ArrayList<>();
                mCategoryList=mModel.getCategoryListing();

                if (page_no == 1) {
                    dismissProgressDialog();
                }

                if (page_no > 1) {
                    assert mModel != null;
                    if (mModel.getData() == null) {
                        strLastPage = "TRUE";
                    } else if (mModel.getData().size() < 10) {
//                        Toast.makeText(mActivity(), "No more items!!!", Toast.LENGTH_SHORT).show();
                        strLastPage = "TRUE";
                    } else {
                        strLastPage = "FALSE";
                    }
                }


                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mDataList = mModel.getData();
                    } else if (page_no > 1) {
                        tempArrayList = mModel.getData();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mDataList.addAll(tempArrayList);
                        }
                    }

                    if (page_no == 1) {
                        setBeautyAdapter();
                    } else {
//                        mFindBeautyDescAdapter.notifyDataSetChanged();
                    }
                    setHorizontalBeautyAdapter();
                } else {
//                    showAlertDialog(Objects.requireNonNull(mActivity), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<FindBeautyModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void getIntentData() {
            if (getIntent() != null) {
                if (getIntent().getExtras().get("category_id") != null) {
                    cat_id = (String) getIntent().getExtras().get("category_id");
                    latitude = (String) getIntent().getExtras().get("latitude");
                    longitude = (String) getIntent().getExtras().get("longitude");

                }
            }
    }

    private void setBeautyAdapter() {
        mFindBeautyDescAdapter = new FindBeautyDesAdapter(mActivity, (ArrayList<FindBeautyModel.Datum>) mDataList, mModel, paginationInterface);
        beautyRV.setLayoutManager(new LinearLayoutManager(mActivity));
        beautyRV.setAdapter(mFindBeautyDescAdapter);
        mFindBeautyDescAdapter.notifyDataSetChanged();
    }

    private void setHorizontalBeautyAdapter() {
//        mhorizontalTextAdapter = new BeautyHorizontalAdapter(mActivity,(ArrayList<FindBeautyModel.CategoryListing>) mCategoryList,listener);
//        BeautyHorizontalRV.setLayoutManager(new LinearLayoutManager(mActivity,RecyclerView.HORIZONTAL,false));
//        BeautyHorizontalRV.setAdapter(mhorizontalTextAdapter);
//        mhorizontalTextAdapter.notifyDataSetChanged();
    }
//    @OnClick({R.id.imgSMSIV, R.id.imgFbIV, R.id.imgMailIV, R.id.copyLinkRL,R.id.imgBackIV})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.imgSMSIV:
//                performSMSClick();
//                break;
//            case R.id.imgFbIV:
//                performFbClick();
//                break;
//            case R.id.imgMailIV:
//                performMailClick();
//                break;
//            case R.id.copyLinkRL:
//                performCopyLinkClick();
//                break;
//            case R.id.imgBackIV:
//                performBackClick();
//                break;
//
//        }
//    }



    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            showProgressDialog(Objects.requireNonNull(mActivity));
//            mProgressBar.setVisibility(View.VISIBLE);

            page_no++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeFindBeautyApi();
                    } else {
                        if(progressDialog.isShowing()){
                            dismissProgressDialog();}
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }


//    @Override
//    public void getData(String cat_id) {
//        this.cat_id=cat_id;
//        executeFindBeautyApi();
//    }
}
