package com.kinnyconnect.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kinnyconnect.app.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InviteActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = InviteActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = InviteActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.imgSMSIV)
    ImageView imgSMSIV;
    @BindView(R.id.imgFbIV)
    ImageView imgFbIV;
    @BindView(R.id.imgMailIV)
    ImageView imgMailIV;
    @BindView(R.id.copyLinkRL)
    RelativeLayout copyLinkRL;
    @BindView(R.id.txtCopyLinkTV)
    TextView txtCopyLinkTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
    }

    @OnClick({R.id.imgSMSIV, R.id.imgFbIV, R.id.imgMailIV, R.id.copyLinkRL, R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSMSIV:
                performSMSClick();
                break;
            case R.id.imgFbIV:
                performFbClick();
                break;
            case R.id.imgMailIV:
                performMailClick();
                break;
            case R.id.copyLinkRL:
                performCopyLinkClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;

        }
    }

    private void performCopyLinkClick() {
        showToast(mActivity,"Link Copied");
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", "https://play.google.com/store/apps/details?id=" + "com.kinnyconnect.app");
        clipboard.setPrimaryClip(clip);
    }

    private void performMailClick() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
//        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, "https://play.google.com/store/apps/details?id=" + "com.kinnyconnect.app");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void performFbClick() {
        Intent intent = getPackageManager().getLaunchIntentForPackage("com.facebook.android");
        if (intent != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setPackage("com.facebook.android");
            shareIntent.putExtra(Intent.EXTRA_STREAM, "https://play.google.com/store/apps/details?id=" + "com.kinnyconnect.app");
            shareIntent.setType("image/jpeg");
            startActivity(shareIntent);
        } else {
            // bring user to the market to download the app.
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + "com.facebook.katana"));
            startActivity(intent);
        }
    }

    private void performSMSClick() {
        Uri uri = Uri.parse("smsto:");
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", "https://play.google.com/store/apps/details?id=" + "com.kinnyconnect.app");
        startActivity(it);
    }

    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
