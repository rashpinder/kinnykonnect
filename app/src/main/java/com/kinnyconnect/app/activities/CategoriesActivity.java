package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.FindBeautyAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.CatInterface;
import com.kinnyconnect.app.model.CategoryListingModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class CategoriesActivity extends BaseActivity  implements FindBeautyAdapter.PaginationInterface, CatInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = CategoriesActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = CategoriesActivity.this;

    /*
     * Widgets
     * */
    Unbinder unbinder;
    FindBeautyAdapter mFindBeautyAdapter;
    @BindView(R.id.beautyRV)
    RecyclerView beautyRV;
    List<CategoryListingModel.CategoryDetail> mCategoryList;
//String business_id;
    String latitude;
    String longitude;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    private int page_no = 1;
    private int item_count = 10;
    private String strLastPage = "FALSE";
    private List<CategoryListingModel.CategoryDetail> tempArrayList = new ArrayList<>();


    FindBeautyAdapter.PaginationInterface paginationInterface;
   CatInterface catInterface;
    private RecyclerView.LayoutManager layoutManager;
     String category_id;
    String title;
    int pos;
    String img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getIntentData();
        paginationInterface = this;
        catInterface = this;
        if (tempArrayList != null) {
            tempArrayList.clear();
        }
        if (mCategoryList != null) {
            mCategoryList.clear();
        }
        showCategoryList();
    }

        private void getIntentData() {
            if (getIntent() != null) {
//            if (getIntent().getExtras().get("category_id") != null) {
//                business_id = (String) getIntent().getExtras().get("business_id");
                latitude = (String) getIntent().getExtras().get("latitude");
                longitude = (String) getIntent().getExtras().get("longitude");

//            }
            }
        }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void showCategoryList() {
        if (!isNetworkAvailable(Objects.requireNonNull(mActivity))) {
            showAlertDialog(Objects.requireNonNull(mActivity), getString(R.string.internet_connection_error));
        } else {
            if (page_no == 1) {
                showProgressDialog(Objects.requireNonNull(mActivity));
            } else if (page_no > 1) {
                dismissProgressDialog();
//                mProgressBar.setVisibility(View.GONE);
            }
            executeCategoryListApi();
        }
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("category", "all");
        mMap.put("role", "1");
        mMap.put("pageNo", String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeCategoryListApi() {
//        showProgressDialog(mActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.categoryListRequest(mParam()).enqueue(new Callback<CategoryListingModel>() {
            @Override
            public void onResponse(Call<CategoryListingModel> call, Response<CategoryListingModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                CategoryListingModel mModel = response.body();
                mCategoryList = new ArrayList<>();
                if (page_no == 1) {
                    dismissProgressDialog();
                }

                if (page_no > 1) {
                    assert mModel != null;
                    if (mModel.getCategoryDetails() == null) {
                        strLastPage = "TRUE";
                    } else if (mModel.getCategoryDetails().size() < 10) {
//                        Toast.makeText(mActivity(), "No more items!!!", Toast.LENGTH_SHORT).show();
                        strLastPage = "TRUE";
                    } else {
                        strLastPage = "FALSE";
                    }
                }


                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mCategoryList = mModel.getCategoryDetails();
                    } else if (page_no > 1) {
                        tempArrayList = mModel.getCategoryDetails();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mCategoryList.addAll(tempArrayList);
                        }
                    }

                    if (page_no == 1) {
                        setCategoryAdapter();
                        txtNoDataFountTV.setVisibility(View.GONE);
                    } else {
//                        mFindBeautyAdapter.notifyDataSetChanged();
                        txtNoDataFountTV.setVisibility(View.VISIBLE);
                        txtNoDataFountTV.setText(mModel.getMessage());
                    }

                } else {
                    showAlertDialog(Objects.requireNonNull(mActivity), mModel.getMessage());
                }
            }

            private void setCategoryAdapter() {
                if (mActivity!=null){
                    mFindBeautyAdapter = new FindBeautyAdapter(Objects.requireNonNull(mActivity), (ArrayList<CategoryListingModel.CategoryDetail>) mCategoryList, paginationInterface,latitude,longitude,catInterface);
                    beautyRV.setLayoutManager(new LinearLayoutManager(Objects.requireNonNull(mActivity)));
                    beautyRV.setAdapter(mFindBeautyAdapter);
                    mFindBeautyAdapter.notifyDataSetChanged();
                }}


            @Override
            public void onFailure(Call<CategoryListingModel> call, Throwable t) {
                dismissProgressDialog();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            showProgressDialog(Objects.requireNonNull(mActivity));
//            mProgressBar.setVisibility(View.VISIBLE);

            page_no++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeCategoryListApi();
                    } else {
                        if(progressDialog.isShowing()){
                            dismissProgressDialog();}
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void getData(String category_id, String title, int pos, String img) {
        this.category_id=category_id;
        this.title=title;
        this.pos=pos;
        this.img=img;
        Intent intent = new Intent(mActivity, HomeActivity.class);
        intent.putExtra("category_id",category_id);
        intent.putExtra("title", title);
        intent.putExtra("pos", pos);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("img", img);
        startActivity(intent);
        finish();
    }
}