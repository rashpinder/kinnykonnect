package com.kinnyconnect.app.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.ServicesAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.DeleteItemInterface;
import com.kinnyconnect.app.model.AddToCartModel;
import com.kinnyconnect.app.model.EditHoursModel;
import com.kinnyconnect.app.model.ForgotPasswordModel;
import com.kinnyconnect.app.model.ManageBusinessModel;
import com.kinnyconnect.app.utils.Constants;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import com.suke.widget.SwitchButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageBusinessAccountActivity extends BaseActivity
        implements SwitchButton.OnTouchListener, SwitchButton.OnCheckedChangeListener ,DeleteItemInterface{
    /**
     * Getting the Current Class Name
     */
    String TAG = ManageBusinessAccountActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ManageBusinessAccountActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.switchSundaySB)
    SwitchButton switchSundaySB;
    @BindView(R.id.switchMondaySB)
    SwitchButton switchMondaySB;
    @BindView(R.id.switchTuesdaySB)
    SwitchButton switchTuesdaySB;
    @BindView(R.id.switchWednesdaySB)
    SwitchButton switchWednesdaySB;
    @BindView(R.id.switchThursdaySB)
    SwitchButton switchThursdaySB;
    @BindView(R.id.switchFridaySB)
    SwitchButton switchFridaySB;
    @BindView(R.id.switchSaturdaySB)
    SwitchButton switchSaturdaySB;
    @BindView(R.id.txtSundayStartTimeTV)
    TextView txtSundayStartTimeTV;
    @BindView(R.id.txtSundayEndTimeTV)
    TextView txtSundayEndTimeTV;
    @BindView(R.id.txtStartTimeMondayTV)
    TextView txtStartTimeMondayTV;
    @BindView(R.id.txtEndTimeMondayTV)
    TextView txtEndTimeMondayTV;
    @BindView(R.id.txtStartTimeTuesdayTV)
    TextView txtStartTimeTuesdayTV;
    @BindView(R.id.txtEndTimeTuesdayTV)
    TextView txtEndTimeTuesdayTV;
    @BindView(R.id.txtStartTimeWednesdayTV)
    TextView txtStartTimeWednesdayTV;
    @BindView(R.id.txtEndTimeWednesdayTV)
    TextView txtEndTimeWednesdayTV;
    @BindView(R.id.txtStartTimeThursdayTV)
    TextView txtStartTimeThursdayTV;
    @BindView(R.id.txtEndTimeThursdayTV)
    TextView txtEndTimeThursdayTV;
    @BindView(R.id.txtStartTimeFridayTV)
    TextView txtStartTimeFridayTV;
    @BindView(R.id.txtEndTimeFridayTV)
    TextView txtEndTimeFridayTV;
    @BindView(R.id.txtStartTimeSaturdayTV)
    TextView txtStartTimeSaturdayTV;
    @BindView(R.id.txtEndTimeSaturdayTV)
    TextView txtEndTimeSaturdayTV;
    @BindView(R.id.txtAcceptingTV)
    EditText txtAcceptingTV;
    @BindView(R.id.txtEditTV)
    TextView txtEditTV;
    @BindView(R.id.txtAddTV)
    TextView txtAddTV;
    @BindView(R.id.txtMessageAddTV)
    TextView txtMessageAddTV;
 @BindView(R.id.imgBackIV)
 ImageView imgBackIV;


    @BindView(R.id.servicesRV)
    RecyclerView servicesRV;
    ServicesAdapter mServicesAdapter;
    List<ManageBusinessModel.Datum> mDataList;
    List<ManageBusinessModel.Datum.Service> mServicesList;
    List<ManageBusinessModel.Datum.WorkingDetail> mWorkingList;
    List<ManageBusinessModel.Datum.MessageBoard> mMessageBoardList;
    String sun_wi;
    String mon_wi;
    String tues_wi;
    String wed_wi;
    String thu_wi;
    String fri_wi;
    String sat_wi;
    String work_id;
    String sun_oc;
    String mon_oc;
    String tues_oc;
    String wed_oc;
    String thu_oc;
    String fri_oc;
    String sat_oc;
    String work_id_time;
    String open_close;
    String mb_id;
    String sun_time;
    String start_time;
    String end_time;
    String service_id;
    int pos;
    static Boolean isTouched = false;
    int new_selectedHour;
    private DeleteItemInterface deleteItemInterface;
    String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_business_account);
        ButterKnife.bind(this);
        deleteItemInterface=this;
        setStatusBar(mActivity);
        getIntentData();
        switchSundaySB.setOnTouchListener(this);
        switchSundaySB.setOnCheckedChangeListener(this);
        switchMondaySB.setOnTouchListener(this);
        switchMondaySB.setOnCheckedChangeListener(this);
        switchTuesdaySB.setOnTouchListener(this);
        switchTuesdaySB.setOnCheckedChangeListener(this);
        switchWednesdaySB.setOnTouchListener(this);
        switchWednesdaySB.setOnCheckedChangeListener(this);
        switchThursdaySB.setOnTouchListener(this);
        switchThursdaySB.setOnCheckedChangeListener(this);
        switchFridaySB.setOnTouchListener(this);
        switchFridaySB.setOnCheckedChangeListener(this);
        switchSaturdaySB.setOnTouchListener(this);
        switchSaturdaySB.setOnCheckedChangeListener(this);

    }

    private void getIntentData() {
        if (getIntent() != null) {
            value = getIntent().getStringExtra("value");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getManageBusinessData();
    }


    private void getManageBusinessData() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeManageBusinessUpApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("businessID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeManageBusinessUpApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.businessListRequest(mParam()).enqueue(new Callback<ManageBusinessModel>() {
            @Override
            public void onResponse(Call<ManageBusinessModel> call, Response<ManageBusinessModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ManageBusinessModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    mDataList = new ArrayList<>();
                    mServicesList = new ArrayList<>();
                    mWorkingList = new ArrayList<>();
                    mMessageBoardList = new ArrayList<>();
                    mDataList = mModel.getData();
                    for (int i = 0; i < mDataList.size(); i++) {
                        mServicesList = mDataList.get(i).getServices();
                        mWorkingList = mDataList.get(i).getWorkingDetails();
                        mMessageBoardList = mDataList.get(i).getMessageBoard();
                    }

                    setServicesAdapter();
                    setBookingAvailability();
                    setMessageBoard();

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            private void setMessageBoard() {
                for (int i = 0; i < mMessageBoardList.size(); i++) {
                    txtAcceptingTV.setText(mMessageBoardList.get(i).getTitle());
                    mb_id = mMessageBoardList.get(i).getId();
                }
            }

            private void setBookingAvailability() {
                for (int i = 0; i < mWorkingList.size(); i++) {

                    if (mWorkingList.get(i).getDays().equals("0")) {
                        if (mWorkingList.get(i).getOpenClose().equals("1")) {
                            switchSundaySB.setChecked(false);
//                            txtSundayStartTimeTV.setClickable(false);
//                            txtSundayEndTimeTV.setClickable(false);
                        } else {
                            switchSundaySB.setChecked(true);
//                            txtSundayStartTimeTV.setClickable(true);
//                            txtSundayEndTimeTV.setClickable(true);
                        }
                        if (mWorkingList.get(i).getStartHours().equals("")) {
                            txtSundayStartTimeTV.setText("Start Time");
                        } else {
                            txtSundayStartTimeTV.setText(mWorkingList.get(i).getStartHours());
                        }
                        if (mWorkingList.get(i).getEndHours().equals("")) {
                            txtSundayEndTimeTV.setText("End Time");
                        } else {
                            txtSundayEndTimeTV.setText(mWorkingList.get(i).getEndHours());
                        }
                        sun_wi = mWorkingList.get(i).getWorkID();
                        sun_oc = mWorkingList.get(i).getOpenClose();
                    }
                    if (mWorkingList.get(i).getDays().equals("1")) {
                        if (mWorkingList.get(i).getOpenClose().equals("1")) {
//                            txtStartTimeMondayTV.setClickable(false);
//                            txtEndTimeMondayTV.setClickable(false);
                        } else {
                            switchMondaySB.setChecked(true);
//                            txtStartTimeMondayTV.setClickable(true);
//                            txtEndTimeMondayTV.setClickable(true);
                        }
                        if (mWorkingList.get(i).getStartHours().equals("")) {
                            txtStartTimeMondayTV.setText("Start Time");
                        } else {
                            txtStartTimeMondayTV.setText(mWorkingList.get(i).getStartHours());
                        }
                        if (mWorkingList.get(i).getEndHours().equals("")) {
                            txtEndTimeMondayTV.setText("End Time");
                        } else {
                            txtEndTimeMondayTV.setText(mWorkingList.get(i).getEndHours());
                        }
                        mon_wi = mWorkingList.get(i).getWorkID();
                        mon_oc = mWorkingList.get(i).getOpenClose();
                    }
                    if (mWorkingList.get(i).getDays().equals("2")) {
                        if (mWorkingList.get(i).getOpenClose().equals("1")) {
                            switchTuesdaySB.setChecked(false);
//                            txtStartTimeTuesdayTV.setClickable(false);
//                            txtEndTimeTuesdayTV.setClickable(false);
                        } else {
                            switchTuesdaySB.setChecked(true);

//                            txtStartTimeTuesdayTV.setClickable(true);
//                            txtEndTimeTuesdayTV.setClickable(true);
                        }
                        if (mWorkingList.get(i).getStartHours().equals("")) {
                            txtStartTimeTuesdayTV.setText("Start Time");
                        } else {
                            txtStartTimeTuesdayTV.setText(mWorkingList.get(i).getStartHours());
                        }
                        if (mWorkingList.get(i).getEndHours().equals("")) {
                            txtEndTimeTuesdayTV.setText("End Time");
                        } else {
                            txtEndTimeTuesdayTV.setText(mWorkingList.get(i).getEndHours());
                        }
                        tues_wi = mWorkingList.get(i).getWorkID();
                        tues_oc = mWorkingList.get(i).getOpenClose();
                    }
                    if (mWorkingList.get(i).getDays().equals("3")) {
                        if (mWorkingList.get(i).getOpenClose().equals("1")) {
                            switchWednesdaySB.setChecked(false);
//                            txtStartTimeWednesdayTV.setClickable(false);
//                            txtEndTimeWednesdayTV.setClickable(false);
                        } else {
                            switchWednesdaySB.setChecked(true);
//                            txtStartTimeWednesdayTV.setClickable(true);
//                            txtEndTimeWednesdayTV.setClickable(true);
                        }
                        if (mWorkingList.get(i).getStartHours().equals("")) {
                            txtStartTimeWednesdayTV.setText("Start Time");
                        } else {
                            txtStartTimeWednesdayTV.setText(mWorkingList.get(i).getStartHours());
                        }
                        if (mWorkingList.get(i).getEndHours().equals("")) {
                            txtEndTimeWednesdayTV.setText("End Time");
                        } else {
                            txtEndTimeWednesdayTV.setText(mWorkingList.get(i).getEndHours());
                        }

                        wed_wi = mWorkingList.get(i).getWorkID();
                        wed_oc = mWorkingList.get(i).getOpenClose();
                    }
                    if (mWorkingList.get(i).getDays().equals("4")) {
                        if (mWorkingList.get(i).getOpenClose().equals("1")) {
                            switchThursdaySB.setChecked(false);
//                            txtStartTimeThursdayTV.setClickable(false);
//                            txtEndTimeThursdayTV.setClickable(false);
                        } else {
                            switchThursdaySB.setChecked(true);
//                            txtStartTimeThursdayTV.setClickable(true);
//                            txtEndTimeThursdayTV.setClickable(true);
                        }
                        if (mWorkingList.get(i).getStartHours().equals("")) {
                            txtStartTimeThursdayTV.setText("Start Time");
                        } else {
                            txtStartTimeThursdayTV.setText(mWorkingList.get(i).getStartHours());
                        }
                        if (mWorkingList.get(i).getEndHours().equals("")) {
                            txtEndTimeThursdayTV.setText("End Time");
                        } else {
                            txtEndTimeThursdayTV.setText(mWorkingList.get(i).getEndHours());
                        }
                        thu_wi = mWorkingList.get(i).getWorkID();
                        thu_oc = mWorkingList.get(i).getOpenClose();
                    }
                    if (mWorkingList.get(i).getDays().equals("5")) {
                        if (mWorkingList.get(i).getOpenClose().equals("1")) {
                            switchFridaySB.setChecked(false);
//                            txtStartTimeFridayTV.setClickable(false);
//                            txtEndTimeFridayTV.setClickable(false);
                        } else {
                            switchFridaySB.setChecked(true);
//                            txtStartTimeFridayTV.setClickable(true);
//                            txtEndTimeFridayTV.setClickable(true);
                        }
                        if (mWorkingList.get(i).getStartHours().equals("")) {
                            txtStartTimeFridayTV.setText("Start Time");
                        } else {
                            txtStartTimeFridayTV.setText(mWorkingList.get(i).getStartHours());
                        }
                        if (mWorkingList.get(i).getEndHours().equals("")) {
                            txtEndTimeFridayTV.setText("End Time");
                        } else {
                            txtEndTimeFridayTV.setText(mWorkingList.get(i).getEndHours());
                        }
                        fri_wi = mWorkingList.get(i).getWorkID();
                        fri_oc = mWorkingList.get(i).getOpenClose();
                    }
                    if (mWorkingList.get(i).getDays().equals("6")) {
                        if (mWorkingList.get(i).getOpenClose().equals("1")) {
                            switchSaturdaySB.setChecked(false);
//                            txtStartTimeSaturdayTV.setClickable(false);
//                            txtEndTimeSaturdayTV.setClickable(false);
                        } else {
                            switchSaturdaySB.setChecked(true);
//                            txtStartTimeSaturdayTV.setClickable(true);
//                            txtEndTimeSaturdayTV.setClickable(true);
                        }
                        if (mWorkingList.get(i).getStartHours().equals("")) {
                            txtStartTimeSaturdayTV.setText("Start Time");
                        } else {
                            txtStartTimeSaturdayTV.setText(mWorkingList.get(i).getStartHours());
                        }
                        if (mWorkingList.get(i).getEndHours().equals("")) {
                            txtEndTimeSaturdayTV.setText("End Time");
                        } else {
                            txtEndTimeSaturdayTV.setText(mWorkingList.get(i).getEndHours());
                        }
                        sat_wi = mWorkingList.get(i).getWorkID();
                        sat_oc = mWorkingList.get(i).getOpenClose();
                    }
                }
            }

            @Override
            public void onFailure(Call<ManageBusinessModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void setServicesAdapter() {
        mServicesAdapter = new ServicesAdapter(mActivity, (ArrayList<ManageBusinessModel.Datum.Service>) mServicesList,deleteItemInterface);
        servicesRV.setLayoutManager(new LinearLayoutManager(mActivity));
        servicesRV.setAdapter(mServicesAdapter);
    }

    @OnClick({R.id.txtEditTV, R.id.txtAddTV, R.id.txtMessageAddTV, R.id.txtSundayEndTimeTV, R.id.txtSundayStartTimeTV, R.id.txtStartTimeMondayTV, R.id.txtEndTimeMondayTV, R.id.txtStartTimeTuesdayTV,
            R.id.txtEndTimeTuesdayTV, R.id.txtStartTimeWednesdayTV, R.id.txtEndTimeWednesdayTV, R.id.txtStartTimeThursdayTV, R.id.txtEndTimeThursdayTV, R.id.txtStartTimeFridayTV, R.id.txtEndTimeFridayTV,
            R.id.txtStartTimeSaturdayTV, R.id.txtEndTimeSaturdayTV,R.id.imgBackIV})
    public void onViewClicked(View vi) {
        switch (vi.getId()) {
            case R.id.txtAddTV:
                performAddClick();
                break;
            case R.id.txtEditTV:
                performEditClick();
                break;
            case R.id.txtMessageAddTV:
                performAddMessageClick();
                break;
            case R.id.txtSundayStartTimeTV:
                performSundayStartTimeClick();
                break;
            case R.id.txtSundayEndTimeTV:
                performSundayEndTimeClick();
                break;
            case R.id.txtStartTimeMondayTV:
                performMondayStartTimeClick();
                break;
            case R.id.txtEndTimeMondayTV:
                performMondayEndTimeClick();
                break;
            case R.id.txtStartTimeTuesdayTV:
                performTuesdayStartTimeClick();
                break;
            case R.id.txtEndTimeTuesdayTV:
                performTuesdayEndTimeClick();
                break;
            case R.id.txtStartTimeWednesdayTV:
                performWednesdayStartTimeClick();
                break;
            case R.id.txtEndTimeWednesdayTV:
                performWednesdayEndTimeClick();
                break;
            case R.id.txtStartTimeThursdayTV:
                performThursdayStartTimeClick();
                break;
            case R.id.txtEndTimeThursdayTV:
                performThursdayEndTimeClick();
                break;
            case R.id.txtStartTimeFridayTV:
                performFridayStartTimeClick();
                break;
            case R.id.txtEndTimeFridayTV:
                performFridayEndTimeClick();
                break;
            case R.id.txtStartTimeSaturdayTV:
                performSaturdayStartTimeClick();
                break;
            case R.id.txtEndTimeSaturdayTV:
                performSaturdayEndTimeClick();
                break;
 case R.id.imgBackIV:
                performBackClick();
                break;

        }
    }

    private void performSaturdayEndTimeClick() {
        work_id_time = sat_wi;
        showEndTimePicker(txtEndTimeSaturdayTV);
    }

    private void performSaturdayStartTimeClick() {
        work_id_time = sat_wi;
        showTimePicker(txtStartTimeSaturdayTV);
    }

    private void performFridayEndTimeClick() {
        work_id_time = fri_wi;
        showEndTimePicker(txtEndTimeFridayTV);
    }

    private void performFridayStartTimeClick() {
        work_id_time = fri_wi;
        showTimePicker(txtStartTimeFridayTV);
    }

    private void performThursdayEndTimeClick() {
        work_id_time = thu_wi;
        showEndTimePicker(txtEndTimeThursdayTV);
    }

    private void performThursdayStartTimeClick() {
        work_id_time = thu_wi;
        showTimePicker(txtStartTimeThursdayTV);
    }

    private void performWednesdayEndTimeClick() {
        work_id_time = wed_wi;
        showEndTimePicker(txtEndTimeWednesdayTV);
    }

    private void performWednesdayStartTimeClick() {
        work_id_time = wed_wi;
        showTimePicker(txtStartTimeWednesdayTV);
    }

    private void performTuesdayEndTimeClick() {
        work_id_time = tues_wi;
        showEndTimePicker(txtEndTimeTuesdayTV);
    }

    private void performTuesdayStartTimeClick() {
        work_id_time = tues_wi;
        showTimePicker(txtStartTimeTuesdayTV);
    }

    private void performMondayEndTimeClick() {
        work_id_time = mon_wi;
        showEndTimePicker(txtEndTimeMondayTV);
    }

    private void performMondayStartTimeClick() {
        work_id_time = mon_wi;
        showTimePicker(txtStartTimeMondayTV);
    }


    private void performSundayEndTimeClick() {
        work_id_time = sun_wi;
        showEndTimePicker(txtSundayEndTimeTV);
    }


    private void performSundayStartTimeClick() {
        work_id_time = sun_wi;

        showTimePicker(txtSundayStartTimeTV);
    }

    public void showTimePicker(TextView mTextview) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        int am = mcurrentTime.get(Calendar.AM_PM);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(mActivity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String am_pm;
                if (selectedHour > 12) {
                    new_selectedHour = selectedHour - 12;
                    am_pm = "PM";
                } else if (selectedHour == 12) {
                    new_selectedHour = selectedHour;
                    am_pm = "PM";
                } else {
                    am_pm = "AM";
                }
                // mTextview.setText(selectedHour + ":" + selectedMinute);
                mTextview.setText(new_selectedHour + ":" + convertDate(selectedMinute) + " " + am_pm);
                mTextview.setTextColor(getResources().getColor(R.color.colorGreyText));
                start_time = mTextview.getText().toString().trim();
                executeStartEditHoursApi();

            }


        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public static String convertDate(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }

    public void showEndTimePicker(TextView mTextview) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        int am = mcurrentTime.get(Calendar.AM_PM);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(mActivity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String am_pm;
                if (selectedHour > 12) {
                    new_selectedHour = selectedHour - 12;
                    am_pm = "PM";
                } else if (selectedHour == 12) {
                    new_selectedHour = selectedHour;
                    am_pm = "PM";
                } else {
                    am_pm = "AM";
                }
                // mTextview.setText(selectedHour + ":" + selectedMinute);
                mTextview.setText(new_selectedHour + ":" + convertEndDate(selectedMinute) + " " + am_pm);
                mTextview.setTextColor(getResources().getColor(R.color.colorGreyText));
                end_time = mTextview.getText().toString().trim();
                executeEndEditHoursApi();
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");

        mTimePicker.show();
    }

    public static String convertEndDate(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }


    private void executeStartEditHoursApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            setEditStartHours();
        }

    }

    /*
     * Execute api
     * */
    private Map<String, String> editStartHoursParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("workID", work_id_time);
        mMap.put("startHours", start_time);
//        mMap.put("endHours", end_time);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void setEditStartHours() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editHoursRequest(editStartHoursParam()).enqueue(new Callback<EditHoursModel>() {
            @Override
            public void onResponse(Call<EditHoursModel> call, Response<EditHoursModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                EditHoursModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }


            @Override
            public void onFailure(Call<EditHoursModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeEndEditHoursApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            setEditEndHours();
        }

    }

    /*
     * Execute api
     * */
    private Map<String, String> editEndHoursParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("workID", work_id_time);
//        mMap.put("startHours", start_time);
        mMap.put("endHours", end_time);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void setEditEndHours() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editHoursRequest(editEndHoursParam()).enqueue(new Callback<EditHoursModel>() {
            @Override
            public void onResponse(Call<EditHoursModel> call, Response<EditHoursModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                EditHoursModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }


            @Override
            public void onFailure(Call<EditHoursModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void performSaturdayClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            work_id = sat_wi;
            executeOpenCloseApi();
        }
    }

    private void performFridayClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            work_id = fri_wi;
            executeOpenCloseApi();
        }
    }

    private void performThursdayClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            work_id = thu_wi;
            executeOpenCloseApi();
        }
    }

    private void performWednesdayClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            work_id = wed_wi;
            executeOpenCloseApi();
        }
    }

    private void performTuesdayClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            work_id = tues_wi;
            executeOpenCloseApi();
        }
    }

    private void performMondayClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            work_id = mon_wi;
            executeOpenCloseApi();
        }
    }

    private void performSundayClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            work_id = sun_wi;
            executeOpenCloseApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> onOffParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("workID", work_id);
        mMap.put("open", open_close);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeOpenCloseApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editOpenCloseRequest(onOffParam()).enqueue(new Callback<EditHoursModel>() {
            @Override
            public void onResponse(Call<EditHoursModel> call, Response<EditHoursModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                EditHoursModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }


            @Override
            public void onFailure(Call<EditHoursModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void performAddMessageClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            txtMessageAddTV.setVisibility(View.GONE);
            txtEditTV.setVisibility(View.VISIBLE);
            txtAcceptingTV.setEnabled(false);
            executeAddMessageApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> addMessageParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("messageBoardID", mb_id);
        mMap.put("title", txtAcceptingTV.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddMessageApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editMBRequest(addMessageParam()).enqueue(new Callback<EditHoursModel>() {
            @Override
            public void onResponse(Call<EditHoursModel> call, Response<EditHoursModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                EditHoursModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<EditHoursModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void performEditClick() {
        txtAcceptingTV.setEnabled(true);
        txtMessageAddTV.setVisibility(View.VISIBLE);
        txtEditTV.setVisibility(View.GONE);
    }

    private void performAddClick() {
        Intent intent = new Intent(mActivity, AddServiceActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void performBackClick() {
        if (value.equals("account")){
            onBackPressed();
        }
        else {
            startActivity(new Intent(mActivity, DrawerActivity.class));
            finish();
        }

//        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        isTouched = true;
        return false;
    }

    @Override
    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
        switch (view.getId()) {
            case R.id.switchSundaySB:
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        open_close = "0";
                        performSundayClick();

                    } else {
                        open_close = "1";
                        performSundayClick();
                    }
                } else {
                    if (isChecked) {
                        open_close = "0";
                        switchSundaySB.setChecked(true);

                    } else {
                        open_close = "1";
                        switchSundaySB.setChecked(false);

                    }
                }

                break;
            case R.id.switchMondaySB:
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        open_close = "0";
                        performMondayClick();
                    } else {
                        open_close = "1";
                        performMondayClick();
                    }
                } else {
                    if (isChecked) {
                        switchMondaySB.setChecked(true);
                        open_close = "0";
                    } else {
                        switchMondaySB.setChecked(false);
                        open_close = "1";
                    }
                }
                break;
            case R.id.switchTuesdaySB:
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        open_close = "0";
                        performTuesdayClick();
                    } else {
                        open_close = "1";
                        performTuesdayClick();
                    }
                } else {
                    if (isChecked) {
                        switchTuesdaySB.setChecked(true);
                        open_close = "0";
                    } else {
                        switchTuesdaySB.setChecked(false);
                        open_close = "1";
                    }
                }

                break;
            case R.id.switchWednesdaySB:
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        open_close = "0";
                        performWednesdayClick();
                    } else {
                        open_close = "1";
                        performWednesdayClick();
                    }
                } else {
                    if (isChecked) {
                        switchWednesdaySB.setChecked(true);
                        open_close = "0";
                    } else {
                        switchWednesdaySB.setChecked(false);
                        open_close = "1";
                    }
                }
                break;
            case R.id.switchThursdaySB:
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        open_close = "0";
                        performThursdayClick();
                    } else {
                        open_close = "1";
                        performThursdayClick();
                    }
                } else {
                    if (isChecked) {
                        switchThursdaySB.setChecked(true);
                        open_close = "0";
                    } else {
                        switchThursdaySB.setChecked(false);
                        open_close = "1";
                    }
                }
                break;
            case R.id.switchFridaySB:
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        open_close = "0";
                        performFridayClick();
                    } else {
                        open_close = "1";
                        performFridayClick();
                    }
                } else {
                    if (isChecked) {
                        switchFridaySB.setChecked(true);
                        open_close = "0";
                    } else {
                        switchFridaySB.setChecked(false);
                        open_close = "1";
                    }
                }
                break;
            case R.id.switchSaturdaySB:
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        open_close = "0";
                        performSaturdayClick();
                    } else {
                        open_close = "1";
                        performSaturdayClick();
                    }
                } else {
                    if (isChecked) {
                        switchSaturdaySB.setChecked(true);
                        open_close = "0";
                    } else {
                        switchSaturdaySB.setChecked(false);
                        open_close = "1";
                    }
                }
                break;
        }
    }

    @Override
    public void getData(String cartID, int pos) {
        service_id = cartID;
        this.pos = pos;
        deleteService();
    }


    private void deleteService() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeDeleteServiceApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mDeleteItemParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("serviceID", service_id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeDeleteServiceApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.deleteServiceRequest(mDeleteItemParams()).enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ForgotPasswordModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mServicesList.remove(pos);
                    mServicesAdapter.notifyDataSetChanged();

                } else {
                    showToast(mActivity, mModel.getMessage());
//                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

}
