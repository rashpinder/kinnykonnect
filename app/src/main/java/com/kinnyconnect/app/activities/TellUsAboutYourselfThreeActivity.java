package com.kinnyconnect.app.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.TellYourselfModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TellUsAboutYourselfThreeActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TellUsAboutYourselfThreeActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = TellUsAboutYourselfThreeActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editQuesOneET)
    EditText editQuesOneET;
    @BindView(R.id.editQuesTwoET)
    EditText editQuesTwoET;
    @BindView(R.id.editQuesThreeET)
    EditText editQuesThreeET;
    @BindView(R.id.editQuesFourET)
    EditText editQuesFourET;
    @BindView(R.id.editQuesFiveET)
    EditText editQuesFiveET;
    @BindView(R.id.editQuesSixET)
    EditText editQuesSixET;
    @BindView(R.id.editQuesSevenET)
    EditText editQuesSevenET;
    @BindView(R.id.txtSubmitTV)
    TextView txtSubmitTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tell_us_about_yourself_three);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
    }
    @OnClick({R.id.txtSubmitTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSubmitTV:
                performSubmitClick();
                break;
        }
    }


    private void performSubmitClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                createAccountApi();
            }
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("status", "3");
        mMap.put("interestedBookingServices", editQuesOneET.getText().toString().trim());
        mMap.put("bookingPageWebsite", editQuesTwoET.getText().toString().trim());
        mMap.put("socialMarketing", editQuesThreeET.getText().toString().trim());
        mMap.put("majorityClients", editQuesFourET.getText().toString().trim());
        mMap.put("clientsInMonths", editQuesFiveET.getText().toString().trim());
        mMap.put("faceAsEntrepreneur", editQuesSixET.getText().toString().trim());
        mMap.put("gainfromKinny", editQuesSevenET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void createAccountApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createAccountRequest(mParam()).enqueue(new Callback<TellYourselfModel>() {
            @Override
            public void onResponse(Call<TellYourselfModel> call, Response<TellYourselfModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TellYourselfModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    KinnyConnectPreferences.writeBoolean(mActivity,KinnyConnectPreferences.IS_BUSINNESS_LOGIN,true);
                    KinnyConnectPreferences.writeBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, true);
                    Toast.makeText(mActivity,mModel.getMessage(),Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TellYourselfModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
       if (editQuesOneET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.field_cannot_empty));
            flag = false;
        } else if (editQuesTwoET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.field_cannot_empty));
            flag = false;
        } else if (editQuesThreeET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.field_cannot_empty));
            flag = false;
        } else if (editQuesFourET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.field_cannot_empty));
            flag = false;
        } else if (editQuesFiveET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.field_cannot_empty));
            flag = false;
        } else if (editQuesSixET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.field_cannot_empty));
            flag = false;
        }else if (editQuesSevenET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.field_cannot_empty));
            flag = false;
        }
        return flag;
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
