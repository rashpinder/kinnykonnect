package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.EndTimeSlotsAdapter;
import com.kinnyconnect.app.adapters.SelectServiceAdapter;
import com.kinnyconnect.app.adapters.StartTimeSlotsAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.EndTimeSlotsInterface;
import com.kinnyconnect.app.interfaces.SelectServiceInterface;
import com.kinnyconnect.app.interfaces.SelectTimeSlotInterface;
import com.kinnyconnect.app.interfaces.ServiceInterface;
import com.kinnyconnect.app.model.BookModel;
import com.kinnyconnect.app.model.BusinessDetailModel;
import com.kinnyconnect.app.model.EndTimeModel;
import com.kinnyconnect.app.model.TimeSlotModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import com.savvi.rangedatepicker.CalendarPickerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class BookingActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = BookingActivity.this.getClass().getSimpleName();
    /**
     * Current Activity Instance
     */
    Activity mActivity = BookingActivity.this;

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.servicesRV)
    RecyclerView servicesRV;
    @BindView(R.id.mCalenderCV)
    CalendarPickerView mCalenderCV;
    @BindView(R.id.headerLL)
    LinearLayout headerLL;
    @BindView(R.id.mStartTimeSlotsRecyclerView)
    RecyclerView mStartTimeSlotsRecyclerView;
    @BindView(R.id.mEndTimeSlotsRecyclerView)
    RecyclerView mEndTimeSlotsRecyclerView;
    @BindView(R.id.artistNotFountTV)
    TextView artistNotFountTV;
    @BindView(R.id.txtStartTimeTV)
    TextView txtStartTimeTV;
    @BindView(R.id.txtEndTimeTV)
    TextView txtEndTimeTV;
    List<BusinessDetailModel.BusinessDetail> mBusinessDetailList;
    List<BusinessDetailModel.BusinessDetail.Service> mServicesList;
    List<TimeSlotModel.FreeSlot> mTimeSlotList;
    int num_day;
    String strStartTimeInMillies;
    String strEndTimeInMillies;

    SelectServiceAdapter mServicesAdapter;

    String strCalendarDate = "";
    String strDayDate = "";
    String business_id;
    TimeSlotModel.FreeSlot mFreeTimeSlotsModel = new TimeSlotModel.FreeSlot();
    String mEndTimeSlot;
//    TimeSlotModel.FreeSlot mEndFreeTimeSlotsModel = new TimeSlotModel.FreeSlot();
    BusinessDetailModel.BusinessDetail.Service mServicesModel = new BusinessDetailModel.BusinessDetail.Service();
    /*
     * Calender
     * */
    SimpleDateFormat mSimpleDateFormat;
    Calendar mCalendar;
    StartTimeSlotsAdapter mStartTimeSlotsAdapter;
    EndTimeSlotsAdapter mEndTimeSlotsAdapter;
String serID;
    /*
     * Activity Override method
     * #onActivityCreated
     **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        //butter knife
        ButterKnife.bind(this);
        //Set Calender
        setCalendar();

        getIntentData();

        artistNotFountTV.setVisibility(View.GONE);
        mStartTimeSlotsRecyclerView.setVisibility(View.GONE);
        mEndTimeSlotsRecyclerView.setVisibility(View.GONE);
        txtEndTimeTV.setVisibility(View.GONE);
        txtStartTimeTV.setVisibility(View.GONE);
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("business_id") != null) {
                business_id = (String) getIntent().getExtras().get("business_id");
            }
        }
        executeGetServices();
    }

    SelectServiceInterface mSelectServiceInterface = new SelectServiceInterface() {
        @Override
        public void getSelectedService(BusinessDetailModel.BusinessDetail.Service mServicesModel11) {
            Log.e(TAG, "**Service Name**" + mServicesModel11.getTitle());
            mServicesModel = mServicesModel11;
            serID=mServicesModel11.getSerID();
        }
    };

    ServiceInterface mServiceInterface = new ServiceInterface() {
        @Override
        public void getService(BusinessDetailModel.BusinessDetail.Service mServicesModel11) {
            Log.e(TAG, "**Service Name**" + mServicesModel11.getTitle());
            mServicesModel = mServicesModel11;
            serID=mServicesModel11.getSerID();

            if (!strCalendarDate.equals("")){
                executeGetTimeSlots();
            }

        }
    };
    /*
     * Time Slots Click Listner
     * */
    SelectTimeSlotInterface mSelectTimeSlotInterface = new SelectTimeSlotInterface() {
        @Override
        public void getTimeSlot(TimeSlotModel.FreeSlot mFreeTimeSlotsModel11) {
                mFreeTimeSlotsModel = mFreeTimeSlotsModel11;
                Log.e(TAG, "**Start Time Slot**" + mFreeTimeSlotsModel.getStartTime());
            executeGetEndTimeSlots();
        }
    };

    /*
     * Time Slots Click Listner
     * */
    EndTimeSlotsInterface mEndTimeSlotInterface = new EndTimeSlotsInterface() {
        @Override
        public void getEndTimeSlot(String mEndTime) {
            mEndTimeSlot = mEndTime;
                Log.e(TAG, "**End Time Slot**" + mEndTimeSlot);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setCalendar() {
        mCalendar = Calendar.getInstance();

        mSimpleDateFormat = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 10);
        Calendar lastYear = Calendar.getInstance();
        //lastYear.add(Calendar.YEAR, 0);
        lastYear.add(Calendar.DAY_OF_YEAR, 1);


        Date tomorrow = lastYear.getTime();

        /*
         * CLICK PARTICULAR DATE
         * */
        mCalenderCV.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                Log.e(TAG, "onDateSelected: "+date.toString());

                strCalendarDate =convertCalenderTimeToFormat(date.toString());
                Log.e(TAG, "**Calendar Date Listner**" + strCalendarDate);
                strDayDate = convertTimeToDayName(strCalendarDate);
                Log.e(TAG, "**Day Date Listner**" + strDayDate);
                if (strDayDate.equalsIgnoreCase("sunday")) {
                    num_day = 0;
                    Log.e(TAG, "**Day Date Listner**" + num_day);
                } else if (strDayDate.equalsIgnoreCase("monday")) {
                    num_day = 1;
                    Log.e(TAG, "**Day Date Listner**" + num_day);
                } else if (strDayDate.equalsIgnoreCase("tuesday")) {
                    num_day = 2;
                    Log.e(TAG, "**Day Date Listner**" + num_day);
                } else if (strDayDate.equalsIgnoreCase("wednesday")) {
                    num_day = 3;
                    Log.e(TAG, "**Day Date Listner**" + num_day);
                } else if (strDayDate.equalsIgnoreCase("thursday")) {
                    num_day = 4;
                    Log.e(TAG, "**Day Date Listner**" + num_day);
                } else if (strDayDate.equalsIgnoreCase("friday")) {
                    num_day = 5;
                    Log.e(TAG, "**Day Date Listner**" + num_day);
                } else if (strDayDate.equalsIgnoreCase("saturday")) {
                    num_day = 6;
                    Log.e(TAG, "**Day Date Listner**" + num_day);
                }
                if (mTimeSlotList!=null){
                    mTimeSlotList.clear();
                }
                //Get Appointments time api:
                executeGetTimeSlots();
            }

            @Override
            public void onDateUnselected(Date date) {
//                Log.e(TAG, "onDateUnselected: " + date.toString());
//
//                strCalendarDate =convertCalenderTimeToFormat(date.toString());
//                Log.e(TAG, "**Calendar Date Listner**" + strCalendarDate);
//                strDayDate = convertTimeToDayName(strCalendarDate);
//                Log.e(TAG, "**Day Date Listner**" + strDayDate);
                //Get Appointments time api:
//                if (mTimeSlotList!=null){
//                    mTimeSlotList.clear();
//                }
//                executeGetTimeSlots();
            }
        });

        mCalenderCV.init(tomorrow, nextYear.getTime(), mSimpleDateFormat)
                .inMode(CalendarPickerView.SelectionMode.SINGLE);

    }

    /*
     * Get Day Full Name
     * */
    public String convertCalenderTimeToFormat(String input) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat inFormat = new SimpleDateFormat("E MMM dd hh:mm:ss z yyyy");

        Date date = null;
        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
        }

        SimpleDateFormat outFormat = new SimpleDateFormat("yyyy/MM/dd");
        String goal = outFormat.format(date);

        return goal;
    }

    //execute get detail api
    private Map<String, String> mTimeSlotParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("businessID", business_id);
        mMap.put("date", strCalendarDate);
        mMap.put("serviceID", serID);
        mMap.put("day", String.valueOf(num_day));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetTimeSlots() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeTimeSlotsApi();
        }
    }


    private void executeTimeSlotsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getTimeSlotsRequest(mTimeSlotParams()).enqueue(new Callback<TimeSlotModel>() {
            @Override
            public void onResponse(Call<TimeSlotModel> call, retrofit2.Response<TimeSlotModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                TimeSlotModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mTimeSlotList = new ArrayList<>();
                    mTimeSlotList = mModel.getFreeSlots();
                    artistNotFountTV.setVisibility(View.GONE);
                    mStartTimeSlotsRecyclerView.setVisibility(View.VISIBLE);
                    mEndTimeSlotsRecyclerView.setVisibility(View.GONE);
                    txtEndTimeTV.setVisibility(View.GONE);
                    txtStartTimeTV.setVisibility(View.VISIBLE);
                    //Set Up Recycler View Data
                    setStartTimeSlotAdapter(mTimeSlotList);
//                    setEndTimeSlotAdapter(mTimeSlotList);
                } else {
                    artistNotFountTV.setVisibility(View.VISIBLE);
                    mStartTimeSlotsRecyclerView.setVisibility(View.GONE);
                    mEndTimeSlotsRecyclerView.setVisibility(View.GONE);
                    txtEndTimeTV.setVisibility(View.GONE);
                    txtStartTimeTV.setVisibility(View.GONE);
                    setStartTimeSlotAdapter(mTimeSlotList);
//                    setEndTimeSlotAdapter(mTimeSlotList);
                }
            }

            @Override
            public void onFailure(Call<TimeSlotModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    //execute get detail api
    private Map<String, String> mEndTimeSlotParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("businessID", business_id);
        mMap.put("date", strCalendarDate);
        mMap.put("serviceID", serID);
        mMap.put("day", String.valueOf(num_day));
        mMap.put("startTime", mFreeTimeSlotsModel.getStartTime());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetEndTimeSlots() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeEndTimeSlotsApi();
        }
    }


    private void executeEndTimeSlotsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getEndSlotsRequest(mEndTimeSlotParams()).enqueue(new Callback<EndTimeModel>() {
            @Override
            public void onResponse(Call<EndTimeModel> call, retrofit2.Response<EndTimeModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                EndTimeModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mEndTimeSlot=mModel.getEndHours();
                    artistNotFountTV.setVisibility(View.GONE);
//                    mStartTimeSlotsRecyclerView.setVisibility(View.VISIBLE);
                    mEndTimeSlotsRecyclerView.setVisibility(View.VISIBLE);
                    txtEndTimeTV.setVisibility(View.VISIBLE);
                    txtStartTimeTV.setVisibility(View.VISIBLE);
                    //Set Up Recycler View Data
//                    setStartTimeSlotAdapter(mTimeSlotList);
                    setEndTimeSlotAdapter(mEndTimeSlot);
                } else {
                    artistNotFountTV.setVisibility(View.VISIBLE);
                    mStartTimeSlotsRecyclerView.setVisibility(View.GONE);
                    mEndTimeSlotsRecyclerView.setVisibility(View.GONE);
                    txtEndTimeTV.setVisibility(View.GONE);
                    txtStartTimeTV.setVisibility(View.GONE);
//                    setStartTimeSlotAdapter(mTimeSlotList);
                    setEndTimeSlotAdapter(mEndTimeSlot);
                }
            }

            @Override
            public void onFailure(Call<EndTimeModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Set Time Slot Adapter
     * */
    private void setStartTimeSlotAdapter(List<TimeSlotModel.FreeSlot> mTimeSlotsArrayList) {
        mStartTimeSlotsAdapter = new StartTimeSlotsAdapter(mActivity, (ArrayList<TimeSlotModel.FreeSlot>) mTimeSlotsArrayList, mSelectTimeSlotInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mStartTimeSlotsRecyclerView.setLayoutManager(mLayoutManagerC);
        mStartTimeSlotsRecyclerView.setAdapter(mStartTimeSlotsAdapter);

    }

    /*
     * Set Time Slot Adapter
     * */
    private void setEndTimeSlotAdapter(String mEndTimeSlot) {
        mEndTimeSlotsAdapter = new EndTimeSlotsAdapter(mActivity, mEndTimeSlotInterface , mEndTimeSlot);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        mEndTimeSlotsRecyclerView.setLayoutManager(mLayoutManagerC);
        mEndTimeSlotsRecyclerView.setAdapter(mEndTimeSlotsAdapter);
    }

    @OnClick({R.id.txtSubmitTV,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSubmitTV:
                performBookNowClick();
                break;
                case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void performBookNowClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeBookAppointmentApi();
            }
        }
    }

    //execute get detail api
    private Map<String, String> mBookingParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("businessID", business_id);
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        mMap.put("day", String.valueOf(num_day));
        mMap.put("endTime", mEndTimeSlot);
        mMap.put("startTime", mFreeTimeSlotsModel.getStartTime());
        mMap.put("serviceID", mServicesModel.getSerID());
        mMap.put("date", strCalendarDate);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeBookAppointmentApi() {
        showProgressDialog(mActivity);
//        strStartTimeInMillies = (strCalendarDate + " " + mFreeTimeSlotsModel.getStartTime());
//        strEndTimeInMillies = (strCalendarDate + " " + mEndFreeTimeSlotsModel.getEndTime());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.bookAppointmentRequest(mBookingParams()).enqueue(new Callback<BookModel>() {
            @Override
            public void onResponse(Call<BookModel> call, retrofit2.Response<BookModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                BookModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<BookModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private boolean isValidate() {
        boolean flag = true;
            if (strCalendarDate.equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_select_date));
                flag = false;
            }
        else if (mFreeTimeSlotsModel.getStartTime() == null || mFreeTimeSlotsModel.getStartTime().length() == 0) {
            showAlertDialog(mActivity, getString(R.string.please_select_start_time));
            flag = false;
        } else if (mEndTimeSlot == null || mEndTimeSlot.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_end_time));
            flag = false;
        }
        return flag;
    }


    //execute get detail api
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("businessID", business_id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetServices() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeServicesDetailApi();
        }
    }


    private void executeServicesDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getBusinessDetailRequest(mParams()).enqueue(new Callback<BusinessDetailModel>() {
            @Override
            public void onResponse(Call<BusinessDetailModel> call, retrofit2.Response<BusinessDetailModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                BusinessDetailModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
//                    review=mModel.getReview();
                    mServicesList = new ArrayList<>();
                    mBusinessDetailList = mModel.getBusinessDetails();

                    for (int i = 0; i < mBusinessDetailList.size(); i++) {
                        mServicesList = mBusinessDetailList.get(i).getServices();
                    }
                    setAdapter();

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<BusinessDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void setAdapter() {
        if (mServicesList.size() > 0)
            servicesRV.setBackgroundResource(R.color.colorPurpleLightBg);
        servicesRV.setNestedScrollingEnabled(false);
        mServicesAdapter = new SelectServiceAdapter(mActivity, mServicesList, mSelectServiceInterface,mServiceInterface);
        RecyclerView.LayoutManager mLayoutManagerC = new LinearLayoutManager(mActivity);
        servicesRV.setLayoutManager(mLayoutManagerC);
        servicesRV.setAdapter(mServicesAdapter);
    }
}
