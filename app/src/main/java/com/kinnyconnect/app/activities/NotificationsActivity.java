package com.kinnyconnect.app.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.NotificationAdapter;
import com.kinnyconnect.app.adapters.ServicesAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.DeleteItemInterface;
import com.kinnyconnect.app.interfaces.FindBeautyInterface;
import com.kinnyconnect.app.interfaces.IncDecClickInterface;
import com.kinnyconnect.app.model.AddToCartModel;
import com.kinnyconnect.app.model.NotificationsModel;
import com.kinnyconnect.app.model.IncDecQuantityModel;
import com.kinnyconnect.app.model.ManageBusinessModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = NotificationsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = NotificationsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.notificationRV)
    RecyclerView notificationRV;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    List<NotificationsModel.Datum> mNotificationsList;
    NotificationAdapter mNotificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getNotificationsDetails();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setNotificationAdapter() {
        mNotificationAdapter = new NotificationAdapter(mActivity, (ArrayList<NotificationsModel.Datum>) mNotificationsList);
        notificationRV.setLayoutManager(new LinearLayoutManager(mActivity));
        notificationRV.setAdapter(mNotificationAdapter);
    }


    @OnClick({R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }
    
    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.ID,null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getNotificationsDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetNotificationsDetailsApi();
        }
    }

    private void executeGetNotificationsDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getNotificationRequest(mParams()).enqueue(new Callback<NotificationsModel>() {
            @Override
            public void onResponse(Call<NotificationsModel> call, Response<NotificationsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                NotificationsModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mNotificationsList=mModel.getData();
                    setNotificationAdapter();
                    txtNoDataFountTV.setVisibility(View.GONE);
                } else  {
//                    showAlertDialog(mActivity, mModel.getMessage());
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
//                    mNotificationAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<NotificationsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

}
