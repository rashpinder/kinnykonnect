package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;

import com.kinnyconnect.app.R;
import com.suke.widget.SwitchButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacySettingsActivity extends BaseActivity implements SwitchButton.OnTouchListener,SwitchButton.OnCheckedChangeListener{
    /**
     * Getting the Current Class Name
     */
    String TAG = PrivacySettingsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = PrivacySettingsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.switchNotificationSB)
    SwitchButton switchNotificationSB;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.switchLocationSB)
    SwitchButton switchLocationSB;
    boolean GpsStatus ;
    LocationManager locationManager;
    static Boolean isTouched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_settings);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        switchLocationSB.setOnTouchListener(this);
        switchLocationSB.setOnCheckedChangeListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        CheckGpsStatus();
    }

    public void CheckGpsStatus(){
         locationManager = (LocationManager)mActivity.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(GpsStatus == true) {
            switchLocationSB.setChecked(true);
        } else {
            switchLocationSB.setChecked(false);
        }
    }
    @OnClick({R.id.imgBackIV,R.id.switchNotificationSB,R.id.switchLocationSB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
            case R.id.switchNotificationSB:
                performNotificationClick();
                break;
                case R.id.switchLocationSB:
                performLocationClick();
                break;
        }
    }

    private void performLocationClick() {
        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    private void performNotificationClick() {

    }

    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        isTouched = true;
        return false;
    }

    @Override
    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
        switch (view.getId()) {
            case R.id.switchLocationSB:
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        performLocationClick();
                    } else {
                        performLocationClick();
                    }
                }
}}}
