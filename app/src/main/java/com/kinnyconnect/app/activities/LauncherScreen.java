package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import com.kinnyconnect.app.utils.WelcomePrefrences;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.ButterKnife;

public class LauncherScreen extends BaseActivity {
    /*
     * Initlaize Activity...
     * */
    Activity mActivity = LauncherScreen.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = LauncherScreen.this.getClass().getSimpleName();
    private String notification_type, title, message, roomId, eventId, notification_id = "", notification_read_status = "", created = "", expireDate = "";
    boolean isNotification = false;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher_screen);
        ButterKnife.bind(this);
        printKeyHash(mActivity);
        if (getIntent().getExtras() != null) {
            // notification_type = String.valueOf(getIntent().getExtras().get("type"));
            JSONObject data = null;
            try {
                if (getIntent().getStringExtra("data") != null) {
                    data = new JSONObject(getIntent().getStringExtra("data"));
                    notification_type = data.getString("notification_type");
                    notification_id = data.getString("notification_id");
                    title = data.getString("title");
                    message = data.getString("message");
                    notification_read_status = data.getString("notification_read_status");
                    expireDate = data.getString("expireDate");
                    created = data.getString("created");
                    Log.e(TAG, "TYPE::" + title + "and" + message);


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (notification_type != null) {
                isNotification = true;
                if (notification_type != null) {
                    if (notification_type.equalsIgnoreCase("1")) {

                        intent = new Intent(this, NotificationsActivity.class);

                    } else if (notification_type.equalsIgnoreCase("2")) {
                        intent = new Intent(this, DrawerActivity.class);
                        KinnyConnectPreferences.writeInteger(mActivity, KinnyConnectPreferences.TYPE_TWO, 2);

                    } else {
                        intent = new Intent(this, SplashScreen.class);
                    }
                } else {
                    intent = new Intent(this, SplashScreen.class);
                }
                startActivity(intent);
                finish();

                if (!isNotification) {
                    setUpSplash();
                }
            } else {
                setUpSplash();
            }
        } else {
            setUpSplash();
        }
        //setCountryAutoComplete();

    }

    public String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));
                Log.e(TAG, "Key Hash=++++++=" + key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }
        return key;
    }

    private void setUpSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!WelcomePrefrences.readBoolean(mActivity, WelcomePrefrences.REMEMBER, false)) {
                    Intent i = new Intent(LauncherScreen.this, SplashScreen.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(LauncherScreen.this, DrawerActivity.class);
                    startActivity(i);
                    finish();
                }
            }

        }, 2000);
    }
}


//package com.kinnyconnect.app.activities;
//import android.app.Activity;
//import android.content.Intent;
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.content.pm.Signature;
//import android.os.Bundle;
//import android.os.Handler;
//import android.util.Base64;
//import android.util.Log;
//
//import com.kinnyconnect.app.R;
//
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
//
//public class LauncherScreen extends BaseActivity {
//    /*
//     * Initlaize Activity...
//     * */
//    Activity mActivity = LauncherScreen.this;
//    /**
//     * Getting the Current Class Name
//     */
//    String TAG = LauncherScreen.this.getClass().getSimpleName();
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_launcher_screen);
//        setStatusBar(mActivity);
//
//        printKeyHash(mActivity);
//
//        setUpSplash();
//    }
//
//    private void setUpSplash() {
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                    Intent i = new Intent(LauncherScreen.this, SplashScreen.class);
//                    startActivity(i);
//                    finish();
//            }
//        }, 2000);
//    }
//
//
//    public String printKeyHash(Activity context) {
//        PackageInfo packageInfo;
//        String key = null;
//        try {
//            //getting application package name, as defined in manifest
//            String packageName = context.getApplicationContext().getPackageName();
//
//            //Retriving package info
//            packageInfo = context.getPackageManager().getPackageInfo(packageName,
//                    PackageManager.GET_SIGNATURES);
//
//            Log.e("Package Name=", context.getApplicationContext().getPackageName());
//
//            for (Signature signature : packageInfo.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                key = new String(Base64.encode(md.digest(), 0));
//                Log.e(TAG, "Key Hash=++++++=" + key);
//            }
//        } catch (PackageManager.NameNotFoundException e1) {
//            Log.e("Name not found", e1.toString());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e("No such an algorithm", e.toString());
//        } catch (Exception e) {
//            Log.e("Exception", e.toString());
//        }
//        return key;
//    }
//}
