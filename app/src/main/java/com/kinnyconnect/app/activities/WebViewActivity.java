package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.utils.Constants;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebViewActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = WebViewActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Intance
     */
    Activity mActivity = WebViewActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;
    @BindView(R.id.webViewWV)
    WebView webViewWV;
    @BindView(R.id.mProgressBarPB)
    AVLoadingIndicatorView mProgressBarPB;

    /*
     * Initialize object...
     * */

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        //Get Intent Data
        getIntentData();
    }

    private void getIntentData() {
        if (getIntent().getStringExtra(Constants.WV_TYPE).equals(Constants.HELP_SUPPORT)) {
            setHelpSupport(Constants.HELP_SUPPORT_WEB);
        }
        else if (getIntent().getStringExtra(Constants.WV_TYPE).equals(Constants.PRIVACY_POLICY)) {
//            setPrivacyPolicy(Constants.PRIVACY_POLICY_WEB);
        }
    }

    @OnClick(R.id.imgBackIV)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        finish();
    }


    private void setDataOnWidgets(String strWebUrl) {
        mProgressBarPB.setVisibility(View.VISIBLE);
        // To Show PDF IN WebView
        webViewWV.getSettings().setJavaScriptEnabled(true);
        webViewWV.getSettings().setBuiltInZoomControls(true);
        webViewWV.getSettings().setLoadWithOverviewMode(true);
        webViewWV.getSettings().setUseWideViewPort(true);
        webViewWV.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mProgressBarPB.setVisibility(View.VISIBLE);
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                mProgressBarPB.setVisibility(View.GONE);
            }
        });

        webViewWV.loadUrl(strWebUrl);
    }

    private void setHelpSupport(String strUrl) {
        txtTitleTV.setText(getString(R.string.help));
        setDataOnWidgets(strUrl);
    }

    private void setPrivacyPolicy(String strUrl) {
        txtTitleTV.setText(getString(R.string.privacy_policy));
        setDataOnWidgets(strUrl);
    }

}
