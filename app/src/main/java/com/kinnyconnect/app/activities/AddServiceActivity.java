package com.kinnyconnect.app.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.AddToCartModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddServiceActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = AddServiceActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AddServiceActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editServiceNameET)
    EditText editServiceNameET;
    @BindView(R.id.timeSpinner)
    Spinner timeSpinner;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.editDescriptionET)
    EditText editDescriptionET;
    @BindView(R.id.editPriceET)
    EditText editPriceET;
    @BindView(R.id.txtAddServiceTV)
    TextView txtAddServiceTV;
    @BindView(R.id.mainRL)
    RelativeLayout mainRL;
    String time = "";

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        setTimeAdapter();
        mainRL.setOnTouchListener((v, event) -> {
            hideKeyBoard(mActivity,getCurrentFocus());
            return true;
        });
    }

    /*
     * Time adapter
     * */
    private void setTimeAdapter() {
        hideKeyBoard(mActivity, this.timeSpinner);
        Typeface font = Typeface.createFromAsset(mActivity.getAssets(),
                "RalewayRegular.ttf");
        String[] mStatusArry = getResources().getStringArray(R.array.time);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mStatusArry) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner_time, null);
                }
                TextView itemTextView = v.findViewById(R.id.itemTimeTV);
//                if (position == 0) {
//                    itemTextView.setVisibility(View.INVISIBLE);
//                }
                itemTextView.setText(mStatusArry[position]);
                itemTextView.setTypeface(font);
                return v;
            }
        };

        timeSpinner.setAdapter(adapter);

        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                time = mStatusArry[position];
                if (position == 0) {
                    ((TextView) view).setTextSize(13);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                } else {
                    ((TextView) view).setTextSize(13);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                timeSpinner.setSelection(0);
            }
        });

        timeSpinner.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(timeSpinner.getWindowToken(), 0);
                return false;
            }
        }) ;
    }




    @OnClick({R.id.txtAddServiceTV, R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtAddServiceTV:
                performAddServiceClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;

        }
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("businessID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        mMap.put("title", editServiceNameET.getText().toString().trim());
        mMap.put("description", editDescriptionET.getText().toString().trim());
        mMap.put("price", editPriceET.getText().toString().trim());
        mMap.put("time", timeSpinner.getSelectedItem().toString());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performAddServiceClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeAddServiceApi();
            }
        }
    }


    private void executeAddServiceApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addServiceRequest(mParams()).enqueue(new Callback<AddToCartModel>() {
            @Override
            public void onResponse(Call<AddToCartModel> call, Response<AddToCartModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                AddToCartModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddToCartModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (editServiceNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_service_name));
            flag = false;
        }else if (editServiceNameET.getText().toString().trim().length()>20) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_service_name));
            flag = false;
        } else if (editPriceET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_price));
            flag = false;
        } else if (editPriceET.getText().toString().trim().length()>5) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_price));
            flag = false;
        } else if (editDescriptionET.getText().toString().trim().length()>40) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_desc));
            flag = false;
        }
        return flag;
    }
}
