package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.ReedemPointsModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RedeemPointsActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = RedeemPointsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = RedeemPointsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtYourNumberTV)
    TextView txtYourNumberTV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    SpannableStringBuilder sb;
    StyleSpan bss;
    ForegroundColorSpan fcs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_points);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getReedemPoints();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getReedemPoints() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeReedemPointsApi();
        }
    }


    private void executeReedemPointsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getPointsRequest(mParams()).enqueue(new Callback<ReedemPointsModel>() {
            @Override
            public void onResponse(Call<ReedemPointsModel> call, Response<ReedemPointsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ReedemPointsModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    String num = txtYourNumberTV.getText() + " " + mModel.getPoints();
                    Spannable sb = new SpannableString(num);

                    /** for bold for example. substract the endselection from startselection to get the length **/
                    sb.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 92, num.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); //bold
                    /** do more styles ...**/
                    sb.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorBtn)), 92, num.length(), 0);

                    sb.setSpan(new RelativeSizeSpan(1.5f), 92, num.length(), 0); // set size
                    txtYourNumberTV.setText(sb);

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ReedemPointsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    @OnClick({R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }

    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
