package com.kinnyconnect.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.FindBeautyAdapter;
import com.kinnyconnect.app.adapters.FindBeautyDesAdapter;
import com.kinnyconnect.app.adapters.ShopItemAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.FindBeautyInterface;
import com.kinnyconnect.app.model.CategoryListingModel;
import com.kinnyconnect.app.model.FindBeautyModel;
import com.kinnyconnect.app.model.ShopListModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopItemActivity extends BaseActivity  implements ShopItemAdapter.PaginationInterface{
    /**
     * Getting the Current Class Name
     */
    String TAG = ShopItemActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ShopItemActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.ShopItemRV)
    RecyclerView ShopItemRV;
    @BindView(R.id.txtItemTV)
    TextView txtItemTV;
    @BindView(R.id.lengthTxt)
    TextView lengthTxt;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    ShopItemAdapter mShopItemAdapter;
    List<ShopListModel.ProductDetail> mProductsDetail;
    private FindBeautyInterface listener;
    int count = 0;
    private int page_no = 1;
    private int item_count = 10;
    private String strLastPage = "FALSE";
    String cat_id;
    String title;
    private List<ShopListModel.ProductDetail> tempArrayList = new ArrayList<>();
    ShopItemAdapter.PaginationInterface paginationInterface;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_item);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getIntentData();
        setData();
        paginationInterface =this;
        if (tempArrayList != null) {
            tempArrayList.clear();
        }
        if (mProductsDetail != null) {
            mProductsDetail.clear();
        }
        page_no=1;
        getShopItemsList();

    }

    private void setData() {
        count = title.length();
        if (count >= 1 && count <= 4) {
            lengthTxt.getLayoutParams().width = 140;
            lengthTxt.getLayoutParams().height = 20;
        } else if (count >= 4 && count <= 7) {
            lengthTxt.getLayoutParams().width = 180;
            lengthTxt.getLayoutParams().height = 20;
        }
        else if (count >= 8 && count <= 12) {
            lengthTxt.getLayoutParams().width = 250;
            lengthTxt.getLayoutParams().height = 20;
        }
        else if(count >= 12 && count <= 18) {
            lengthTxt.getLayoutParams().width = 350;
            lengthTxt.getLayoutParams().height = 20;
        }
        else if(count >= 18 && count <= 24) {
            lengthTxt.getLayoutParams().width = 450;
            lengthTxt.getLayoutParams().height = 20;
        }
        else if(count >= 24 && count <= 30) {
            lengthTxt.getLayoutParams().width = 550;
            lengthTxt.getLayoutParams().height = 20;

        } else if(count >= 30 && count <= 36) {
            lengthTxt.getLayoutParams().width = 650;
            lengthTxt.getLayoutParams().height = 20;
        } else if (count >= 36 && count <= 42) {
            lengthTxt.getLayoutParams().width = 750;
            lengthTxt.getLayoutParams().height = 20;}
        else {
            lengthTxt.getLayoutParams().width = 160;
            lengthTxt.getLayoutParams().height = 20;
        }

        txtItemTV.setText(title);
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("category_id") != null) {
                cat_id = (String) getIntent().getExtras().get("category_id");}

                title = (String) getIntent().getExtras().get("title");

        }
    }
    /*
  Execute api
   */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("catID", cat_id);
        mMap.put("pageNo",String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getShopItemsList() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetShopListApi();
        }
    }

    private void executeGetShopListApi() {
        if (page_no == 1) {
            showProgressDialog(Objects.requireNonNull(mActivity));
        } else if (page_no > 1) {
            dismissProgressDialog();
//                mProgressBar.setVisibility(View.GONE);
        }
//        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getShopItemsListRequest(mParam()).enqueue(new Callback<ShopListModel>() {
            @Override
            public void onResponse(Call<ShopListModel> call, Response<ShopListModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ShopListModel mModel = response.body();

                mProductsDetail=new ArrayList<>();
                mProductsDetail=mModel.getProductDetails();
strLastPage=mModel.getLast_page();
                if (page_no == 1) {
                    dismissProgressDialog();
                }
//                if (page_no > 1) {
//                    assert mModel != null;
//                    if (mModel.getProductDetails() == null) {
//                        strLastPage = "TRUE";
//                    } else if (mModel.getProductDetails().size() < 10) {
////                        Toast.makeText(mActivity(), "No more items!!!", Toast.LENGTH_SHORT).show();
//                        strLastPage = "TRUE";
//                    } else {
//                        strLastPage = "FALSE";
//                    }
//                }


                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mProductsDetail = mModel.getProductDetails();
                    } else if (page_no > 1) {
                        tempArrayList = mModel.getProductDetails();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mProductsDetail.addAll(tempArrayList);
                        }
                    }

                    if (page_no == 1) {
                        setShopItemAdapter();

                    } else {
                        mShopItemAdapter.notifyDataSetChanged();
                    }
                    txtNoDataFountTV.setVisibility(View.GONE);
                } else {
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
//                    showAlertDialog(Objects.requireNonNull(mActivity), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ShopListModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            if (strLastPage.equals("FALSE")){
            showProgressDialog(Objects.requireNonNull(mActivity));
//            mProgressBar.setVisibility(View.VISIBLE);

            page_no++;}
            else {
                dismissProgressDialog();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeGetShopListApi();
                    } else {
                        if(progressDialog.isShowing()){
                            dismissProgressDialog();}
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }


    private void setShopItemAdapter() {
        if (mActivity!=null){
            GridLayoutManager manager = new GridLayoutManager(mActivity, 2, GridLayoutManager.VERTICAL, false);
            ShopItemRV.setLayoutManager(manager);
            mShopItemAdapter = new ShopItemAdapter(mActivity, (ArrayList<ShopListModel.ProductDetail>) mProductsDetail, paginationInterface);
            ShopItemRV.setAdapter(mShopItemAdapter);
            mShopItemAdapter.notifyDataSetChanged();}}
//        }

//    @OnClick({R.id.imgSMSIV, R.id.imgFbIV, R.id.imgMailIV, R.id.copyLinkRL,R.id.imgBackIV})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.imgSMSIV:
//                performSMSClick();
//                break;
//            case R.id.imgFbIV:
//                performFbClick();
//                break;
//            case R.id.imgMailIV:
//                performMailClick();
//                break;
//            case R.id.copyLinkRL:
//                performCopyLinkClick();
//                break;
//            case R.id.imgBackIV:
//                performBackClick();
//                break;
//
//        }
//    }



    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(mActivity, DrawerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
