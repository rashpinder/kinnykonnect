package com.kinnyconnect.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.BookingPurchaseAdapter;
import com.kinnyconnect.app.adapters.FindBeautyAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.BookingPurchaseModel;
import com.kinnyconnect.app.model.BookingPurchaseModel;
import com.kinnyconnect.app.model.CategoryListingModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingPurchaseActivity extends BaseActivity {
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.bookingPurchaseRV)
    RecyclerView bookingPurchaseRV;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    List<BookingPurchaseModel.Datum> mPurchaseList;
    BookingPurchaseAdapter bookingPurchaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_purchase);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getBookingPurchaseDetails();
    }

    @OnClick({R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }
    private void getBookingPurchaseDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetBookingPurchaseDetailsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetBookingPurchaseDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getBookingPurchaseRequest(mParam()).enqueue(new Callback<BookingPurchaseModel>() {
            @Override
            public void onResponse(Call<BookingPurchaseModel> call, Response<BookingPurchaseModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                BookingPurchaseModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    setPurchaseAdapter();
                    txtNoDataFountTV.setVisibility(View.GONE);
                } else {
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
//                    bookingPurchaseAdapter.notifyDataSetChanged();
//                    showAlertDialogFinish(mActivity,"No Data Available");
                }
            }

            private void setPurchaseAdapter() {
                    bookingPurchaseAdapter = new BookingPurchaseAdapter(Objects.requireNonNull(mActivity), (ArrayList<BookingPurchaseModel.Datum>) mPurchaseList);
                bookingPurchaseRV.setLayoutManager(new LinearLayoutManager(Objects.requireNonNull(mActivity)));
                bookingPurchaseRV.setAdapter(bookingPurchaseAdapter);
                    bookingPurchaseAdapter.notifyDataSetChanged();
                }

            @Override
            public void onFailure(Call<BookingPurchaseModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
