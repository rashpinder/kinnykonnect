package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.BeautyHorizontalAdapter;
import com.kinnyconnect.app.adapters.FindBeautyDesAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.FindBeautyInterface;
import com.kinnyconnect.app.model.FindBeautyModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kinnyconnect.app.activities.DrawerActivity.editAddressET;

public class HomeActivity extends BaseActivity implements FindBeautyDesAdapter.PaginationInterface, FindBeautyInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = HomeActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.beautyRV)
    RecyclerView beautyRV;
    @BindView(R.id.txtLocationTV)
    TextView txtLocationTV;
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgLocIV)
    ImageView imgLocIV;
    @BindView(R.id.BeautyHorizontalRV)
    RecyclerView BeautyHorizontalRV;
    @BindView(R.id.txtTitleTV)
    TextView txtTitleTV;
    @BindView(R.id.imgBusinessIV)
    ImageView imgBusinessIV;
    FindBeautyDesAdapter mFindBeautyDescAdapter;
    BeautyHorizontalAdapter mhorizontalTextAdapter;
    String cat_id;
    String title;
    String latitude;
    String longitude;
    List<FindBeautyModel.Datum> mDataList;
    List<FindBeautyModel.CategoryListing> mCategoryList;
    private FindBeautyInterface listener;
    FindBeautyModel mModel;
    private int page_no = 1;
    private final int item_count = 10;
    private String strLastPage = "FALSE";
    private List<FindBeautyModel.Datum> tempArrayList = new ArrayList<>();

    FindBeautyDesAdapter.PaginationInterface paginationInterface;
    LatLng latLng = null;
    String title_var = "1";
    String img;
    int pos = 0;
    String address;
    int AUTOCOMPLETE_REQUEST_CODE = 5;
    public double mLatitude;
    public double mLongitude;

    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    RequestOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        imgMenuIV.setVisibility(View.GONE);
        imgBackIV.setVisibility(View.VISIBLE);
        txtLocationTV.setText(KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.SELECTED_LOCATION, ""));

        getIntentData();
        options = new RequestOptions()
                .placeholder(R.drawable.ic_home_ph)
                .error(R.drawable.ic_home_ph)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();

//        setData();
        paginationInterface = this;
        listener = this;


    }

    @OnClick({R.id.imgLocIV,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgLocIV:
                setSearchIntent();
                break;
                case R.id.imgBackIV:
                performBackClick();
                break;
        }
    }

    private void performBackClick() {
        startActivity(new Intent(mActivity,DrawerActivity.class));
        finish();
    }


    public void setSearchIntent() {
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(mActivity);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                latLng = place.getLatLng();
                mLatitude = latLng.latitude;
                mLongitude = latLng.longitude;
                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LAT_NEW, String.valueOf(mLatitude));
                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LNG_NEW, String.valueOf(mLongitude));
                Log.e(TAG, "*********SLATITUDE********" + mLatitude);
                Log.e(TAG, "*********SLONGITUDE********" + mLongitude);

                address = place.getAddress();
                editAddressET.setText(address);
                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.SELECTED_LOCATION, address);
                Log.e(TAG, "Place: " + place.getName() + ", " + place.getId() + ", " + place.getLatLng() + ", " + place.getAddress());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.e(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
//                showToast(mActivity,"Error");
                // The user canceled the operation.
            }

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (tempArrayList != null) {
            tempArrayList.clear();
        }
        if (mDataList != null) {
            mDataList.clear();
        }
        if (mCategoryList != null) {
            mCategoryList.clear();
        }
        txtLocationTV.setText(KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.SELECTED_LOCATION, ""));

        performGetFindBeauty();

    }
//    private void setData() {
//        txtTitleTV.setText(title);
//    }

    /*
    Execute api
     */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        mMap.put("catID", cat_id);
        mMap.put("latitude", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LAT_NEW, null));
        mMap.put("longitude", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LNG_NEW, null));
        mMap.put("pageNo", String.valueOf(page_no));
        mMap.put("distance", "100");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performGetFindBeauty() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            page_no = 1;
            executeFindBeautyApi();
        }
    }

    private void executeFindBeautyApi() {
        if (page_no == 1) {
            showProgressDialog(Objects.requireNonNull(mActivity));
        } else if (page_no > 1) {
            dismissProgressDialog();
//                mProgressBar.setVisibility(View.GONE);
        }
//        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.findBeautyRequest(mParam()).enqueue(new Callback<FindBeautyModel>() {
            @Override
            public void onResponse(Call<FindBeautyModel> call, Response<FindBeautyModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                mModel = response.body();

                mDataList = new ArrayList<>();
                mCategoryList = new ArrayList<>();
                mCategoryList = mModel.getCategoryListing();
                mDataList = mModel.getData();
                strLastPage = mModel.getLast_page();
                if (page_no == 1) {
                    dismissProgressDialog();
                }
//                if (page_no > 1) {
//                    assert mModel != null;
//                    if (mModel.getData() == null) {
//                        strLastPage = "TRUE";
//                    } else if (mModel.getData().size() < 10) {
////                        Toast.makeText(mActivity(), "No more items!!!", Toast.LENGTH_SHORT).show();
//                        strLastPage = "TRUE";
//                    } else {
//                        strLastPage = "FALSE";
//                    }
//                }


                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mDataList = mModel.getData();
                    } else if (page_no > 1) {
                        tempArrayList = mModel.getData();
                    } else {
                        mDataList.clear();
                        tempArrayList.clear();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mDataList.addAll(tempArrayList);
                        }
                    }

                    if (page_no == 1) {
                        setHorizontalBeautyAdapter();
                        setBeautyAdapter();

                    } else {
//                        mFindBeautyDescAdapter.notifyDataSetChanged();
//                        mhorizontalTextAdapter.notifyDataSetChanged();
                    }
//                    setHorizontalBeautyAdapter();
//                    if (title_var.equals("1")){
//                        txtTitleTV.setText(mModel.getCatID());}

                    txtTitleTV.setText(title);
                    Glide.with(mActivity).load(img).apply(options).into(imgBusinessIV);

                    txtNoDataFountTV.setVisibility(View.GONE);
                } else {
                    if (mDataList != null) {
                        mDataList.clear();

                    }
                    setHorizontalBeautyAdapter();
                    setBeautyAdapter();
                    if (title != null) {
                        txtTitleTV.setText(title);
                    }
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
//                    setHorizontalBeautyAdapter();
//                    showAlertDialog(Objects.requireNonNull(mActivity), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<FindBeautyModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("category_id") != null) {
                cat_id = (String) getIntent().getExtras().get("category_id");
                title = (String) getIntent().getExtras().get("title");
                latitude = (String) getIntent().getExtras().get("latitude");
                longitude = (String) getIntent().getExtras().get("longitude");
                pos = (int) getIntent().getExtras().get("pos");
                img = (String) getIntent().getExtras().get("img");
            }
        }
    }

    private void setBeautyAdapter() {
        mFindBeautyDescAdapter = new FindBeautyDesAdapter(mActivity, (ArrayList<FindBeautyModel.Datum>) mDataList, mModel, paginationInterface);
        beautyRV.setLayoutManager(new LinearLayoutManager(mActivity));
        beautyRV.setAdapter(mFindBeautyDescAdapter);
        mFindBeautyDescAdapter.notifyDataSetChanged();
    }

    private void setHorizontalBeautyAdapter() {
        mhorizontalTextAdapter = new BeautyHorizontalAdapter(mActivity, (ArrayList<FindBeautyModel.CategoryListing>) mCategoryList, listener, txtTitleTV, pos,img);
        BeautyHorizontalRV.setLayoutManager(new LinearLayoutManager(mActivity, RecyclerView.HORIZONTAL, false));
        BeautyHorizontalRV.setAdapter(mhorizontalTextAdapter);
        BeautyHorizontalRV.getLayoutManager().scrollToPosition(pos);
        mhorizontalTextAdapter.notifyDataSetChanged();

    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            if (strLastPage.equals("FALSE")) {
                showProgressDialog(Objects.requireNonNull(mActivity));
//            mProgressBar.setVisibility(View.VISIBLE);

                page_no++;
            } else {
                dismissProgressDialog();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeFindBeautyApi();
                    } else {
                        if (progressDialog.isShowing()) {
                            dismissProgressDialog();
                        }
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }


    @Override
    public void getData(String cat_id, String title, int pos, String img) {
        this.cat_id = cat_id;
        this.title = title;
        this.pos = pos;
        txtTitleTV.setText(title);
        title_var = "2";
        this.img = img;
        Glide.with(mActivity).load(img).apply(options).into(imgBusinessIV);
        executeFindBeautyApi();
    }
}
