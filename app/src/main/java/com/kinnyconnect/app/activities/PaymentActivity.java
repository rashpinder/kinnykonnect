package com.kinnyconnect.app.activities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.activities.BaseActivity;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.ApplyCouponModel;
import com.kinnyconnect.app.model.CardModel;
import com.kinnyconnect.app.model.PaymentModel;
import com.kinnyconnect.app.utils.Constants;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class PaymentActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = PaymentActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = PaymentActivity.this;
    /**
     * Widgets
     */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.cvcET)
    EditText cvcET;
    @BindView(R.id.txtDoneTV)
    TextView txtDoneTV;

    @BindView(R.id.cardNumberET)
    EditText cardNumberET;

    @BindView(R.id.expiayDateTV)
    TextView expiayDateTV;

    @BindView(R.id.cardHolderNameET)
    EditText cardHolderNameET;

    //Initialize
    Card card;
    Stripe stripe = null;
    String cardNumber, cvc;
    String expirayYear;
    String expiraydate;
    String stripe_token = "";
    String eventId = "";
    String ticketId = "";
    String total_price = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            total_price = getIntent().getStringExtra(Constants.TOTAL_PRICE);
//            eventId = mModel.getEventDetail().getEventId();
//            ticketId = mModel.getEventDetail().getTicketId();
        }

    }

    /*
     * Widget Click Listner
     * */
    @OnClick({R.id.imgBackIV,R.id.txtDoneTV,R.id.expiayDateTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                setBackClick();
                break;
                case R.id.expiayDateTV:
                setExpiryClick();
                break;
            case R.id.txtDoneTV:
                if (isValidate()) {
                    onNextClick();
                }
                break;

        }
    }

    private void setExpiryClick() {
            Calendar calendar = Calendar.getInstance(Locale.getDefault());

            calendar.set(Calendar.HOUR_OF_DAY, Calendar.HOUR);
            calendar.set(Calendar.MINUTE, Calendar.MINUTE);

            final Date defaultDate = calendar.getTime();
            Date currentDate = new Date(System.currentTimeMillis() - 1000);
            new SingleDateAndTimePickerDialog.Builder(this)
                    .bottomSheet()
                    .curved()
                    .mainColor(getResources().getColor(R.color.colorBlack))
                    .defaultDate(defaultDate)
                    .minDateRange(currentDate)
                    .displayMinutes(false)
                    .displayHours(false)
                    .displayDays(false)
                    .displayMonth(true)
                    .displayYears(true)
                    .titleTextColor(getResources().getColor(R.color.colorBtn))
                    .displayDaysOfMonth(false)
                    // .display()
                    .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                        @Override
                        public void onDisplayed(SingleDateAndTimePicker picker) {

                        }
                    })
                    .title(" Expiry Date  ")
                    .listener(new SingleDateAndTimePickerDialog.Listener() {
                        @Override
                        public void onDateSelected(Date date) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    SimpleDateFormat DateFor11 = new SimpleDateFormat("EEE d MMM HH:mm", Locale.getDefault());
                                    String s = DateFor11.format(date);
                                    // String s=date;

                                    SimpleDateFormat DateFor12 = new SimpleDateFormat("MM/yyyy");
                                    SimpleDateFormat DateFor13 = new SimpleDateFormat("yyyy");
                                    SimpleDateFormat DateFor14 = new SimpleDateFormat("MM");
                                    String stringDate = DateFor12.format(date);
                                    expiayDateTV.setText(stringDate);
                                    expiraydate = DateFor14.format(date);
                                    expirayYear = DateFor13.format(date);
                                    //  tx_appointmentbook.setText("Submit");
                                }
                            }, 500);
                        }
                    }).display();

        }


    private void setBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void onNextClick() {
//        preventMultipleClick();
//        cardNumber = cardNumberET.getText().toString().trim();
//        cvc = cvcET.getText().toString().trim();
//
//        card = new Card(cardNumber, Integer.valueOf(expiraydate), Integer.valueOf(expirayYear), cvc);
//        if (isCardValid()) {
//            getStripeToken(card);
//        } else {
//            showToast(mActivity, getString(R.string.error));
//        }
    }

    private boolean isCardValid() {
        boolean validation = card.validateCard();
        if (validation) {
            return true;
        } else if (!card.validateNumber()) {
            showAlertDialog(mActivity, getString(R.string.The_card_invalid));
        } else if (!card.validateExpMonth()) {
            showAlertDialog(mActivity, getString(R.string.expiration_month_invalid));

        } else if (!card.validateExpYear()) {
            showAlertDialog(mActivity, getString(R.string.expiration_year_invalid));

        } else if (!card.validateCVC()) {
            showAlertDialog(mActivity, getString(R.string.cvc_invalid));
        } else {
            showAlertDialog(mActivity, getString(R.string.card_details_invalid));
        }
        return false;
    }

    /*
     * To Get Token
     **/
    private void getStripeToken(Card card) {

        // showProgressDialog(mActivity);
        stripe = new Stripe();

        stripe.createToken(card, Constants.PUBLISHABLE_KEY, new TokenCallback() {
            @Override
            public void onError(Exception error) {
                Log.e("ERROR", String.valueOf(error));
            }

            @Override
            public void onSuccess(Token token) {
                Log.e("TOKEN", String.valueOf(token));

                stripe_token = token.getId();
                Log.e(TAG,"Token::"+stripe_token);
                if (stripe_token != null) {
                    if (!isNetworkAvailable(mActivity)) {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    } else {
//                        executeAddStripePymentApi();
                    }
                }
            }
        });
    }

    public boolean isValidate() {
        boolean flag = true;
        if (cardHolderNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_holder_name));
            flag = false;
        } else if (cardNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_number));
            flag = false;
        } else if (expiayDateTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_expiray_date));
            flag = false;
        } else if (cvcET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_cvc));
            flag = false;
        } else if (!(cvcET.getText().length() <= 3)) {
            showAlertDialog(mActivity, getString(R.string.please_enter_3_digit_cvc));
            flag = false;
        }
        return flag;
    }

    /*
//     * Execute api
//     * */
//    private Map<String, String> mPaymentParams() {
//        Map<String, String> mMap = new HashMap<>();
//        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
////        mMap.put("coupanCode", txtCouponTV.getText().toString().trim());
//        Log.e(TAG, "**PARAM**" + mMap.toString());
//        return mMap;
//    }

//    private void executeAddStripePymentApi() {
//            if (!isNetworkAvailable(mActivity)) {
//                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
//            } else {
//                executePaymentApi();
//            }
//
//    }

//    private void executePaymentApi() {
//            showProgressDialog(mActivity);
//            ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
//            mApiInterface.applyCouponRequest(mPaymentParams()).enqueue(new Callback<PaymentModel>() {
//                @Override
//                public void onResponse(Call<PaymentModel> call, Response<PaymentModel> response) {
//                    dismissProgressDialog();
//                    Log.e(TAG, "**RESPONSE**" + response.body());
//                    PaymentModel mModel = response.body();
//                    assert mModel != null;
//                    if (mModel.getStatus().equals("1")) {
//                        showToast(mActivity,mModel.getMessage());
//                    } else {
//                        showAlertDialog(mActivity,mModel.getMessage());
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<PaymentModel> call, Throwable t) {
//                    dismissProgressDialog();
//                    Log.e(TAG, "**ERROR**" + t.getMessage());
//                }
//            });
//        }
}



