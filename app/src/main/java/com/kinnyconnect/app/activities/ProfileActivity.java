package com.kinnyconnect.app.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.AddToCartModel;
import com.kinnyconnect.app.model.ProfileModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ProfileActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ProfileActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.imgProfileIV)
    ImageView imgProfileIV;
    @BindView(R.id.txtNameTV)
    TextView txtNameTV;
    @BindView(R.id.txtUserEmailTV)
    TextView txtUserEmailTV;
    @BindView(R.id.txtAddressFieldTV)
    TextView txtAddressFieldTV;
    @BindView(R.id.txtBioTV)
    TextView txtBioTV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imgEditIV)
    ImageView imgEditIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        imgEditIV.setVisibility(View.VISIBLE);
    }


    @OnClick({R.id.imgEditIV,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgEditIV:
                performEditProfileClick();
                break;
            case R.id.imgBackIV:
                onBackPressed();
                break;

        }
    }

    private void performEditProfileClick() {
        startActivity(new Intent(mActivity,EditProfileActivity.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent mIntent = new Intent(mActivity, DrawerActivity.class);
//        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(mIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserProfileDetails();
    }

    private void getUserProfileDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetUserDetailsApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGetUserDetailsApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProfileDetailsRequest(mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ProfileModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    String imageurl = mModel.getUserDetails().getProfileImage();
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();
                    if (imageurl != null) {
                        Glide.with(mActivity)
                                .load(imageurl)
                                .apply(options)
                                .into(imgProfileIV);
                    } else {
                        String img = KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.IMAGE, null);
                        Glide.with(mActivity)
                                .load(img)
                                .apply(options)
                                .into(imgProfileIV);
                    }
                    txtNameTV.setText(mModel.getUserDetails().getName());
                    txtUserEmailTV.setText(mModel.getUserDetails().getEmail());
                    txtAddressFieldTV.setText(mModel.getUserDetails().getAddress());
                    txtBioTV.setText(mModel.getUserDetails().getBio());
                } else {
//                    showAlertDialog(mActivity,"No Data Available");
                    showToast(mActivity,mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

}
