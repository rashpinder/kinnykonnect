package com.kinnyconnect.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.ServicesProvidedAdapter;
import com.kinnyconnect.app.adapters.ShareExperienceAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.BusinessDetailModel;
import com.kinnyconnect.app.model.GoogleLoginLinkModel;
import com.kinnyconnect.app.model.ManageBusinessModel;
import com.kinnyconnect.app.model.ReedemPointsModel;
import com.kinnyconnect.app.model.RefreshTokenModel;
import com.kinnyconnect.app.utils.Constants;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = DashboardActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = DashboardActivity.this;

    /*
     * Widgets
     * */

    /*
     * Widgets
     * */
    ServicesProvidedAdapter mServicesProvidedAdapter;
    ShareExperienceAdapter mShareExpAdapter;
    @BindView(R.id.servicesProvidedRV)
    RecyclerView servicesProvidedRV;
    @BindView(R.id.ShareExpRV)
    RecyclerView ShareExpRV;
    @BindView(R.id.txtContactTV)
    TextView txtContactTV;
    @BindView(R.id.txtBookTV)
    TextView txtBookTV;
    @BindView(R.id.ratingLL)
    LinearLayout ratingLL;
    @BindView(R.id.txtMessageTV)
    TextView txtMessageTV;
    @BindView(R.id.imgKinnyIV)
    ImageView imgKinnyIV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.txtLocTV)
    TextView txtLocTV;
    @BindView(R.id.ratingbarRB)
    RatingBar ratingbarRB;
    @BindView(R.id.txtViewAllTV)
    TextView txtViewAllTV;
    @BindView(R.id.txtProductNameTV)
    TextView txtProductNameTV;
    @BindView(R.id.txtRatingTV)
    TextView txtRatingTV;
    List<BusinessDetailModel.BusinessDetail> mBusinessDetailList;
    List<BusinessDetailModel.BusinessDetail.Service> mServicesList;
    List<BusinessDetailModel.BusinessDetail.ShareExperience> mShareExperienceList;
    List<BusinessDetailModel.BusinessDetail.MessageBoard> mMessageBoardList;
    String business_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getIntentData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getBusinessDetail();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("business_id") != null) {
                business_id = (String) getIntent().getExtras().get("business_id");
            }
        }
    }

    //execute get detail api
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("businessID", business_id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getBusinessDetail() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeBusinessDetailApi();
        }
    }


    private void executeBusinessDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getBusinessDetailRequest(mParams()).enqueue(new Callback<BusinessDetailModel>() {
            @Override
            public void onResponse(Call<BusinessDetailModel> call, Response<BusinessDetailModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                BusinessDetailModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mServicesList = new ArrayList<>();
                    mShareExperienceList = new ArrayList<>();
                    mMessageBoardList = new ArrayList<>();
                    mBusinessDetailList = mModel.getBusinessDetails();
                    txtProductNameTV.setText(mModel.getBusinessDetails().get(0).getName());
                    txtLocTV.setText(mModel.getBusinessDetails().get(0).getLocation());
                    txtRatingTV.setText(mModel.getBusinessDetails().get(0).getRating());
                   ratingbarRB.setRating(Float.parseFloat(mModel.getBusinessDetails().get(0).getRating()));
                    ratingbarRB.setEnabled(false);
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .dontAnimate()
                            .dontTransform();
                    Glide.with(mActivity)
                            .load(mModel.getBusinessDetails().get(0).getBusinessImage())
                            .apply(options)
                            .into(imgKinnyIV);
                    for (int i = 0; i < mBusinessDetailList.size(); i++) {
                        mServicesList = mBusinessDetailList.get(i).getServices();
                        mShareExperienceList = mBusinessDetailList.get(i).getShareExperience();
                        mMessageBoardList = mBusinessDetailList.get(i).getMessageBoard();
                    }

                    setServicesAdapter();
                    setShareExpAdapter();
                    setMessageBoard();

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<BusinessDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setMessageBoard() {
        for (int i = 0; i < mMessageBoardList.size(); i++) {
            txtMessageTV.setText(mMessageBoardList.get(i).getTitle());

        }
    }

    @OnClick({R.id.txtContactTV, R.id.txtBookTV, R.id.ratingLL,R.id.imgBackIV,R.id.txtViewAllTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtContactTV:
                performContactClick();
                break;
                case R.id.txtViewAllTV:
                performViewAllClick();
                break;
            case R.id.txtBookTV:
                performBookClick();
                break;
                case R.id.imgBackIV:
              onBackPressed();
              finish();
                break;
            case R.id.ratingLL:
                if (KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, false)) {
                    performRatingClick();}
                else {
                    showLoginAlertDialog(mActivity, "Please Login First");
                }
                break;
        }
    }

    private void performViewAllClick() {
        Intent intent = new Intent(mActivity, ViewAllReviewsActivity.class);
        intent.putExtra("business_id", business_id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void performRatingClick() {
        Intent intent = new Intent(mActivity, RatingActivity.class);
        intent.putExtra("business_id", business_id);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void performBookClick() {
        if(KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, false)){
            Log.e(TAG,"connn"+KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.IS_CONNECT, ""));
            if (KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.IS_CONNECT, "").equals("no")) {
                performGoogleLinkClick();
            } else {
                performRefreshClick();
            } }
        else {
            showLoginAlertDialog(mActivity,"Please Login First");
        }

    }

    private void performGoogleLinkClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            loginWithGoogle();
        }
    }

    private void loginWithGoogle() {
        showProgressDialog(mActivity);
        RequestBody userID = RequestBody.create(MediaType.parse("text/plain"), KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginGoogleLinkRequest(userID).enqueue(new Callback<GoogleLoginLinkModel>() {

            @Override
            public void onResponse(Call<GoogleLoginLinkModel> call, Response<GoogleLoginLinkModel> response) {
                dismissProgressDialog();
                GoogleLoginLinkModel mModel = response.body();
                assert response.body() != null;
                if (response.body().getStatus().equals(1)) {
                    KinnyConnectPreferences.writeBoolean(mActivity, KinnyConnectPreferences.IS_GOOGLE_LOGIN, true);
                    assert mModel != null;
                    String url = mModel.getLoginUrl();
                    Intent intent = new Intent(mActivity, GoogleWebViewActivity.class);
                    intent.putExtra(Constants.URL, url);
//                    intent.putExtra("connect",mModel.getConnect());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.IS_CONNECT, mModel.getConnect());
                    intent.putExtra("business_id", business_id);
                    startActivity(intent);
                } else {
                    dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<GoogleLoginLinkModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**Error**" + t.toString());
            }
        });
    }

    //    private void performBookClick() {
//        Intent intent = new Intent(mActivity, BookingActivity.class);
//        intent.putExtra("business_id",business_id);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);  }
    private void performRefreshClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            RefreshTheToken();
        }
    }

    /*
     * Execute google api
     * */
    private Map<String, String> mRefreshParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", Constants.CLIENT_ID_SERVER);
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        mMap.put("client_secret", Constants.CLIENT_SECRET_SERVER);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void RefreshTheToken() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.refreshTokenRequest(mRefreshParams()).enqueue(new retrofit2.Callback<RefreshTokenModel>() {
            @Override
            public void onResponse(Call<RefreshTokenModel> call, Response<RefreshTokenModel> response) {
                dismissProgressDialog();
//                Log.e(TAG, "**RESPONSE Refresh**" + response.body().toString());
                RefreshTokenModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals(1)) {
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.CALENDAR_DETAIL, mModel.getCalendarDetail());
                    Intent intent = new Intent(mActivity, BookingActivity.class);
                    intent.putExtra("business_id", business_id);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RefreshTokenModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void performContactClick() {
        if (KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, false)) {
            startActivity(new Intent(mActivity, ContactUsActivity.class));
        } else {
            showLoginAlertDialog(mActivity, "Please Login First");
        }

    }


    private void setServicesAdapter() {
        mServicesProvidedAdapter = new ServicesProvidedAdapter(mActivity, mServicesList);
        servicesProvidedRV.setLayoutManager(new LinearLayoutManager(mActivity));
        servicesProvidedRV.setAdapter(mServicesProvidedAdapter);
        mServicesProvidedAdapter.notifyDataSetChanged();
    }

    private void setShareExpAdapter() {
        mShareExpAdapter = new ShareExperienceAdapter(mActivity, mShareExperienceList);
        ShareExpRV.setLayoutManager(new LinearLayoutManager(mActivity));
        ShareExpRV.setAdapter(mShareExpAdapter);
        mShareExpAdapter.notifyDataSetChanged();

    }
}
