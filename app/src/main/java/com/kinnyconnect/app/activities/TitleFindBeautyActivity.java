package com.kinnyconnect.app.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsApi;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.BeautyHorizontalAdapter;
import com.kinnyconnect.app.adapters.FindBeautyDesAdapter;
import com.kinnyconnect.app.adapters.ServicesAdapter;
import com.kinnyconnect.app.adapters.ShopItemAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.ResetPasswordModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kinnyconnect.app.utils.Constants.REQUEST_PERMISSION_CODE;

public class TitleFindBeautyActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TitleFindBeautyActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = TitleFindBeautyActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtFindBeautyTV)
    TextView txtFindBeautyTV;
    @BindView(R.id.txtAddressTV)
    TextView txtAddressTV;
    @BindView(R.id.textTitleTV)
    TextView textTitleTV;
    int AUTOCOMPLETE_REQUEST_CODE = 5;
    String lat = "";
    String lng = "";
    String timeStamp = "";
    LatLng latLng = null;
    String addres;
    String cat_id = "";
    String title = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_find_beauty);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getIntentDAta();
        setDataOnViews();
        txtAddressTV.setText(KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.CURRENT_LOCATION_NEW, ""));
        lat = KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.CURR_LAT, "");
        lng = KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.CURR_LONG, "");

        timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        Log.e(TAG, "CURENT_TIMESTAMP" + timeStamp);

        /*
         * For places Location google api
         * */
        initializePlaceSdKLocation();
    }

    private void setDataOnViews() {
        textTitleTV.setText(title);
    }

    /*
     * Getting data
     * */
    private void getIntentDAta() {
        if (getIntent() != null) {
            if (getIntent().getExtras().get("category_id") != null) {
                cat_id = (String) getIntent().getExtras().get("category_id");
                title = (String) getIntent().getExtras().get("title");
            }
        }
    }

    @OnClick({R.id.txtFindBeautyTV, R.id.txtAddressTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtFindBeautyTV:
                performFindBeautyClick();
                break;
            case R.id.txtAddressTV:
                setSearchIntent();
                break;
        }
    }

    private void setSearchIntent() {
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(mActivity);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                txtAddressTV.setText(place.getName());
                latLng = place.getLatLng();
                addres = place.getAddress();

                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() + ", " + place.getLatLng() + ", " + place.getAddress());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void performFindBeautyClick() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            Intent intent = new Intent(mActivity, FindBeautyActivity.class);
            intent.putExtra("category_id", cat_id);
//            intent.putExtra("latitude", String.valueOf(latLng.latitude));
//            intent.putExtra("longitude", String.valueOf(latLng.latitude));
//            Log.e("msggg", String.valueOf(latLng.latitude));
//            Log.e("msggg",  String.valueOf(latLng.latitude));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }


    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
