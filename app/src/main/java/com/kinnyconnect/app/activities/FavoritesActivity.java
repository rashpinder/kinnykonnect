package com.kinnyconnect.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.CartAdapter;
import com.kinnyconnect.app.adapters.FavoriteAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.DeleteItemInterface;
import com.kinnyconnect.app.interfaces.IncDecClickInterface;
import com.kinnyconnect.app.model.AddToCartModel;
import com.kinnyconnect.app.model.CartModel;
import com.kinnyconnect.app.model.FavoriteModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritesActivity extends BaseActivity implements DeleteItemInterface {
    /**
     * Getting the Current Class Name
     */
    String TAG = FavoritesActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = FavoritesActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.favRV)
    RecyclerView favRV;
    String favID;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;

    List<FavoriteModel.Datum> mFavArrayList;
    FavoriteAdapter mFavAdapter;
    private DeleteItemInterface deleteItemInterface;
int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        deleteItemInterface = this;
        GetFavoriteList();
    }

    @OnClick({R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;

        }
    }

    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void GetFavoriteList() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeGetFavListApi();
        }
    }


    private void executeGetFavListApi() {
        if (mFavArrayList != null) {
            mFavArrayList.clear();
        }
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getFavListRequest(mParams()).enqueue(new Callback<FavoriteModel>() {
            @Override
            public void onResponse(Call<FavoriteModel> call, Response<FavoriteModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                FavoriteModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mFavArrayList = new ArrayList<>();
                    mFavArrayList = mModel.getData();
                    setFavAdapter();
                    txtNoDataFountTV.setVisibility(View.GONE);
                } else {
//                    showFAvAlertDialog(mActivity, mModel.getMessage());
                    txtNoDataFountTV.setVisibility(View.VISIBLE);
                    txtNoDataFountTV.setText(mModel.getMessage());
                    if (mFavArrayList!=null){
                    mFavAdapter.notifyDataSetChanged();}
                }
            }

            @Override
            public void onFailure(Call<FavoriteModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
    /*
     *
     * Error Alert Dialog
     * */
    public void showFAvAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }


    private void setFavAdapter() {
        mFavAdapter = new FavoriteAdapter(mActivity, (ArrayList<FavoriteModel.Datum>) mFavArrayList, deleteItemInterface);
        favRV.setLayoutManager(new LinearLayoutManager(mActivity));
        favRV.setAdapter(mFavAdapter);
    }

    @Override
    public void getData(String cartID,int pos) {
        this.favID = cartID;
        this.pos = pos;
        removeFavItem();
    }

    private void removeFavItem() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeRemoveFavItemApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mRemoveItemParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("wishID", favID);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeRemoveFavItemApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.removeFavRequest(mRemoveItemParams()).enqueue(new Callback<AddToCartModel>() {
            @Override
            public void onResponse(Call<AddToCartModel> call, Response<AddToCartModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                AddToCartModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
//                    GetFavoriteList();
                    mFavArrayList.remove(pos);
                    mFavAdapter.notifyDataSetChanged();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddToCartModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


}
