package com.kinnyconnect.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.ResetPasswordModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ResetPasswordActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ResetPasswordActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.editOldPasswordET)
    EditText editOldPasswordET;
    @BindView(R.id.editNewPasswordET)
    EditText editNewPasswordET;
    @BindView(R.id.editConfirmPasswordET)
    EditText editConfirmPasswordET;
    @BindView(R.id.passRL)
    RelativeLayout passRL;
    @BindView(R.id.newPassRL)
    RelativeLayout newPassRL;
    @BindView(R.id.confirmPassRL)
    RelativeLayout confirmPassRL;
    @BindView(R.id.txtSubmitTV)
    TextView txtSubmitTV;
    @BindView(R.id.mainRL)
    RelativeLayout mainRL;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        mainRL.setOnTouchListener((v, event) -> {
            hideKeyBoard(mActivity,getCurrentFocus());
            return true;
        });
    }

    @OnClick({R.id.txtSubmitTV, R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSubmitTV:
                    performSubmitClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;

        }
    }

    private void performBackClick() {
        onBackPressed();
//        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void performSubmitClick() {
        if (isValildate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                if(KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, false)){
                    executeChangePassApi();
                }
                else {
                    showLoginAlertDialog(mActivity,"You are not logged In");
                }
            }
        }
    }

    /*
  Execute api
   */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.ID,null));
        mMap.put("oldPassword", editOldPasswordET.getText().toString().trim());
        mMap.put("newPassword", editNewPasswordET.getText().toString().trim());
        mMap.put("confirmPassword", editConfirmPasswordET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeChangePassApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.changePassRequest(mParam()).enqueue(new Callback<ResetPasswordModel>() {
            @Override
            public void onResponse(Call<ResetPasswordModel> call, Response<ResetPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ResetPasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
    set up validations
     */
    public boolean isValildate() {
        boolean flag = true;
        if (editOldPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_old_password));
            flag = false;
        } else if (editNewPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_password));
            flag = false;
        } else if (editConfirmPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password));
            flag = false;
        } else if (!editNewPasswordET.getText().toString().trim().equals(editConfirmPasswordET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.password_mismatch));
            flag = false;
        } else if (editOldPasswordET.getText().toString().trim().length()<6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_6_digit_pass));
            flag = false;}
        else if (editNewPasswordET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_6_digit_pass));
            flag = false;
        } else if (editConfirmPasswordET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_6_digit_pass));
            flag = false;
        } else if (editOldPasswordET.getText().toString().trim().equals(editNewPasswordET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.new_pass_and_old_pasword_different));
            flag = false;
        }
        return flag;
    }

}
