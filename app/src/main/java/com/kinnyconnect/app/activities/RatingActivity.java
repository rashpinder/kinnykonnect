package com.kinnyconnect.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.fonts.TextViewRage;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.RatingModel;
import com.kinnyconnect.app.model.ReedemPointsModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RatingActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = RatingActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = RatingActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.ratingbarRB)
    RatingBar ratingbarRB;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.editReviewET)
    EditText editReviewET;
    @BindView(R.id.txtSubmitTV)
    TextView txtSubmitTV;
    @BindView(R.id.topLL)
    LinearLayout topLL;
    String rating;
    String business_id;
    private long mLastClickTime = 0;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getIntentData();
        topLL.setOnTouchListener((v, event) -> {
            hideKeyBoard(mActivity,getCurrentFocus());
            return true;
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("business_id") != null) {
                business_id = (String) getIntent().getExtras().get("business_id");
            }
        }}

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.ID,null));
        mMap.put("businessID", business_id);
        mMap.put("rating", rating);
        mMap.put("review", editReviewET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void submitRating() {
        if(isValidate()){
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeRatingApi();
        }}}



    private void executeRatingApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.submitReviewsRequest(mParams()).enqueue(new Callback<RatingModel>() {
            @Override
            public void onResponse(Call<RatingModel> call, Response<RatingModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                RatingModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity,mModel.getMessage());
                    finish();
                  } else  {
                    showRatingAlertDialog(mActivity, mModel.getMessage());
//                    showToast(mActivity,mModel.getMessage());

                }
            }

            @Override
            public void onFailure(Call<RatingModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
    /*
     *
     * Error Alert Dialog
     * */
    public void showRatingAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }



    @OnClick({R.id.imgBackIV,R.id.txtSubmitTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                break;
                case R.id.txtSubmitTV:
                performSubmitClick();
                break;
        }
    }

    private void performSubmitClick() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        rating=String.valueOf(ratingbarRB.getRating());
        submitRating();

    }

    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (rating.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_rating));
            flag = false;
        } else if (editReviewET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_review));
            flag = false;
        }else if (editReviewET.getText().toString().trim().length()>40) {
            showAlertDialog(mActivity, getString(R.string.please_add_valid_length_review));
            flag = false;
        }
        return flag;
    }

}
