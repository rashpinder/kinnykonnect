package com.kinnyconnect.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.GoogleLoginLinkModel;
import com.kinnyconnect.app.utils.Constants;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoogleWebViewActivity extends BaseActivity {
    /*
     * Initlaize Activity...
     * */
    Activity mActivity = GoogleWebViewActivity.this;
    /**
     * Getting the Current Class Name
     */
    String TAG = GoogleWebViewActivity.this.getClass().getSimpleName();

    /*Widgets*/
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.thankyouBt)
    TextView thankyouBt;
    String business_id;
    String url;
//    String connect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_web_view);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        if (getIntent() != null) {
            url = Objects.requireNonNull(getIntent().getExtras()).getString(Constants.URL);
//            connect = Objects.requireNonNull(getIntent().getExtras()).getString("connect");
            if (Objects.requireNonNull(getIntent().getExtras()).get("business_id") != null) {
                business_id = (String) getIntent().getExtras().get("business_id");
            }
        }
        WebSettings settings = webView.getSettings();
        settings.setUserAgentString("Kinny Konnect");
        settings.setJavaScriptEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();


        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click...");
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "Finished loading URL: " + url);

                if (url.contains("https://www.dharmani.com/kinnyKonnect/webservices/TokenGenerate.php")) {
                    performGoogleLinkClick();
                    thankyouBt.setVisibility(View.VISIBLE);
                    thankyouBt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if ((KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.IS_CONNECT,null)).equals("no")) {

                                Log.e(TAG,"conn"+(KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.IS_CONNECT,null)));
                                webView.clearCache(true);
                                CookieSyncManager.createInstance(mActivity);
                                CookieManager cookieManager = CookieManager.getInstance();
                                cookieManager.removeAllCookie();
                                finish();
                            } else {
                                Intent intent = new Intent(mActivity, BookingActivity.class);
                                intent.putExtra("business_id", business_id);
                                startActivity(intent);
                            }
                        }
                    });
                } else {
                    thankyouBt.setVisibility(View.GONE);
                }
            }


            private void performGoogleLinkClick() {
                if (!isNetworkAvailable(mActivity)) {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error));
                } else {
                    loginWithGoogle();
                }
            }

            private void loginWithGoogle() {
                showProgressDialog(mActivity);
                RequestBody userID = RequestBody.create(MediaType.parse("text/plain"), KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));

                ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
                mApiInterface.loginGoogleLinkRequest(userID).enqueue(new Callback<GoogleLoginLinkModel>() {

                    @Override
                    public void onResponse(Call<GoogleLoginLinkModel> call, Response<GoogleLoginLinkModel> response) {
                        dismissProgressDialog();
                        GoogleLoginLinkModel mModel = response.body();
                        assert response.body() != null;
                        if (response.body().getStatus().equals(1)) {
                            KinnyConnectPreferences.writeBoolean(mActivity, KinnyConnectPreferences.IS_GOOGLE_LOGIN, true);
                            KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.IS_CONNECT, mModel.getConnect());
                        } else {
                            dismissProgressDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<GoogleLoginLinkModel> call, Throwable t) {
                        dismissProgressDialog();
                        Log.e(TAG, "**Error**" + t.toString());
                    }
                });
            }
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "Error: " + description);
                Toast.makeText(mActivity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                alertDialog.setTitle("Error");
                alertDialog.setMessage(description);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                alertDialog.show();
            }
        });
        webView.loadUrl(url);
    }
}
