package com.kinnyconnect.app.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsApi;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.LoginModel;
import com.kinnyconnect.app.model.LogoutModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import com.kinnyconnect.app.utils.LoginPrefrences;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kinnyconnect.app.activities.DrawerActivity.imgLocIV;
import static com.kinnyconnect.app.utils.Constants.REQUEST_PERMISSION_CODE;

public class BusinessSignInActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult> {

    /************************
     *Fused Google Location
     **************/
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    public double mLatitude;
    public double mLongitude;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;
    private String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    private String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
//    CallbackManager mCallbackManager;
    /*************************************************************/

    /**
     * Getting the Current Class Name
     */
    String TAG = BusinessSignInActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = BusinessSignInActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.txtSignInTV)
    TextView txtSignInTV;
    @BindView(R.id.txtDontHaveAccountTV)
    TextView txtDontHaveAccountTV;
    @BindView(R.id.rememberMeRL)
    RelativeLayout rememberMeRL;
    @BindView(R.id.txtForgotPassTV)
    TextView txtForgotPassTV;
    @BindView(R.id.imgTermsIV)
    ImageView imgTermsIV;
    int count = 0;
    boolean rem = true;
    boolean checked = false;
    String strDeviceToken;
    boolean terms = true;
    private SharedPreferences loginBusinessPreferences;
    public Boolean saveBusinessLogin;
    SharedPreferences.Editor loginBusinessPrefsEditor;
    String imgType="";
    String termsChecked = "";
    int strRadioType = 0;
    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState);
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        if (checkPermission()) {
            checkLocationSettings();
        } else {
            requestPermission();
        }
        updateValuesFromBundle(savedInstanceState);

        setContentView(R.layout.activity_business_sign_in);
        ButterKnife.bind(this);

//        mCallbackManager = CallbackManager.Factory.create();
        setStatusBar(mActivity);
//        get device token
        getDeviceToken();
        //check if the remember option has been check before
//        boolean rememberMe = LoginPrefrences.readBoolean(mActivity, LoginPrefrences.REMEMBER_BUSINESS, false);
//
//        if (rememberMe == true) {
//            //get previously stored login details
//            String email = LoginPrefrences.readString(mActivity, LoginPrefrences.BUSINESS_EMAIL, null);
//            String pass = LoginPrefrences.readString(mActivity, LoginPrefrences.BUSINESS_PASSWORD, null);
//
//            if (email != null && pass != null) {
//                editEmailET.setText(email);
//                editPasswordET.setText(pass);
//                imgTermsIV.setImageResource(R.drawable.ic_check_radio);
//            }}
        loginBusinessPreferences = getSharedPreferences("loginBusinessPrefs", MODE_PRIVATE);
        loginBusinessPrefsEditor = loginBusinessPreferences.edit();

        saveBusinessLogin = loginBusinessPreferences.getBoolean("saveBusinessLogin", false);
        Log.e(TAG,"LOGIB::"+saveBusinessLogin);

        if (saveBusinessLogin == true) {
            Log.e(TAG, "SHAREDvALYES::" + loginBusinessPreferences.getString(KinnyConnectPreferences.BUSINNESS_EMAIL, "") + " AND " + loginBusinessPreferences.getString(KinnyConnectPreferences.BUSINNESS_PASSWORD, ""));
            editEmailET.setText(loginBusinessPreferences.getString(KinnyConnectPreferences.BUSINNESS_EMAIL, ""));
            editPasswordET.setText(loginBusinessPreferences.getString(KinnyConnectPreferences.BUSINNESS_PASSWORD, ""));
            imgTermsIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_radio));
        }
    }

    private void getDeviceToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strDeviceToken = task.getResult().getToken();
                        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.DEVICE_TOKEN, strDeviceToken);
                        Log.e(TAG, "**Push Token**" + strDeviceToken);
                    }
                });
    }


    @OnClick({R.id.txtSignInTV, R.id.txtDontHaveAccountTV, R.id.rememberMeRL,R.id.txtForgotPassTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSignInTV:
                performSignInClick();
                break;
            case R.id.txtDontHaveAccountTV:
                performSignUpClick();
                break;
            case R.id.rememberMeRL:
                hideKeyBoard(mActivity, getCurrentFocus());
                performRememberClick();
                break;
                case R.id.txtForgotPassTV:
                performForgotPasswordClick();
                break;
        }
    }

    private void performForgotPasswordClick() {
        Intent intent=new Intent(mActivity,ForgotPasswordActivity.class);
        intent.putExtra("value","2");
        startActivity(intent);
    }

    private void performRememberClick() {
//        if (rem) {
//            imgTermsIV.setImageResource(R.drawable.ic_check_radio);
//            rem = false;
//            checked = true;
//            saveLoginDetails();
//        } else {
//            imgTermsIV.setImageResource(R.drawable.ic_uncheck_radio);
//            rem = true;
//            checked = false;
//            removeLoginDetails();
//        }
//        return;
        if (strRadioType == 0) {
            imgTermsIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_radio));
            termsChecked = "1";
            loginBusinessPrefsEditor.putBoolean("saveBusinessLogin", true);
            loginBusinessPrefsEditor.putString(KinnyConnectPreferences.BUSINNESS_EMAIL, editEmailET.getText().toString().trim());
            loginBusinessPrefsEditor.putString(KinnyConnectPreferences.BUSINNESS_PASSWORD, editPasswordET.getText().toString().trim());
            Log.e(TAG, "AddedALYES::" + editEmailET.getText().toString().trim() + "AND:" + editPasswordET.getText().toString().trim());
            loginBusinessPrefsEditor.commit();
            imgType="checked";
            Log.e(TAG,"VALUE::"+imgType);
            strRadioType++;
        } else {
// saveBusinessLogin = loginBusinessPreferences.getBoolean("saveBusinessLogin", false);
            loginBusinessPrefsEditor.putBoolean("saveBusinessLogin", false);
            loginBusinessPrefsEditor.putString(KinnyConnectPreferences.BUSINNESS_EMAIL, "");
            loginBusinessPrefsEditor.putString(KinnyConnectPreferences.BUSINNESS_PASSWORD, "");
            imgTermsIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheck_radio));
// saveBusinessLogin = loginBusinessPreferences.getBoolean("saveBusinessLogin", false);
            termsChecked = "";
// loginBusinessPrefsEditor.clear();
            loginBusinessPrefsEditor.commit();
            saveBusinessLogin = loginBusinessPreferences.getBoolean("saveBusinessLogin", false);
            imgType="unchecked";
            Log.e(TAG,"VALUE::"+imgType);
// loginBusinessPrefsEditor.commit();
            strRadioType = 0;
        }
    }

//    private void removeLoginDetails() {
//        LoginPrefrences.writeBoolean(mActivity, LoginPrefrences.REMEMBER_BUSINESS, false);
//        LoginPrefrences.writeString(mActivity, LoginPrefrences.BUSINESS_EMAIL, null);
//        LoginPrefrences.writeString(mActivity, LoginPrefrences.BUSINESS_PASSWORD, null);
//    }

//    private void saveLoginDetails() {
//        String email = editEmailET.getText().toString();
//        String pass = editPasswordET.getText().toString();
//        LoginPrefrences.writeBoolean(mActivity, LoginPrefrences.REMEMBER_BUSINESS, true);
//        LoginPrefrences.writeString(mActivity, LoginPrefrences.BUSINESS_EMAIL, email);
//        LoginPrefrences.writeString(mActivity, LoginPrefrences.BUSINESS_PASSWORD, pass);
//
//    }


    /******************
     *Fursed Google Location
     ************/
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link LocationSettingsRequest.Builder} to build
     * a {@link LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * The callback invoked when
     * {@link SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i("", "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i("", "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
            default:
                // finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
//        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("", "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("", "User chose not to make required location settings changes.");
                        requestPermission();
                        break;
                    default:
                        // finish();
                }
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
        //  dismissProgressDialog();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
        mGoogleApiClient.disconnect();
        dismissProgressDialog();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i("", "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        mLatitude = mCurrentLocation.getLatitude();
        mLongitude = mCurrentLocation.getLongitude();
        Log.e(TAG, "*********LATITUDE********" + mLatitude);
        Log.e(TAG, "*********LONGITUDE********" + mLongitude);
//        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.CURRENT_LOCATION_LATITUDE, "" + mLatitude);
//        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.CURRENT_LOCATION_LONGITUDE, "" + mLongitude);

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i("", "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }
    /*****************************************/

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    public boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // requestPermission();
                }
                return;
            }

        }
    }


    private void performSignUpClick() {
        Intent intent = new Intent(mActivity, BusinessSignUpActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void performSignInClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                if(KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, false)){
                    logout(); }
                else {
                    executeSignInApi();
                }

            }
        }
    }

    private void logout() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeLogoutApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mlogoutParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(mlogoutParam()).enqueue(new Callback<LogoutModel>() {
            @Override
            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                LogoutModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    SharedPreferences preferences = KinnyConnectPreferences.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    mActivity.onBackPressed();
                    executeSignInApi();
//                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
//                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(mIntent);
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<LogoutModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());


            }
        });
    }



    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("role", "1");
        mMap.put("latitude","");
        mMap.put("longitude", "");
        mMap.put("deviceToken", strDeviceToken);
        mMap.put("deviceType", "2");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeSignInApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.loginRequest(mParam()).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                LoginModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    KinnyConnectPreferences.writeBoolean(mActivity, KinnyConnectPreferences.IS_BUSINNESS_LOGIN, true);
                    KinnyConnectPreferences.writeBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, true);
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.ID, mModel.getData().getUserID());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.EMAIL, mModel.getData().getEmail());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.NAME, mModel.getData().getName());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.ROLE, mModel.getData().getRole());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.PASSWORD, mModel.getData().getPassword());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.REFRESH_TOKEN, mModel.getData().getRefreshToken());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.GOOGLE_TOKEN, mModel.getData().getGoogleToken());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.FB_TOKEN, mModel.getData().getFacebookToken());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.VERIFICATION_CODE, mModel.getData().getVerificationCode());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.VERIFIED, mModel.getData().getVerified());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.IMAGE, response.body().getData().getProfileImage());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LAT, mModel.getData().getLat());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LONG, mModel.getData().getLog());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.DEVICE_TYPE, mModel.getData().getDeviceType());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.DEVICE_TOKEN, mModel.getData().getDeviceToken());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.PURCHASED_PLAN, mModel.getData().getPurchasedPlan());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.EXPIRE_DATE, response.body().getData().getExpireDate());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.CREATED, response.body().getData().getCreated());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.IS_CONNECT,mModel.getData().getConnect());
                    Log.e(TAG,"connect"+mModel.getData().getConnect());

//                    KinnyConnectPreferences.writeBoolean(mActivity,KinnyConnectPreferences.IS_BUSINNESS_LOGIN,true);
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINESS_ID, mModel.getData().getUserID());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_EMAIL, mModel.getData().getEmail());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_NAME, mModel.getData().getName());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_ROLE, mModel.getData().getRole());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_PASSWORD, mModel.getData().getPassword());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_GOOGLE_TOKEN, mModel.getData().getGoogleToken());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_FB_TOKEN, mModel.getData().getFacebookToken());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_VERIFICATION_CODE, mModel.getData().getVerificationCode());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_VERIFIED, mModel.getData().getVerified());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_IMAGE, response.body().getData().getProfileImage());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_LAT, mModel.getData().getLat());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_LONG, mModel.getData().getLog());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_DEVICE_TYPE, mModel.getData().getDeviceType());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_DEVICE_TOKEN, mModel.getData().getDeviceToken());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_PURCHASED_PLAN, mModel.getData().getPurchasedPlan());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_EXPIRE_DATE, response.body().getData().getExpireDate());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_CREATED, response.body().getData().getCreated());
//                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.FILL, response.body().getData().getFill());
//                 if (mModel.getData().getFill().equals("0")){
//                     Intent intent = new Intent(mActivity, TellUsAboutYourselfActivity.class);
//                     intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                     startActivity(intent);
//                     finish();
//                 }
//                 else if  (mModel.getData().getFill().equals("1")){
//                     Intent intent = new Intent(mActivity, TellUsAboutYourselfTwoActivity.class);
//                     intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                     startActivity(intent);
//                     finish();
//                 }  else if  (mModel.getData().getFill().equals("2")){
//                     Intent intent = new Intent(mActivity, TellUsAboutYourselfThreeActivity.class);
//                     intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                     startActivity(intent);
//                     finish();
//                 }else {
//                     KinnyConnectPreferences.writeBoolean(mActivity,KinnyConnectPreferences.IS_BUSINNESS_LOGIN,true);
//                     KinnyConnectPreferences.writeBoolean(mActivity,KinnyConnectPreferences.ISLOGIN,true);
                    if (saveBusinessLogin == true)
                    {
//                        Log.e(TAG, "SHAREDvALYES::" + loginBusinessPreferences.getString("Loginemail", "") + " AND " + loginBusinessPreferences.getString("Loginpassword", ""));
//loginBusinessPrefsEditor.putBoolean("saveBusinessLogin", true);
                        loginBusinessPrefsEditor.putString(KinnyConnectPreferences.BUSINNESS_EMAIL, editEmailET.getText().toString().trim());
                        loginBusinessPrefsEditor.putString(KinnyConnectPreferences.BUSINNESS_PASSWORD, editPasswordET.getText().toString().trim());
                        imgTermsIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_radio));
                        loginBusinessPrefsEditor.commit();
                    }

                     Intent intent = new Intent(mActivity, ManageBusinessAccountActivity.class);
                     intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                     startActivity(intent);
                    finish();
//                 }

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Set up validations for Sign In fields
     * */
    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
       if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }
        return flag;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }
}
