package com.kinnyconnect.app.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.TellYourselfModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TellUsAboutYourselfTwoActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TellUsAboutYourselfTwoActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = TellUsAboutYourselfTwoActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtContinueTV)
    TextView txtContinueTV;
    @BindView(R.id.imgBrandNameIV)
    ImageView imgBrandNameIV;
    @BindView(R.id.imgLicenceIV)
    ImageView imgLicenceIV;
    @BindView(R.id.rbYesRB)
    RadioButton rbYesRB;
    @BindView(R.id.rbNoRB)
    RadioButton rbNoRB;

    @BindView(R.id.rbShopRB)
    RadioButton rbShopRB;
    @BindView(R.id.rbHomeRB)
    RadioButton rbHomeRB;

    @BindView(R.id.editAddressET)
    EditText editAddressET;
    @BindView(R.id.editServicesET)
    EditText editServicesET;
    @BindView(R.id.editTravellingServicesET)
    EditText editTravellingServicesET;
    @BindView(R.id.editOfferingServicesET)
    EditText editOfferingServicesET;

    String mBase64Image="";
    String mBase64ImageTwo="";
    String img_type="";
    String licence_status="";
    String location="";
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tell_us_about_yourself_two);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
    }
    @OnClick({R.id.txtContinueTV,R.id.imgBrandNameIV,R.id.imgLicenceIV,R.id.rbYesRB,R.id.rbNoRB,R.id.rbHomeRB,R.id.rbShopRB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtContinueTV:
                performContinueClick();
                break;
                case R.id.imgBrandNameIV:
                performBrandClick();
                break;
                case R.id.imgLicenceIV:
                performLicenceClick();
                break;
            case R.id.rbYesRB:
                performYesClick();
                break;
            case R.id.rbNoRB:
                performNoClick();
                break;
                case R.id.rbHomeRB:
                    performHomeClick();
                break;
            case R.id.rbShopRB:
                performShopClick();
                break;

        }
    }

    private void performHomeClick() {
        rbHomeRB.setChecked(true);
        rbShopRB.setChecked(false);
        location="0";
    }

    private void performShopClick() {
        rbHomeRB.setChecked(false);
        rbShopRB.setChecked(true);
        location="1";
    }

    private void performNoClick() {
        rbYesRB.setChecked(false);
        rbNoRB.setChecked(true);
        licence_status="0";
    }

    private void performYesClick() {
        licence_status="1";
        rbNoRB.setChecked(false);
        rbYesRB.setChecked(true);
    }


    public void hideKeyboardFrom(Activity mActivity, View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    private void performLicenceClick() {
        if (checkPermission()) {
            img_type="second_type";
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private void performBrandClick() {
        if (checkPermission()) {
            img_type="first_type";
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
            CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {
                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), selectedImage, "Title", null);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
                    RequestOptions options = new RequestOptions()
                            .placeholder(R.drawable.ic_ph)
                            .error(R.drawable.ic_ph)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .skipMemoryCache(true)
                            .dontAnimate()
                            .dontTransform();

                    if (path != null) {
                        if (img_type.equals("first_type")){
                        Glide.with(mActivity).load(path)
                                .apply(options)
                                .into(imgBrandNameIV);
                            mBase64Image = encodeTobase64(selectedImage);
                    }
                    else {
                        Glide.with(mActivity).load(path)
                                .apply(options)
                                .into(imgLicenceIV);
                        mBase64ImageTwo = encodeTobase64(selectedImage);
                    }}
//                        imgEditProfileIV.setImageBitmap(selectedImage);


                    Log.e(TAG, "**Image Base 64**" + mBase64Image);
                    Log.e(TAG, "**Image Base 64**" + mBase64ImageTwo);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }
    }

    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 30, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }
    private void performContinueClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                createAccountApi();
            }
        }
//        Intent intent = new Intent(mActivity, TellUsAboutYourselfThreeActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//        finish();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("status", "2");
        mMap.put("brandImage",mBase64Image);
        mMap.put("licenseStatus",licence_status);
        mMap.put("licenseImage", mBase64ImageTwo);
        mMap.put("location", location);
        mMap.put("address", editAddressET.getText().toString().trim());
        mMap.put("servicesProvide", editServicesET.getText().toString().trim());
        mMap.put("offerTravelServices", editOfferingServicesET.getText().toString().trim());
        mMap.put("longTravelServices", editTravellingServicesET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void createAccountApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createAccountRequest(mParam()).enqueue(new Callback<TellYourselfModel>() {
            @Override
            public void onResponse(Call<TellYourselfModel> call, Response<TellYourselfModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TellYourselfModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity,mModel.getMessage(),Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mActivity, TellUsAboutYourselfThreeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TellYourselfModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (mBase64Image.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_upload_logo));
            flag = false;
        }  else if (mBase64ImageTwo.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_upload_licence));
            flag = false;
        }else if (editAddressET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_address));
            flag = false;
        }
        else if (editServicesET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_services));
            flag = false;
        }else if (editTravellingServicesET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add__travel_services));
            flag = false;
        } else if (editOfferingServicesET.getText().toString().trim().equals("")){
            showAlertDialog(mActivity, getString(R.string.please_add_how_long_travel_services));
            flag = false;
        }
        return flag;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
