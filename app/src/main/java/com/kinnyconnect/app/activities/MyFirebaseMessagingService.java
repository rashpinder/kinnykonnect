package com.kinnyconnect.app.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static int NOTIFICATION_ID = 1;
    String TAG = MyFirebaseMessagingService.this.getClass().getSimpleName();
    String user_id = "", notification_type = "", forumId="",title = "", order_id = "", request_id = "",
            notification_id = "", notification_read_status = "", expireDate = "", roomId = "", created = "", userName = "", message = "";
    Intent intent;
    String CHANNEL_ID = "my_channel_01";
    JSONObject data = null;

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.e("newToken", token);
        //Add your token in your sharepreferences.
        getSharedPreferences("_", MODE_PRIVATE).edit().putString("fcm_token", token).apply();
    }

    //Whenewer you need FCM token, just call this static method to get it.
    public static String getToken(Context context) {
        return context.getSharedPreferences("_", MODE_PRIVATE).getString("fcm_token", "empty");
    }
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

//        title = remoteMessage.getNotification().getTitle();
//        message = remoteMessage.getNotification().getBody();
        Log.e(TAG, "TYPE::" + notification_type+"and"+message);

        try {
            data = new JSONObject(remoteMessage.getData().get("data"));
            notification_type =  data.getString("notification_type");
            notification_id =  data.getString("notification_id");
            title =  data.getString("title");
            message =data.getString("message");
            notification_read_status =data.getString("notification_read_status");
            expireDate =data.getString("expireDate");
            created =data.getString("created");
            Log.e(TAG, "TYPE::" + notification_type+"and"+message);
          //  notification_type =  data.getString("push_type");

        } catch (JSONException e) {
            Log.e(TAG,""+e);
            e.printStackTrace();
        }
//        try {
//
////            room_id = data.getString("room_id");
////            receiver_id = data.getString("receiver_id");
////            userName = data.getString("username");
////                        forumId = data.getString("forum_id");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        sendNotification(title, message);

    }
//
//    push_type=>1->mass push
//2->broadcast message push
//3->invite push
//4->chat push


    private void sendNotification(String title, String message) {
        if (notification_type != null) {
            if (notification_type.equalsIgnoreCase("1")) {

                intent = new Intent(this, NotificationsActivity.class);


            }
            else if(notification_type.equalsIgnoreCase("2"))
            {
                intent = new Intent(this, DrawerActivity.class);
                KinnyConnectPreferences.writeInteger(this,KinnyConnectPreferences.TYPE_TWO,2);

            }

            else {
                intent = new Intent(this, SplashScreen.class);
            }
        } else {
            intent = new Intent(this, SplashScreen.class);
        }

//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_logo)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {

                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        }
    }
}
