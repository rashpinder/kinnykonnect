package com.kinnyconnect.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.activities.BaseActivity;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.ForgotPasswordModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ForgotPasswordActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ForgotPasswordActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.txtSubmitTV)
    TextView txtSubmitTV;
    @BindView(R.id.mainRL)
    RelativeLayout mainRL;
    String value;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setStatusBar(mActivity);
        ButterKnife.bind(this);
        showEmail();
        getIntentData();
        mainRL.setOnTouchListener((v, event) -> {
            hideKeyBoard(mActivity,getCurrentFocus());
            return true;
        });
    }


    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("value") != null) {
                value = (String) getIntent().getExtras().get("value");
            }
        }
    }

    private void showEmail() {
//        editEmailET.setText(KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.EMAIL,null));
    }

    @OnClick({R.id.txtSubmitTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSubmitTV:
                performSubmitClick();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (value.equals("1")){
            startActivity(new Intent(mActivity, LoginActivity.class));
            finish();
        }
        else {
            startActivity(new Intent(mActivity, BusinessSignInActivity.class));
            finish();
        }

    }


    private void performSubmitClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeForgotPassApi();
            }
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", editEmailET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeForgotPassApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.forgotPasswordRequest(mParam()).enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ForgotPasswordModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showLoginAlertDialog(mActivity,mModel.getMessage());
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        }
        return flag;
    }

}