package com.kinnyconnect.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.OffersAdapter;
import com.kinnyconnect.app.adapters.ServicesAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.AddToCartModel;
import com.kinnyconnect.app.model.ManageBusinessModel;
import com.kinnyconnect.app.model.SingleProductModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopItemDescriptionActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ShopItemDescriptionActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ShopItemDescriptionActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.imgDecIV)
    ImageView imgDecIV;
    @BindView(R.id.offersRV)
    RecyclerView offersRV;
    @BindView(R.id.imgShopItemIV)
    ImageView imgShopItemIV;
    @BindView(R.id.imgIncIV)
    ImageView imgIncIV;
    @BindView(R.id.txtItemCountTV)
    TextView txtItemCountTV;
    @BindView(R.id.txtItemTV)
    TextView txtItemTV;
    @BindView(R.id.txtItemPriceTV)
    TextView txtItemPriceTV;
    @BindView(R.id.txtAddToCartTV)
    TextView txtAddToCartTV;
    @BindView(R.id.favLL)
    LinearLayout favLL;
    @BindView(R.id.imgFavIV)
    ImageView imgFavIV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;
    String product_id;
    OffersAdapter mOffersAdapter;
    List<SingleProductModel.Data.Offer> mOffersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_item_description);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getIntentData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getShopItemDescription();
    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("product_id") != null) {
                product_id = (String) getIntent().getExtras().get("product_id");
            }
        }
    }

    private void getShopItemDescription() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executegetSingleProductApi();
        }

    }

    /*
        Execute api
         */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("productID", product_id);
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.ID,null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executegetSingleProductApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getSingleProductItemsRequest(mParam()).enqueue(new Callback<SingleProductModel>() {
            @Override
            public void onResponse(Call<SingleProductModel> call, Response<SingleProductModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                SingleProductModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mOffersList = new ArrayList<>();
                    mOffersList = mModel.getData().getOffer();
                    if (mModel.getData().getImage() != null) {
                        String imageurl = mModel.getData().getImage();
                        RequestOptions options = new RequestOptions()
                                .placeholder(R.drawable.ic_ph)
                                .error(R.drawable.ic_ph)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH)
                                .dontAnimate()
                                .dontTransform();

                        Glide.with(mActivity)
                                .load(imageurl)
                                .apply(options)
                                .into(imgShopItemIV);
                    }

                    txtItemPriceTV.setText(mModel.getData().getPrice());
                    txtItemTV.setText(mModel.getData().getTitle());
                    if (mModel.getData().getFav().equals("1")){
                        imgFavIV.setImageResource(R.drawable.ic_fav_active);
                    }
                    else {
                        imgFavIV.setImageResource(R.drawable.ic_fav_inactive);
                    }
                    if (mOffersList.size()==0){
                        txtNoDataFountTV.setVisibility(View.VISIBLE);
                   }
                    else {
                        setOffersAdapter();
                        txtNoDataFountTV.setVisibility(View.GONE);
                    }
                } else {
//                    showAlertDialog(mActivity, mModel.getMessage());
                    showToast(mActivity,mModel.getMessage());
                }

            }

            private void setOffersAdapter() {
                mOffersAdapter = new OffersAdapter(mActivity, (ArrayList<SingleProductModel.Data.Offer>) mOffersList);
                offersRV.setLayoutManager(new LinearLayoutManager(mActivity));
                offersRV.setAdapter(mOffersAdapter);
            }

            @Override
            public void onFailure(Call<SingleProductModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @OnClick({R.id.imgDecIV, R.id.imgIncIV, R.id.txtAddToCartTV, R.id.favLL,R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgDecIV:
                performDecreaseCountClick();
                break;
            case R.id.imgIncIV:
                performIncreaseCountClick();
                break;
                case R.id.imgBackIV:
                performBackClick();
                break;
            case R.id.txtAddToCartTV:
                if((KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.ID,null))!=null){
                performAddToCartClick();}
                else {
                    showToast(mActivity,"Please Login First");
                }
                break;
            case R.id.favLL:
                if((KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.ID,null))!=null){
                    performFavoriteClick();}
                else {
                    showToast(mActivity,"Please Login First");
                }
                break;

        }
    }

    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mFavParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        mMap.put("productID", product_id);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performFavoriteClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeAddToFavApi();
        }
    }


    private void executeAddToFavApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addToWishlistRequest(mFavParams()).enqueue(new Callback<AddToCartModel>() {
            @Override
            public void onResponse(Call<AddToCartModel> call, Response<AddToCartModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                AddToCartModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    imgFavIV.setImageResource(R.drawable.ic_fav_active);
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddToCartModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        mMap.put("productID", product_id);
        mMap.put("quantity", txtItemCountTV.getText().toString().trim());
        mMap.put("price", txtItemPriceTV.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performAddToCartClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeAddToCartApi();
        }
    }


    private void executeAddToCartApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addToCartRequest(mParams()).enqueue(new Callback<AddToCartModel>() {
            @Override
            public void onResponse(Call<AddToCartModel> call, Response<AddToCartModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                AddToCartModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    Intent intent = new Intent(mActivity, CartActivity.class);
                    intent.putExtra("value","shop_item");
                    startActivity(intent);

//                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddToCartModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void performDecreaseCountClick() {
        String count = txtItemCountTV.getText().toString();
        int no = Integer.parseInt(count) - 1;
        if (no >= 1) {
            txtItemCountTV.setText(no + "");
        } else {
            showToast(mActivity, "Quantity cannot be less than 1");
        }
    }

    private void performIncreaseCountClick() {
        String count = txtItemCountTV.getText().toString();
        int no = Integer.parseInt(count) + 1;

        txtItemCountTV.setText(no + "");
    }
}
