package com.kinnyconnect.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsApi;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.SignUpModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kinnyconnect.app.utils.Constants.REQUEST_PERMISSION_CODE;

public class BusinessSignUpActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult> {

    /************************
     *Fused Google Location
     **************/
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    public double mLatitude;
    public double mLongitude;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;
    private String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    private String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
//    CallbackManager mCallbackManager;
    /*************************************************************/

    /**
     * Getting the Current Class Name
     */
    String TAG = BusinessSignUpActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = BusinessSignUpActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editNameET)
    EditText editNameET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.txtSignUpTV)
    TextView txtSignUpTV;
    @BindView(R.id.txtAlreadyHaveAccountTV)
    TextView txtAlreadyHaveAccountTV;
    @BindView(R.id.termsRL)
    RelativeLayout termsRL;
    @BindView(R.id.imgTermsIV)
    ImageView imgTermsIV;
    @BindView(R.id.mainRL)
    RelativeLayout mainRL;
    int count = 0;
    boolean rem = true;
    boolean checked = false;
    String strDeviceToken;
    boolean terms = true;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState);
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        if (checkPermission()) {
            checkLocationSettings();
        } else {
            requestPermission();
        }
        updateValuesFromBundle(savedInstanceState);

        setContentView(R.layout.activity_business_sign_up);
        ButterKnife.bind(this);
//        mCallbackManager = CallbackManager.Factory.create();
        setStatusBar(mActivity);
//        get device token
        getDeviceToken();
        mainRL.setOnTouchListener((v, event) -> {
            hideKeyBoard(mActivity,getCurrentFocus());
            return true;
        });

    }

    private void getDeviceToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "**Get Instance Failed**", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        strDeviceToken = task.getResult().getToken();
                        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.DEVICE_TOKEN, strDeviceToken);
                        Log.e(TAG, "**Push Token**" + strDeviceToken);
                    }
                });
    }


    @OnClick({R.id.txtSignUpTV, R.id.txtAlreadyHaveAccountTV, R.id.termsRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSignUpTV:
                performSignUpClick();
                break;
            case R.id.txtAlreadyHaveAccountTV:
                performSignInClick();
                break;
            case R.id.termsRL:
                hideKeyBoard(mActivity, getCurrentFocus());
                performTermsClick();
                break;
        }
    }


    private void performTermsClick() {
        if (terms) {
            imgTermsIV.setImageResource(R.drawable.ic_check_radio);
            terms = false;
            checked = true;

        } else {
            imgTermsIV.setImageResource(R.drawable.ic_uncheck_radio);
            terms = true;
            checked = false;
        }
        return;
    }


    /******************
     *Fursed Google Location
     ************/
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link LocationSettingsRequest.Builder} to build
     * a {@link LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * The callback invoked when
     * {@link SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i("", "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i("", "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
            default:
                // finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
//        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("", "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("", "User chose not to make required location settings changes.");
                        requestPermission();
                        break;
                    default:
                        // finish();
                }
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
        //  dismissProgressDialog();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
        mGoogleApiClient.disconnect();
        dismissProgressDialog();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i("", "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        mLatitude = mCurrentLocation.getLatitude();
        mLongitude = mCurrentLocation.getLongitude();
        Log.e(TAG, "*********LATITUDE********" + mLatitude);
        Log.e(TAG, "*********LONGITUDE********" + mLongitude);
//        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.CURRENT_LOCATION_LATITUDE, "" + mLatitude);
//        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.CURRENT_LOCATION_LONGITUDE, "" + mLongitude);

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i("", "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }
    /*****************************************/

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    public boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // requestPermission();
                }
                return;
            }

        }
    }


    private void performSignInClick() {
        Intent intent = new Intent(mActivity, BusinessSignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void performSignUpClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeSignUpApi();
            }
        }
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("name", editNameET.getText().toString().trim());
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("latitude", String.valueOf(mLatitude));
        mMap.put("longitude", String.valueOf(mLongitude));
        mMap.put("deviceType", "2");
        mMap.put("deviceToken", strDeviceToken);
        mMap.put("role", "1");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeSignUpApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signUpRequest(mParam()).enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignUpModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
//                    KinnyConnectPreferences.writeBoolean(mActivity,KinnyConnectPreferences.IS_BUSINNESS_LOGIN,true);
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINESS_ID, mModel.getData().getUserID());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_EMAIL, mModel.getData().getEmail());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_NAME, mModel.getData().getName());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_ROLE, mModel.getData().getRole());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_PASSWORD, mModel.getData().getPassword());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_GOOGLE_TOKEN, mModel.getData().getGoogleToken());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_FB_TOKEN, mModel.getData().getFacebookToken());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_VERIFICATION_CODE, mModel.getData().getVerificationCode());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_VERIFIED, mModel.getData().getVerified());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_IMAGE, response.body().getData().getProfileImage());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_LAT, mModel.getData().getLat());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_LONG, mModel.getData().getLog());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_DEVICE_TYPE, mModel.getData().getDeviceType());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_DEVICE_TOKEN, mModel.getData().getDeviceToken());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_PURCHASED_PLAN, mModel.getData().getPurchasedPlan());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_EXPIRE_DATE, response.body().getData().getExpireDate());
                    KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.BUSINNESS_CREATED, response.body().getData().getCreated());
                    Intent intent = new Intent(mActivity, BusinessSignInActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Set up validations for Sign In fields
     * */
    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (editNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_name));
            flag = false;
        } else if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().length() < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_min_6_digit_pass));
            flag = false;
        } else if (!checked) {
            showAlertDialog(mActivity, getString(R.string.please_agree_privacy));
            flag = false;
        }
        return flag;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }
}
