package com.kinnyconnect.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.ForgotPasswordModel;
import com.kinnyconnect.app.model.TellYourselfModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TellUsAboutYourselfActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TellUsAboutYourselfActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = TellUsAboutYourselfActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtContinueTV)
    TextView txtContinueTV;
    @BindView(R.id.txtPhoneTV)
    EditText txtPhoneTV;
    @BindView(R.id.txtEmailTV)
    EditText txtEmailTV;
    @BindView(R.id.txtInstaTV)
    EditText txtInstaTV;
    @BindView(R.id.txtFbTV)
    EditText txtFbTV;
    @BindView(R.id.txtTwitterTV)
    EditText txtTwitterTV;
    @BindView(R.id.editOwnerNameET)
    EditText editOwnerNameET;
    @BindView(R.id.editDobET)
    TextView editDobET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tell_us_about_yourself);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
    }

    @OnClick({R.id.txtContinueTV, R.id.dobRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtContinueTV:
                performContinueClick();
                break;
            case R.id.dobRL:
                performDOBClick();
                break;

        }
    }

    private void performDOBClick() {
        showValidDatePickerDialog(mActivity, editDobET);
    }

    private void showValidDatePickerDialog(Activity mActivity, TextView editValidDateET) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        editValidDateET.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void performContinueClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                createAccountApi();
            }
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("status", "1");
        mMap.put("businessID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.BUSINESS_ID, null));
        mMap.put("ownerName", editOwnerNameET.getText().toString().trim());
        mMap.put("DOB", editDobET.getText().toString().trim());
        mMap.put("phoneNo", txtPhoneTV.getText().toString().trim());
        mMap.put("email", txtEmailTV.getText().toString().trim());
        mMap.put("instagram", txtInstaTV.getText().toString().trim());
        mMap.put("facebook", txtFbTV.getText().toString().trim());
        mMap.put("twitter", txtTwitterTV.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void createAccountApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createAccountRequest(mParam()).enqueue(new Callback<TellYourselfModel>() {
            @Override
            public void onResponse(Call<TellYourselfModel> call, Response<TellYourselfModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TellYourselfModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mActivity, TellUsAboutYourselfTwoActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TellYourselfModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (editOwnerNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_owner_name));
            flag = false;
        } else if (editDobET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_dob));
            flag = false;
        } else if (txtPhoneTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone));
            flag = false;
        } else if ((txtPhoneTV.getText().toString().trim().length() < 10)) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_contact));
            flag = false;
        } else if (txtEmailTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address));
            flag = false;
        } else if (!isValidEmaillId(txtEmailTV.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address));
            flag = false;
        } else if (txtInstaTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_insta));
            flag = false;
        } else if (txtFbTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_fb));
            flag = false;
        } else if (txtTwitterTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_tw));
            flag = false;
        }
        return flag;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
