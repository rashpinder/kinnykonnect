package com.kinnyconnect.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.adapters.FavoriteAdapter;
import com.kinnyconnect.app.adapters.FindBeautyAdapter;
import com.kinnyconnect.app.adapters.FindBeautyDesAdapter;
import com.kinnyconnect.app.adapters.ViewAllReviewsAdapter;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.interfaces.FindBeautyInterface;
import com.kinnyconnect.app.model.CategoryListingModel;
import com.kinnyconnect.app.model.FavoriteModel;
import com.kinnyconnect.app.model.FindBeautyModel;
import com.kinnyconnect.app.model.ViewAllReviewsModel;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllReviewsActivity extends BaseActivity  implements ViewAllReviewsAdapter.PaginationInterface{
    /**
     * Getting the Current Class Name
     */
    String TAG = ViewAllReviewsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ViewAllReviewsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.ReviewRV)
    RecyclerView ReviewRV;
    ViewAllReviewsAdapter mReviewsAdapter;
    List<ViewAllReviewsModel.Datum> mReviewsList;
    int count = 0;
    private int page_no = 1;
    private String strLastPage = "FALSE";
    private List<ViewAllReviewsModel.Datum> tempArrayList = new ArrayList<>();
    ViewAllReviewsAdapter.PaginationInterface paginationInterface;
    String business_id;
    @BindView(R.id.txtNoDataFountTV)
    TextView txtNoDataFountTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_reviews);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getIntentData();
        paginationInterface =this;
        if (tempArrayList != null) {
            tempArrayList.clear();
        }
        if (mReviewsList != null) {
            mReviewsList.clear();
        }
        page_no=1;
        getReviewsList();

    }

    private void getIntentData() {
        if (getIntent() != null) {
            if (Objects.requireNonNull(getIntent().getExtras()).get("business_id") != null) {
                business_id = (String) getIntent().getExtras().get("business_id");
            }
        }
    }
    /*
  Execute api
   */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("businessID", business_id);
        mMap.put("pageNo",String.valueOf(page_no));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void getReviewsList() {
        if (!isNetworkAvailable(mActivity)) {
            showToast(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeReviewsListApi();
        }
    }

    private void executeReviewsListApi() {
        if (page_no == 1) {
            showProgressDialog(Objects.requireNonNull(mActivity));
        } else if (page_no > 1) {
            dismissProgressDialog();
//                mProgressBar.setVisibility(View.GONE);
        }
//        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getReviewsRequest(mParam()).enqueue(new Callback<ViewAllReviewsModel>() {
            @Override
            public void onResponse(Call<ViewAllReviewsModel> call, Response<ViewAllReviewsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ViewAllReviewsModel mModel = response.body();

                mReviewsList=new ArrayList<>();
                mReviewsList=mModel.getData();
                strLastPage=mModel.getLastPage();
                if (page_no == 1) {
                    dismissProgressDialog();
                }
//                if (page_no > 1) {
//                    assert mModel != null;
//                    if (mModel.getData() == null) {
//                        strLastPage = "TRUE";
//                    } else if (mModel.getData().size() < 10) {
////                        Toast.makeText(mActivity(), "No more items!!!", Toast.LENGTH_SHORT).show();
//                        strLastPage = "TRUE";
//                    } else {
//                        strLastPage = "FALSE";
//                    }
//                }


                assert mModel != null;
                if (mModel.getStatus().equals("1")) {

                    if (page_no == 1) {
                        mReviewsList = mModel.getData();
                    } else if (page_no > 1) {
                        tempArrayList = mModel.getData();
                    }
                    if (tempArrayList != null) {
                        if (tempArrayList.size() > 0) {
                            mReviewsList.addAll(tempArrayList);
                        }
                    }

                    if (page_no == 1) {
                        setReviewsAdapter();
                        txtNoDataFountTV.setVisibility(View.GONE);
                    } else {
                        txtNoDataFountTV.setVisibility(View.VISIBLE);
                        txtNoDataFountTV.setText(mModel.getMessage());
                        mReviewsAdapter.notifyDataSetChanged();
                    }
                } else {
//                    showAlertDialog(Objects.requireNonNull(mActivity), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ViewAllReviewsModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void mPaginationInterface(boolean isLastScrolled) {
        if (isLastScrolled == true) {
            if (strLastPage.equals("FALSE")){
                showProgressDialog(Objects.requireNonNull(mActivity));
//            mProgressBar.setVisibility(View.VISIBLE);
                page_no++;}
            else {
                dismissProgressDialog();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (strLastPage.equals("FALSE")) {
                        executeReviewsListApi();
                    } else {
                        if(progressDialog.isShowing()){
                            dismissProgressDialog();}
//                        mProgressBar.setVisibility(View.GONE);
                    }
                }
            }, 1000);
        }
    }


    private void setReviewsAdapter() {
        mReviewsAdapter = new ViewAllReviewsAdapter(mActivity, (ArrayList<ViewAllReviewsModel.Datum>) mReviewsList, paginationInterface);
        ReviewRV.setLayoutManager(new LinearLayoutManager(mActivity));
          ReviewRV.setAdapter(mReviewsAdapter);
            mReviewsAdapter.notifyDataSetChanged();}
//        }

    @OnClick({R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackIV:
                performBackClick();
                finish();
                break;

        }
    }



    private void performBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
