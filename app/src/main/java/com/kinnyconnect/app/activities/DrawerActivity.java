package com.kinnyconnect.app.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsApi;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.FirebaseApp;
import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.fragments.AccountFragment;
import com.kinnyconnect.app.fragments.BusinessLoginFragment;
import com.kinnyconnect.app.fragments.DealsFragment;
import com.kinnyconnect.app.fragments.FindBeautyFragment;
import com.kinnyconnect.app.fragments.HomeeFragment;
import com.kinnyconnect.app.fragments.JoinCommunityFragment;
import com.kinnyconnect.app.fragments.SettingsFragment;
import com.kinnyconnect.app.fragments.ShopFragment;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.LogoutModel;
import com.kinnyconnect.app.utils.Constants;
import com.kinnyconnect.app.utils.EasyLocationProvider;
import com.kinnyconnect.app.utils.KinnyConnectPreferences;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kinnyconnect.app.utils.Constants.REQUEST_PERMISSION_CODE;

public class DrawerActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult> {

    /************************
     *Fused Google Location
     **************/
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    public double mLatitude;
    public double mLongitude;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;
    private final String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    private final String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    /**
     * Getting the Current Class Name
     */
    String TAG = DrawerActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = DrawerActivity.this;

    /*
     * Widgets
     * */
    public static TextView editAddressET;
//    @BindView(R.id.imgLocIV)
    public static  ImageView imgLocIV;
    @BindView(R.id.txtFindBeautyTV)
    TextView txtFindBeautyTV;
    @BindView(R.id.txtLoginTV)
    TextView txtLoginTV;
    @BindView(R.id.txtSignUpTV)
    TextView txtSignUpTV;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.imgMenuIV)
    ImageView imgMenuIV;
    @BindView(R.id.navBar)
    NavigationView navBar;
    @BindView(R.id.content)
    RelativeLayout content;
    @BindView(R.id.topLL)
    LinearLayout topLL;
    @BindView(R.id.imgCurrentLocIV)
    ImageView imgCurrentLocIV;
    public static TextView txtLocationTV;

    @BindView(R.id.frameL)
    FrameLayout frameL;
    LinearLayout homeLL;
    ImageView imgHomeIV;
    TextView txtHomeTV;
    LinearLayout findBeautyLL;
    ImageView imgFindBeautyIV;
    TextView txtfindBeautyTV;
    LinearLayout businessLL;
    ImageView imgBusinessIV;
    TextView txtLoginBusinessTV;
    LinearLayout joinCommLL;
    ImageView imgJoinComm;
    TextView txtjoinCommTV;
    LinearLayout shopLL;
    ImageView imgShopIV;
    TextView txtshopTV;
    LinearLayout dealsLL;
    ImageView imgDealsIV;
    TextView txtDealsTV;
    LinearLayout settingsLL;
    ImageView imgSettingsIV;
    TextView txtSettingsTV;
    LinearLayout myAccountLL;
    ImageView imgAccountIV;
    TextView txtMyAccountTV;
    LinearLayout logoutLL;
    ImageView imgLogoutIV;
    TextView txtLogoutTV;
    LinearLayout loginLL;
    ImageView imgUserLoginIV;
    TextView txtUserLoginTV;
    String address;
    int AUTOCOMPLETE_REQUEST_CODE = 5;
    double lat;
    double lng;
    String timeStamp = "";
    LatLng latLng = null;
    String addres;
    private final long mLastClickTime = 0;
    public static EasyLocationProvider easyLocationProvider;
    public static int i = 1;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*************
         *Fused Location
         **************/
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        /*Update values using data stored in the Bundle.*/
        updateValuesFromBundle(savedInstanceState);
        buildGoogleApiClient();
        FirebaseApp.initializeApp(this);
        createLocationRequest();
        buildLocationSettingsRequest();
        if (checkPermission()) {
            checkLocationSettings();
        } else {
            requestPermission();
        }
        updateValuesFromBundle(savedInstanceState);
        /*******************************************/

        setContentView(R.layout.activity_drawer);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        imgLocIV=findViewById(R.id.imgLocIV);
        editAddressET = findViewById(R.id.editAddressET);
        txtLocationTV = findViewById(R.id.txtLocationTV);
        KinnyConnectPreferences.writeInteger(mActivity, "value", i);
//if (KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.SELECTED_LOCATION,"")!=null){
        editAddressET.setText(KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.SELECTED_LOCATION, ""));
        txtLocationTV.setText(KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.SELECTED_LOCATION, ""));

        //
//}
//else {
//    getCurrentLATLong();
//}


//        lat = KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LAT_NEW, "");
//        lng = KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LNG_NEW, "");

        /*
         * For places Location google api
         * */
        initializePlaceSdKLocation();
        imgLocIV.setVisibility(View.GONE);
        txtLocationTV.setVisibility(View.GONE);

        if ((KinnyConnectPreferences.readInteger(mActivity, KinnyConnectPreferences.TYPE_TWO, 0)) == 2) {
            switchFragment(new DealsFragment(), Constants.DEALS_FRAGMENT, false, null);
            KinnyConnectPreferences.writeInteger(mActivity, KinnyConnectPreferences.TYPE_TWO, 0);
        }
        setNavigationViewListener();
        if (KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.IS_BUSINNESS_LOGIN, false)) {
            businessLL.setVisibility(View.GONE);
        } else {
            businessLL.setVisibility(View.VISIBLE);
        }
        if ((KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, false))) {
//                    showAlertDialog(mActivity, "You are already logged In");
            txtLoginTV.setVisibility(View.GONE);
            txtSignUpTV.setVisibility(View.GONE);
            logoutLL.setVisibility(View.VISIBLE);
            loginLL.setVisibility(View.GONE);
        } else {
            logoutLL.setVisibility(View.GONE);
            txtLoginTV.setVisibility(View.VISIBLE);
            txtSignUpTV.setVisibility(View.VISIBLE);
            loginLL.setVisibility(View.VISIBLE);
        }

//getIntentData();
    }


//    public void getCurrentLATLong() {
////        hh = getUserId();
////        Log.e(TAG, "USERid::" + hh);
//        easyLocationProvider = new EasyLocationProvider.Builder(mActivity)
//                .setInterval(300000)
//                .setFastestInterval(300000)
////                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
//                .setListener(new EasyLocationProvider.EasyLocationCallback() {
//                    @Override
//                    public void onGoogleAPIClient(GoogleApiClient googleApiClient, String message) {
//                        Log.e("EasyLocationProvider", "onGoogleAPIClient: " + message);
//
//                    }
//
//                    @Override
//                    public void onLocationUpdated(double latitude, double longitude) {
//                        if (i==0){
//                            lat = latitude;
//                            lng = longitude;
//                            Log.e(TAG, "OnGet::" + lat + " " + lng);
//                            try {
//                                Geocoder geocoder;
//                                List<Address> addresses;
//                                geocoder = new Geocoder(mActivity, Locale.getDefault());
//                                addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//
//                                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.SELECTED_LOCATION, address);
//                                String city = addresses.get(0).getLocality();
//                                String state = addresses.get(0).getAdminArea();
//                                String country = addresses.get(0).getCountryName();
//                                String postalCode = addresses.get(0).getPostalCode();
//                                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
//                                editAddressET.setText(KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.SELECTED_LOCATION,null));
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            i++;
//                        }
//                        else {
//                            Toast.makeText(mActivity,"errrrr",Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//
//                    @Override
//                    public void onLocationUpdateRemoved() {
//                        Log.e("EasyLocationProvider", "onLocationUpdateRemoved");
//                    }
//                }).build();
//
//        getLifecycle().addObserver(easyLocationProvider);
//    }
//    private void getIntentData() {
//        if (getIntent() != null) {
//            if ((Objects.requireNonNull(getIntent().getExtras()).get("deals")!=null)) {
//            if (Objects.requireNonNull(getIntent().getExtras()).get("deals").equals("2")) {
//                deals = (String) getIntent().getExtras().get("deals");
//            }
//            else {
//
//            }
//        }
//        switchFragment(new DealsFragment(), Constants.DEALS_FRAGMENT, false, null);
//        deals="";
//    }}

    private void setNavigationViewListener() {
        View header = navBar.getHeaderView(0);
        homeLL = header.findViewById(R.id.homeLL);
        imgHomeIV = header.findViewById(R.id.imgHomeIV);
        txtHomeTV = header.findViewById(R.id.txtHomeTV);
        findBeautyLL = header.findViewById(R.id.findBeautyLL);
        imgFindBeautyIV = header.findViewById(R.id.imgFindBeautyIV);
        txtfindBeautyTV = header.findViewById(R.id.txtfindBeautyTV);
        joinCommLL = header.findViewById(R.id.joinCommLL);
        imgJoinComm = header.findViewById(R.id.imgJoinComm);
        txtjoinCommTV = header.findViewById(R.id.txtjoinCommTV);
        shopLL = header.findViewById(R.id.shopLL);
        imgShopIV = header.findViewById(R.id.imgShopIV);
        txtshopTV = header.findViewById(R.id.txtshopTV);
        dealsLL = header.findViewById(R.id.dealsLL);
        imgDealsIV = header.findViewById(R.id.imgDealsIV);
        txtDealsTV = header.findViewById(R.id.txtDealsTV);
        settingsLL = header.findViewById(R.id.settingsLL);
        imgSettingsIV = header.findViewById(R.id.imgSettingsIV);
        txtSettingsTV = header.findViewById(R.id.txtSettingsTV);
        myAccountLL = header.findViewById(R.id.myAccountLL);
        imgAccountIV = header.findViewById(R.id.imgAccountIV);
        txtMyAccountTV = header.findViewById(R.id.txtMyAccountTV);
        logoutLL = header.findViewById(R.id.logoutLL);
        imgLogoutIV = header.findViewById(R.id.imgLogoutIV);
        txtLogoutTV = header.findViewById(R.id.txtLogoutTV);
        businessLL = header.findViewById(R.id.businessLL);
        imgBusinessIV = header.findViewById(R.id.imgBusinessIV);
        txtLoginBusinessTV = header.findViewById(R.id.txtLoginBusinessTV);
        loginLL = header.findViewById(R.id.loginLL);
        imgUserLoginIV = header.findViewById(R.id.imgUserLoginIV);
        txtUserLoginTV = header.findViewById(R.id.txtUserLoginTV);

        myAccountLL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawer(GravityCompat.START);
                homeLL.setBackgroundColor(Color.TRANSPARENT);
                findBeautyLL.setBackgroundColor(Color.TRANSPARENT);
                joinCommLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                logoutLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackgroundColor(Color.TRANSPARENT);

                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorPurpleText));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorPurpleText));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorBlack));

                loginLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorBlack));

                switchFragment(new AccountFragment(), Constants.ACCOUNT_FRAGMENT, false, null);
            }
        });
        businessLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawer(GravityCompat.START);
                homeLL.setBackgroundColor(Color.TRANSPARENT);
                findBeautyLL.setBackgroundColor(Color.TRANSPARENT);
                joinCommLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                logoutLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackgroundColor(Color.TRANSPARENT);

                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorPurpleText));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorPurpleText));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorBlack));

                loginLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorBlack));
//                switchFragment(new BusinessLoginFragment(), Constants.BUSINESS_FRAGMENT, false, null);

                if (KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.IS_BUSINNESS_LOGIN, false)) {
                    showAlertDialog(mActivity, "You are already logged in with Business Account");
                } else {
                    switchFragment(new BusinessLoginFragment(), Constants.BUSINESS_FRAGMENT, false, null);

                }
            }
        });

        settingsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                homeLL.setBackgroundColor(Color.TRANSPARENT);
                findBeautyLL.setBackgroundColor(Color.TRANSPARENT);
                joinCommLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                logoutLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackgroundColor(Color.TRANSPARENT);

                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorPurpleText));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorBlack));
                loginLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorBlack));

                switchFragment(new SettingsFragment(), Constants.SETTINGS_FRAGMENT, false, null);

            }
        });

        dealsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                homeLL.setBackgroundColor(Color.TRANSPARENT);
                findBeautyLL.setBackgroundColor(Color.TRANSPARENT);
                joinCommLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                logoutLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackgroundColor(Color.TRANSPARENT);
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorPurpleText));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorPurpleText));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorBlack));
                loginLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorBlack));
                switchFragment(new DealsFragment(), Constants.DEALS_FRAGMENT, false, null);
            }
        });

        findBeautyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                homeLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackgroundColor(Color.TRANSPARENT);
                joinCommLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackgroundColor(Color.TRANSPARENT);
                findBeautyLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                logoutLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackgroundColor(Color.TRANSPARENT);

                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorPurpleText));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorPurpleText));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorBlack));
                loginLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorBlack));
                switchFragment(new FindBeautyFragment(), Constants.FIND_BEAUTY_FRAGMENT, false, null);

            }
        });

        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                findBeautyLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackgroundColor(Color.TRANSPARENT);
                joinCommLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackgroundColor(Color.TRANSPARENT);
                homeLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                logoutLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackgroundColor(Color.TRANSPARENT);

                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorPurpleText));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorPurpleText));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorBlack));
                loginLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorBlack));
                switchFragment(new HomeeFragment(), Constants.HOME_FRAGMENT, false, null);

            }
        });

        shopLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                findBeautyLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackgroundColor(Color.TRANSPARENT);
                joinCommLL.setBackgroundColor(Color.TRANSPARENT);
                homeLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                logoutLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackgroundColor(Color.TRANSPARENT);

                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorPurpleText));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorPurpleText));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorBlack));
                loginLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorBlack));
                switchFragment(new ShopFragment(), Constants.SHOP_FRAGMENT, false, null);

            }
        });
        joinCommLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                findBeautyLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackgroundColor(Color.TRANSPARENT);
                homeLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackgroundColor(Color.TRANSPARENT);
                joinCommLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                logoutLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackgroundColor(Color.TRANSPARENT);

                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorPurpleText));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorPurpleText));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorBlack));
                loginLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorBlack));
                switchFragment(new JoinCommunityFragment(), Constants.JOIN_COMM_FRAGMENT, false, null);
            }
        });

        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                drawer_layout.closeDrawer(GravityCompat.START);
                findBeautyLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackgroundColor(Color.TRANSPARENT);
                homeLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackgroundColor(Color.TRANSPARENT);
                logoutLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                joinCommLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackgroundColor(Color.TRANSPARENT);

                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorPurpleText));

                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorPurpleText));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorBlack));
                loginLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorBlack));
                performLogoutClick();
            }
        });
        loginLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                drawer_layout.closeDrawer(GravityCompat.START);
                findBeautyLL.setBackgroundColor(Color.TRANSPARENT);
                dealsLL.setBackgroundColor(Color.TRANSPARENT);
                homeLL.setBackgroundColor(Color.TRANSPARENT);
                shopLL.setBackgroundColor(Color.TRANSPARENT);
                settingsLL.setBackgroundColor(Color.TRANSPARENT);
                myAccountLL.setBackgroundColor(Color.TRANSPARENT);
                loginLL.setBackground(getResources().getDrawable(R.drawable.bg_drawer_item));
                joinCommLL.setBackgroundColor(Color.TRANSPARENT);
                businessLL.setBackgroundColor(Color.TRANSPARENT);

                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgHomeIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgFindBeautyIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgJoinComm.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgShopIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgDealsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgSettingsIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgAccountIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgBusinessIV.setColorFilter(getResources().getColor(R.color.colorBlack));
                imgLogoutIV.setColorFilter(getResources().getColor(R.color.colorBlack));

                txtMyAccountTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtHomeTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtfindBeautyTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtjoinCommTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtshopTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtDealsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLogoutTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtSettingsTV.setTextColor(getResources().getColor(R.color.colorBlack));
                txtLoginBusinessTV.setTextColor(getResources().getColor(R.color.colorBlack));

                logoutLL.setBackgroundColor(Color.TRANSPARENT);
                imgUserLoginIV.setColorFilter(getResources().getColor(R.color.colorPurpleText));
                txtUserLoginTV.setTextColor(getResources().getColor(R.color.colorPurpleText));
                performLoginClick();
            }
        });

    }


    @OnClick({R.id.txtLoginTV, R.id.txtFindBeautyTV, R.id.txtSignUpTV, R.id.imgMenuIV, R.id.imgCurrentLocIV, R.id.editAddressET, R.id.imgLocIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtLoginTV:
                performLoginClick();
                break;
            case R.id.txtFindBeautyTV:
                performFindBeautyClick();
                break;
            case R.id.txtSignUpTV:
                performSignUpClick();
                break;
            case R.id.imgMenuIV:
                drawerOpenClick();
                break;
            case R.id.imgCurrentLocIV:
//                preventMultipleClick();
                currentLocationClick();
                break;
            case R.id.editAddressET:
//                preventMultipleClick();
                setSearchIntent();
                break;
            case R.id.imgLocIV:
//                preventMultipleClick();
                setSearchIntent();
                break;

        }
    }


    private void setSearchIntent() {
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(mActivity);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                latLng = place.getLatLng();
                mLatitude = latLng.latitude;
                mLongitude = latLng.longitude;
                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LAT_NEW, String.valueOf(mLatitude));
                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LNG_NEW, String.valueOf(mLongitude));
                Log.e(TAG, "*********SLATITUDE********" + mLatitude);
                Log.e(TAG, "*********SLONGITUDE********" + mLongitude);

                address = place.getAddress();
                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.SELECTED_LOCATION, address);
                editAddressET.setText(place.getName());
                txtLocationTV.setText(place.getName());
                Log.e(TAG, "Place: " + place.getName() + ", " + place.getId() + ", " + place.getLatLng() + ", " + place.getAddress());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.e(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
//                showToast(mActivity,"Error");
                // The user canceled the operation.
            }
        } else if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Log.i("", "User agreed to make required location settings changes.");
                    startLocationUpdates();
                    break;
                case Activity.RESULT_CANCELED:
                    Log.i("", "User chose not to make required location settings changes.");
                    requestPermission();
                    break;
                default:
                    // finish();
            }

        }
    }


    private void currentLocationClick() {
        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LAT_NEW, KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.CURR_LAT, null));
        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LNG_NEW, KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.CURR_LONG, null));
        editAddressET.setText(addres);
        txtLocationTV.setText(addres);
        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.SELECTED_LOCATION, addres);
    }

    private void performLogoutClick() {
//        if (KinnyConnectPreferences.readBoolean(mActivity, KinnyConnectPreferences.ISLOGIN, false)) {
        showSignoutAlert(mActivity, getString(R.string.logout_sure));
//        } else {
//            showLoginAlertDialog(mActivity, "You are not logged In");
//        }
    }

    public void showSignoutAlert(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.signout_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnNo = alertDialog.findViewById(R.id.btnNo);
        TextView btnYes = alertDialog.findViewById(R.id.btnYes);
        txtMessageTV.setText(strMessage);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                alertDialog.dismiss();
            }
        });
    }

    private void logout() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            executeLogoutApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userID", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.ID, null));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(mParam()).enqueue(new Callback<LogoutModel>() {
            @Override
            public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                LogoutModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                    SharedPreferences preferences = KinnyConnectPreferences.getPreferences(Objects.requireNonNull(mActivity));
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    mActivity.onBackPressed();
                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }

            }

            @Override
            public void onFailure(Call<LogoutModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());


            }
        });
    }

    private void drawerOpenClick() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer_layout, 1, 0) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                content.setTranslationX(slideX);
            }
        };

        drawer_layout.addDrawerListener(actionBarDrawerToggle);
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    private void performFindBeautyClick() {
        if (isValidate()) {
            Intent intent = new Intent(mActivity, CategoriesActivity.class);
            intent.putExtra("latitude", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LAT_NEW, null));
            intent.putExtra("longitude", KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LNG_NEW, null));
            KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LAT_NEW, "" + KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LAT_NEW, null));
            KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LNG_NEW, "" + KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LNG_NEW, null));
            Log.e(TAG, "*********SLATITUDE********" + KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LAT_NEW, null));
            Log.e(TAG, "*********SLONGITUDE********" + KinnyConnectPreferences.readString(mActivity, KinnyConnectPreferences.LNG_NEW, null));

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private void performSignUpClick() {
        Intent intent = new Intent(mActivity, SignUpActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void performLoginClick() {
        Intent intent = new Intent(mActivity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    /*
     * Set up validations for Sign In fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editAddressET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_loc));
            flag = false;
        }
        return flag;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }


    /******************
     *Fursed Google Location
     ************/
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i("", "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link LocationSettingsRequest.Builder} to build
     * a {@link LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest).setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * The callback invoked when
     * {@link SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i("", "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i("", "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(mActivity, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i("", "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i("", "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
            default:
                // finish();
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            // Check for the integer request code originally supplied to startResolutionForResult().
//            case REQUEST_CHECK_SETTINGS:
//                switch (resultCode) {
//                    case Activity.RESULT_OK:
//                        Log.i("", "User agreed to make required location settings changes.");
//                        startLocationUpdates();
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Log.i("", "User chose not to make required location settings changes.");
//                        requestPermission();
//                        break;
//                    default:
//                        // finish();
//                }
//
//        }
//    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
        //  dismissProgressDialog();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
        mGoogleApiClient.disconnect();
        dismissProgressDialog();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i("", "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        lat = mCurrentLocation.getLatitude();
        lng = mCurrentLocation.getLongitude();
        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.CURR_LAT, "" + lat);
        KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.CURR_LONG, "" + lng);

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            Log.e(TAG, "*********LATITUDE********" + lat);
            Log.e(TAG, "*********LONGITUDE********" + lng);

            addres = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//              if (KinnyConnectPreferences.readString(mActivity,KinnyConnectPreferences.SELECTED_LOCATION,"").equals("")){
//                editAddressET.setText(addres);}
            if (!(KinnyConnectPreferences.readString(mActivity, "var", "")).equals("true")) {
                KinnyConnectPreferences.writeString(mActivity, "var", "true");
                editAddressET.setText(addres);
                txtLocationTV.setText(addres);
                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.SELECTED_LOCATION, addres);
                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LAT_NEW, "" + lat);
                KinnyConnectPreferences.writeString(mActivity, KinnyConnectPreferences.LNG_NEW, "" + lng);
            }

            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onConnectionSuspended(int cause) {
        Log.i("", "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }
    /*****************************************/

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    public boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkLocationSettings();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    // requestPermission();
                }
                return;
            }

        }
    }
}
