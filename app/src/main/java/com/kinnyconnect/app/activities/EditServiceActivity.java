package com.kinnyconnect.app.activities;

import androidx.annotation.NonNull;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kinnyconnect.app.R;
import com.kinnyconnect.app.RetrofitApi.ApiClient;
import com.kinnyconnect.app.interfaces.ApiInterface;
import com.kinnyconnect.app.model.ForgotPasswordModel;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditServiceActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = EditServiceActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = EditServiceActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editServiceNameET)
    EditText editServiceNameET;
    @BindView(R.id.timeSpinner)
    Spinner timeSpinner;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.editDescriptionET)
    EditText editDescriptionET;
    @BindView(R.id.editPriceET)
    EditText editPriceET;
    @BindView(R.id.txtAddServiceTV)
    TextView txtAddServiceTV;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    String time = "";
    String service_name = "";
    String service_desc = "";
    String service_time = "";
    String service_price = "";
    String service_id = "";
int pos;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_service);
        ButterKnife.bind(this);
        setStatusBar(mActivity);
        getIntentData();
        setData();
        scrollView.setOnTouchListener((v, event) -> {
            hideKeyBoard(mActivity,getCurrentFocus());
            return true;
        });
    }

    private void setData() {
        editServiceNameET.setText(service_name);
        editPriceET.setText(service_price);
        editDescriptionET.setText(service_desc);
        setTimeAdapter();
    }

    private void getIntentData() {
        if (getIntent() != null) {
                service_name = (String) getIntent().getExtras().get("service_name");
            service_desc = (String) getIntent().getExtras().get("service_desc");
            service_time = (String) getIntent().getExtras().get("service_time");
            service_price = (String) getIntent().getExtras().get("service_price");
            service_id = (String) getIntent().getExtras().get("service_id");
            pos = (int) getIntent().getExtras().get("pos");
        }
    }


    /*
     * Time adapter
     * */
    private void setTimeAdapter() {
        hideKeyBoard(mActivity, this.timeSpinner);
        Typeface font = Typeface.createFromAsset(mActivity.getAssets(),
                "RalewayRegular.ttf");
        String[] mStatusArry = getResources().getStringArray(R.array.time);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mStatusArry) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner_time, null);
                }
                TextView itemTextView = v.findViewById(R.id.itemTimeTV);
//                if (position == 0) {
//                    itemTextView.setVisibility(View.INVISIBLE);
//                }

                itemTextView.setText(mStatusArry[position]);
                itemTextView.setTypeface(font);
                return v;
            }
        };

        timeSpinner.setAdapter(adapter);

        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                time = mStatusArry[position];
                if (position == 0) {
                    ((TextView) view).setTextSize(13);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                } else {
                    ((TextView) view).setTextSize(13);
                    ((TextView) view).setTypeface(font);
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorWhite));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                timeSpinner.setSelection(0);
            }
        });

        timeSpinner.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm=(InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(timeSpinner.getWindowToken(), 0);
                return false;
            }
        }) ;

        //Set Selections
        if (service_time!= null && !service_time.equals("")) {
            for (int i = 0; i < mStatusArry.length; i++) {
                if (service_time.equals(mStatusArry[i])) {
                    timeSpinner.setSelection(i);
                }
            }
        }
    }




    @OnClick({R.id.txtAddServiceTV, R.id.imgBackIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtAddServiceTV:
                performAddServiceClick();
                break;
            case R.id.imgBackIV:
                performBackClick();
                break;

        }
    }

    private void performBackClick() {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("serviceID", service_id);
        mMap.put("title", editServiceNameET.getText().toString().trim());
        mMap.put("description", editDescriptionET.getText().toString().trim());
        mMap.put("price", editPriceET.getText().toString().trim());
        mMap.put("time", timeSpinner.getSelectedItem().toString());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performAddServiceClick() {
        if (isValidate()) {
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error));
            } else {
                executeEditServiceApi();
            }
        }
    }


    private void executeEditServiceApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editServiceRequest(mParams()).enqueue(new Callback<ForgotPasswordModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                ForgotPasswordModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Check Validations of views
     * */
    public boolean isValidate() {
        boolean flag = true;
        if (editServiceNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_service_name));
            flag = false;
        }else if (editServiceNameET.getText().toString().trim().length()>20) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_service_name));
            flag = false;
        } else if (editPriceET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_price));
            flag = false;
        } else if (editPriceET.getText().toString().trim().length()>5) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_price));
            flag = false;
        } else if (editDescriptionET.getText().toString().trim().length()>40) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_desc));
            flag = false;
        }
        return flag;
    }
}
