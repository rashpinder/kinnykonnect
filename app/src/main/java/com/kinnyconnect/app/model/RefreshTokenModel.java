package com.kinnyconnect.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefreshTokenModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("calendar_detail")
    @Expose
    private String calendarDetail;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCalendarDetail() {
        return calendarDetail;
    }

    public void setCalendarDetail(String calendarDetail) {
        this.calendarDetail = calendarDetail;
    }

}