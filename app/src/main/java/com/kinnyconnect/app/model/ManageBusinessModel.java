package com.kinnyconnect.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ManageBusinessModel {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

public class Datum {

    @SerializedName("services")
    @Expose
    private List<Service> services = null;
    @SerializedName("workingDetails")
    @Expose
    private List<WorkingDetail> workingDetails = null;
    @SerializedName("messageBoard")
    @Expose
    private List<MessageBoard> messageBoard = null;

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<WorkingDetail> getWorkingDetails() {
        return workingDetails;
    }

    public void setWorkingDetails(List<WorkingDetail> workingDetails) {
        this.workingDetails = workingDetails;
    }

    public List<MessageBoard> getMessageBoard() {
        return messageBoard;
    }

    public void setMessageBoard(List<MessageBoard> messageBoard) {
        this.messageBoard = messageBoard;
    }

public class MessageBoard {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("businessID")
    @Expose
    private String businessID;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("created")
    @Expose
    private String created;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinessID() {
        return businessID;
    }

    public void setBusinessID(String businessID) {
        this.businessID = businessID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

}

public class Service {

    @SerializedName("serID")
    @Expose
    private String serID;
    @SerializedName("businessID")
    @Expose
    private String businessID;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("price")
    @Expose
    private String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSerID() {
        return serID;
    }

    public void setSerID(String serID) {
        this.serID = serID;
    }

    public String getBusinessID() {
        return businessID;
    }

    public void setBusinessID(String businessID) {
        this.businessID = businessID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

}

public class WorkingDetail {

    @SerializedName("workID")
    @Expose
    private String workID;
    @SerializedName("businessID")
    @Expose
    private String businessID;
    @SerializedName("days")
    @Expose
    private String days;
    @SerializedName("startHours")
    @Expose
    private String startHours;
    @SerializedName("endHours")
    @Expose
    private String endHours;
    @SerializedName("openClose")
    @Expose
    private String openClose;
    @SerializedName("created")
    @Expose
    private String created;

    public String getWorkID() {
        return workID;
    }

    public void setWorkID(String workID) {
        this.workID = workID;
    }

    public String getBusinessID() {
        return businessID;
    }

    public void setBusinessID(String businessID) {
        this.businessID = businessID;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getStartHours() {
        return startHours;
    }

    public void setStartHours(String startHours) {
        this.startHours = startHours;
    }

    public String getEndHours() {
        return endHours;
    }

    public void setEndHours(String endHours) {
        this.endHours = endHours;
    }

    public String getOpenClose() {
        return openClose;
    }

    public void setOpenClose(String openClose) {
        this.openClose = openClose;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

}}}