package com.kinnyconnect.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OldAppointmentModel {

    @SerializedName("businessName")
    @Expose
    private String businessName;
    @SerializedName("businessImage")
    @Expose
    private String businessImage;
    @SerializedName("bookingTime")
    @Expose
    private String bookingTime;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("created")
    @Expose
    private String created;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessImage() {
        return businessImage;
    }

    public void setBusinessImage(String businessImage) {
        this.businessImage = businessImage;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}